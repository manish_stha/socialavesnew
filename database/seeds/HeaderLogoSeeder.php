<?php

use Illuminate\Database\Seeder;

class HeaderLogoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Logo::create(
            [
                'logo' => '30a61ccb5fe5e371754d5b5c1c4e7c5d.svg',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ]
        );
    }
}
