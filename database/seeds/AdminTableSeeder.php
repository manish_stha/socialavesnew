<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Admin::create([

            'fullname' => 'Robik Shrestha',
            'username' => 'Robik',
            'email' => 'robik@socialaves.com',
            'password' => bcrypt('11111111'),
            'privilege' => 'Super Admin',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()

        ]);
    }
}
