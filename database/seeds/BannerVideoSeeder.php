<?php

use Illuminate\Database\Seeder;

class BannerVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Video::create([
            'video' => '6fdb60ad952c44e332d3c13fdb696ed3.mp4',   //in order to upload video or photo place the item in corresponding place first
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
