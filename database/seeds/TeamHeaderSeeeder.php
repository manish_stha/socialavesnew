<?php

use Illuminate\Database\Seeder;

class TeamHeaderSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\HomeTeamHeader::create([

            'title' => 'Meet our creative avians',
            'subtitle' => "OUR TEAM OF CREATIVE EXPLORERS MAKE DREAMS WORK",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()

        ]);
    }
}
