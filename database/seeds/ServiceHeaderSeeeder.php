<?php

use Illuminate\Database\Seeder;

class ServiceHeaderSeeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\HomeServiceHeader::create([

            'title' => 'Explore our services',
            'subtitle' => "we do what's best for you",
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()

        ]);
    }
}
