<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call([AdminTableSeeder::class]);
//        $this->call([PostTableSeeder::class]);
//        $this->call([BannerVideoSeeder::class]);
//        $this->call([HeaderLogoSeeder::class]);
//        $this->call([FooterLogoSeeder::class]);
        $this->call([ServiceHeaderSeeeder::class]);
        $this->call([TeamHeaderSeeeder::class]);

    }
}
