<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('number')->after('title');
            $table->longText('description')->nullable();
            $table->longText('about');
            $table->longText('works');
            $table->longText('needs');
            $table->longText('bonus_skills');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('number');
            $table->dropColumn('description');
            $table->dropColumn('about');
            $table->dropColumn('works');
            $table->dropColumn('needs');
            $table->dropColumn('bonus_skills');

        });
    }
}
