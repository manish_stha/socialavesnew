<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->string('category');
            $table->string('preview_image');
            $table->longText('subtitle');
            $table->string('client');
            $table->string('url')->nullable();
            $table->string('platform')->nullable();
            $table->string('role')->nullable();
            $table->longText('description')->nullable();
            $table->string('singleimage')->nullable();
            $table->string('video')->nullable();
            $table->string('thumbnail_image')->nullable();
            $table->string('videotext')->nullable();
            $table->string('slider1')->nullable();
            $table->string('slider2')->nullable();
            $table->string('slider3')->nullable();
            $table->string('slider4')->nullable();
            $table->string('slider5')->nullable();
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
