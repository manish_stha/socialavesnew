<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeofjobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->string('job_type')->after('number');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     *
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropColumn('job_type');
        });
    }
}
