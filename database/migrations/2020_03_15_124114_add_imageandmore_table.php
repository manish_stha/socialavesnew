<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageandmoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->string('innerimage')->after('image')->nullable();
            $table->string('themeimage')->after('innerimage')->nullable();
            $table->string('innertitle')->after('subtitle');
            $table->longText('innersubtitle')->after('innertitle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            $table->dropColumn('innerimage');
            $table->dropColumn('themeimage');
            $table->dropColumn('innertitle');
            $table->dropColumn('innersubtitle');
        });
    }
}
