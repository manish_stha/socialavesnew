<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'name',
        'upload',
        'title',
        'body',
        'upload1',
        'upload2',
        'upload3',
        'upload4',
        'upload5',
        'video',
        'blog_type',
        'status'
    ];

    public function quote()
    {
        return $this->hasMany('App\Quote', 'blog_id', 'id');

    }

    public function comment()
    {
        return $this->hasMany('App\Comment', 'blog_id', 'id');

    }

    public function reply()
    {
        return $this->hasMany('App\Reply', 'blog_id', 'id');

    }

}
