<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $fillable = [
        'blog_id',
        'quote',
        'writer',
        'status',
    ];

    public function blog()
    {
        return $this->belongsTo('App\Blog', 'blog_id');
    }
}
