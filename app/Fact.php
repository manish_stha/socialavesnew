<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fact extends Model
{
    protected $fillable = [
        'title1',
        'number1',
        'title2',
        'number2',
        'title3',
        'number3',
        'title4',
        'number4',
    ];
}
