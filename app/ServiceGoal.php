<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGoal extends Model
{
    protected $fillable = [
        'service_id',
        'image',
        'title',
        'description',
        'status',
    ];

    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }
}
