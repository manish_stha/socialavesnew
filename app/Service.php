<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'title',
        'subtitle',
        'innertitle',
        'innersubtitle',
        'subtitle',
        'description',
        'description1',
        'image',
        'diduknow',
        'innerimage',
        'themeimage',
        'status',
    ];
}
