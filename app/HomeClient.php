<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeClient extends Model
{
    protected $fillable = [
        'name',
        'image',
        'designation',
        'comment',
        'status'
    ];
}
