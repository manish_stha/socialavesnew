<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Story extends Model
{
    protected $fillable = [
        'header',
        'name',
        'designation',
        'image1',
        'image2',
        'image3',
        'title1',
        'description1',
        'title2',
        'description2',
        'title3',
        'description3',
        'title4',
        'description4'
    ];
}
