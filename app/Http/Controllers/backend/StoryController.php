<?php

namespace App\Http\Controllers\backend;

use App\Story;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StoryController extends BackendController
{
    public function index()
    {
        $storyData = Story::orderBy('id', 'DESC')->get();
        $this->data('storyData', $storyData);
        return view($this->pagePath . '.story.show-stories', $this->data);
    }

    public function StoryTable()
    {
        $storyData = Story::orderBy('id', 'DESC')->get();
        $this->data('storyData', $storyData);
        return view($this->pagePath . '.story.show-stories-table', $this->data);
    }

    public function addStory(Request $request)
    {
        if ($request->isMethod('get')) {
            return view($this->pagePath . '.story.add-story', $this->data);
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'header' => 'required',
                'name' => 'required',
                'designation' => 'required',
                'title1' => 'required',
                'description1' => 'required',
                'title2' => 'required',
                'description2' => 'required',
                'image1' => 'required',
                'image2' => 'required',
                'image3' => 'required'
            ]);


            $data['header'] = $request->header;
            $data['name'] = $request->name;
            $data['designation'] = $request->designation;
            $data['title1'] = $request->title1;
            $data['description1'] = $request->description1;
            $data['title2'] = $request->title2;
            $data['description2'] = $request->description2;
            $data['title3'] = $request->title3;
            $data['description3'] = $request->description3;
            $data['title4'] = $request->title4;
            $data['description4'] = $request->description4;


            //file upload...
            if ($request->hasFile('image1')) {
                $file = $request->file('image1');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/stories');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image1'] = $imageName;

            }

            if ($request->hasFile('image2')) {
                $file = $request->file('image2');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/stories');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image2'] = $imageName;

            }

            if ($request->hasFile('image3')) {
                $file = $request->file('image3');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/stories');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image3'] = $imageName;

            }


            if (Story::create($data)) {
                return redirect()->route('employee-story')->with('success', 'Story has been created');

            }
        }

    }

    public function updateStoryStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $value = Story::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $value->status = 1;
            }
            if (isset($_POST['active'])) {
                $value->status = 0;
            }

            if ($value->update()) {
                return redirect()->back()->with('success', 'Story Status has been changed successfully');
            }
        }

        return false;
    }

    public function deleteImage1($id)
    {
        $criteria = $id;
        $findData = Story::findOrFail($criteria);
        $filename = $findData->image1;
        $deletePath = public_path('uploads/images/stories/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteImage2($id)
    {
        $criteria = $id;
        $findData = Story::findOrFail($criteria);
        $filename = $findData->image2;
        $deletePath = public_path('uploads/images/stories/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteImage3($id)
    {
        $criteria = $id;
        $findData = Story::findOrFail($criteria);
        $filename = $findData->image3;
        $deletePath = public_path('uploads/images/stories/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteStory(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Story::findOrFail($criteria);
        if ($this->deleteImage1($criteria) && $this->deleteImage2($criteria) && $this->deleteImage3($criteria) && ($findData->delete())) {

            return redirect()->back()->with('success', 'Story has been deleted');
        }

    }

    public function editStory(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Story::findOrFail($criteria);
        $this->data('storyData', $findData);

        return view($this->pagePath . '.story.edit-story', $this->data);

    }

    public function editStoryAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'header' => 'required',
                'name' => 'required',
                'designation' => 'required',
                'title1' => 'required',
                'description1' => 'required',
                'title2' => 'required',
                'description2' => 'required',
            ]);


            $data['header'] = $request->header;
            $data['name'] = $request->name;
            $data['designation'] = $request->designation;
            $data['title1'] = $request->title1;
            $data['description1'] = $request->description1;
            $data['title2'] = $request->title2;
            $data['description2'] = $request->description2;
            $data['title3'] = $request->title3;
            $data['description3'] = $request->description3;
            $data['title4'] = $request->title4;
            $data['description4'] = $request->description4;


            //file upload...
            if ($request->hasFile('image1')) {
                $file = $request->file('image1');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/stories');
                if ($this->deleteImage1($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image1'] = $imageName;

            }

            if ($request->hasFile('image2')) {
                $file = $request->file('image2');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/stories');
                if ($this->deleteImage2($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image2'] = $imageName;

            }

            if ($request->hasFile('image3')) {
                $file = $request->file('image3');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/stories');
                if ($this->deleteImage3($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image3'] = $imageName;

            }

        }
        if (Story::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('employee-story')->with('success', 'Story has been updated');


        }

    }


}
