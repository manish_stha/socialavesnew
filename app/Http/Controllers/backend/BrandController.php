<?php

namespace App\Http\Controllers\backend;

use App\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BrandController extends BackendController
{
    public function index()
    {
        $brandData = Brand::orderBy('id', 'DESC')->get();
        $this->data('brandData', $brandData);

        return view($this->pagePath . '.home.client-brands.show-brands', $this->data);
    }

    public function BrandInList()
    {
        $brandData = Brand::orderBy('id', 'DESC')->get();
        $this->data('brandData', $brandData);

        return view($this->pagePath . '.home.client-brands.show-brands-table', $this->data);
    }

    public function addBrand(Request $request)
    {
        if ($request->isMethod('get')) {
            return view($this->pagePath . '.home.client-brands.show-brands', $this->data);
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'image' => 'required',
            ]);

            $data['image'] = $request->image;


            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/home/brands');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }


            if (Brand::create($data)) {
                return redirect()->back()->with('success', 'Client Brand have been added');

            }
        }
    }

    public function updateBrandStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $value = Brand::findorFail($criteria);

            if (isset($_POST['active'])) {
                $value->status = '0';
            }
            if (isset($_POST['inactive'])) {
                $value->status = '1';
            }

            if ($value->update()) {
                return redirect()->back()->with('success', 'Brand Status has been updated successfully');
            }
        }

        return false;
    }


    public function deleteImage($id)
    {
        $criteria = $id;
        $findData = Brand::findOrFail($criteria);
        $filename = $findData->image;
        $deletePath = public_path('uploads/images/home/brands/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteBrand(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Brand::findOrFail($criteria);
        if ($this->deleteImage($criteria) && ($findData->delete())) {

            return redirect()->back()->with('success', 'Brand has been deleted');
        }
    }

    public function editBrandAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;


            $data['image'] = $request->image;

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/home/brands');
                if ($this->deleteImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

        }
        if (Brand::where('id', '=', $criteria)->update($data)) {
            return redirect()->back()->with('success', 'Brand has been updated successfully');


        }

    }


}
