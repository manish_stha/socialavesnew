<?php

namespace App\Http\Controllers\backend;

use App\PortfolioGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfolioGalleryController extends BackendController
{
    public function deleteGallery($id)
    {
        $criteria = $id;
        $findData = PortfolioGallery::findOrFail($criteria);
        $filename = $findData->gallery;
        $deletePath = public_path('uploads/images/portfolio/gallery/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deletePortfolioGallery(Request $request)
    {
        $criteria = $request->criteria;
        $findData = PortfolioGallery::findOrFail($criteria);
        if ($this->deleteGallery($criteria) && $findData->delete()) {

            return redirect()->back()->with('success', 'File has been deleted');
        }

    }
}
