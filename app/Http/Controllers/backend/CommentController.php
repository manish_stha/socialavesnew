<?php

namespace App\Http\Controllers\backend;

use App\Comment;
use App\Http\Controllers\frontend\FrontendController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends BackendController
{
    public function index()
    {
        $commentData = Comment::orderBy('id', 'DESC')->get();
        $this->data('commentData', $commentData);
        return view($this->pagePath . '.comment.show-comments', $this->data);
    }

    public function deleteComment(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Comment::findOrFail($criteria);
        if ($findData->delete()) {

            return redirect()->route('comments')->with('success', 'Comment has been deleted');
        }
    }

    public function updateCommentStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $comment = Comment::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $comment->status = 1;
            }
            if (isset($_POST['active'])) {
                $comment->status = 0;
            }

            if ($comment->update()) {
                return redirect()->route('comments')->with('success', 'Comment Status has been changed successfully');
            }
        }

        return false;
    }


}
