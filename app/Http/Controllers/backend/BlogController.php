<?php

namespace App\Http\Controllers\backend;

use App\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends BackendController
{
    public function index()
    {
        $blogData = Blog::orderBy('id', 'DESC')->paginate(6);
        $this->data('blogData', $blogData);
        return view($this->pagePath . '.blog.show-blog', $this->data);
    }

    public function BlogTable()
    {
        $blogData = Blog::orderBy('id', 'DESC')->get();
        $this->data('blogData', $blogData);
        return view($this->pagePath . '.blog.show-blogs-table', $this->data);
    }

    public function addBlog(Request $request)
    {
        if ($request->isMethod('get')) {
            return view($this->pagePath . '.blog.add-blog', $this->data);
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'body' => 'required',
            ]);

            $data['name'] = $request->name;
            $data['title'] = $request->title;
            $data['body'] = $request->body;
            $data['blog_type'] = $request->blog_type;

            //file upload...
            if ($request->hasFile('upload')) {
                $file = $request->file('upload');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogger');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload'] = $imageName;

            }


            //file upload...
            if ($request->hasFile('upload1')) {
                $file = $request->file('upload1');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload1'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload2')) {
                $file = $request->file('upload2');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload2'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload3')) {
                $file = $request->file('upload3');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload3'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload4')) {
                $file = $request->file('upload4');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload4'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload5')) {
                $file = $request->file('upload5');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload5'] = $imageName;

            }

            //file upload...
            if ($request->hasFile('video')) {
                $file = $request->file('video');
                $ext = $file->getClientOriginalExtension();
                $videoName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/videos/blogs');
                if (!$file->move($uploadPath, $videoName)) {
                    return redirect()->back();
                }

                $data['video'] = $videoName;

            }

            if (Blog::create($data)) {
                return redirect()->route('blogs')->with('success', 'Blog has been created');

            }
        }

    }


    public function deleteFile($id)  //$id is id of Blog which is passed through a request in show blog page.....clarified in deleteBlog & editBlogAction method (can be used in any method)
    {
        $criteria = $id;
        $findData = Blog::findOrFail($criteria);
        $filename = $findData->upload;
        $deletePath = public_path('uploads/images/blogger/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;  //image nahuda ni delete garna
    }

    public function deleteFile1($id)
    {
        $criteria = $id;
        $findData = Blog::findOrFail($criteria);
        $filename = $findData->upload1;
        $deletePath = public_path('uploads/images/blogs/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteFile2($id)
    {
        $criteria = $id;
        $findData = Blog::findOrFail($criteria);
        $filename = $findData->upload2;
        $deletePath = public_path('uploads/images/blogs/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteFile3($id)
    {
        $criteria = $id;
        $findData = Blog::findOrFail($criteria);
        $filename = $findData->upload3;
        $deletePath = public_path('uploads/images/blogs/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteFile4($id)
    {
        $criteria = $id;
        $findData = Blog::findOrFail($criteria);
        $filename = $findData->upload4;
        $deletePath = public_path('uploads/images/blogs/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteFile5($id)
    {
        $criteria = $id;
        $findData = Blog::findOrFail($criteria);
        $filename = $findData->upload5;
        $deletePath = public_path('uploads/images/blogs/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteVideo($id)
    {
        $criteria = $id;
        $findData = Blog::findOrFail($criteria);
        $filename = $findData->video;
        $deletePath = public_path('uploads/videos/blogs/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }


    public function deleteBlog(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Blog::findOrFail($criteria);
        if ($this->deleteFile($criteria) && $this->deleteFile1($criteria) && $this->deleteFile2($criteria) && $this->deleteFile3($criteria) && $this->deleteFile4($criteria) && $this->deleteFile5($criteria) && $this->deleteVideo($criteria) && ($findData->delete())) {

            return redirect()->route('blog-list')->with('success', 'Blog has been deleted');
        }


    }

    public function editBlog(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Blog::findOrFail($criteria);
        $this->data('blogData', $findData);



        return view($this->pagePath . '.blog.edit-blog', $this->data);

    }

    public function editBlogAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'body' => 'required',
            ]);

            $data['name'] = $request->name;
            $data['title'] = $request->title;
            $data['body'] = $request->body;
            $data['blog_type'] = $request->blog_type;


            //file upload...
            if ($request->hasFile('upload')) {
                $file = $request->file('upload');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogger');
                if ($this->deleteFile($criteria) && !$file->move($uploadPath, $imageName)) { //replaces existing files
                    return redirect()->back();
                }

                $data['upload'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload1')) {
                $file = $request->file('upload1');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if ($this->deleteFile1($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload1'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload2')) {
                $file = $request->file('upload2');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if ($this->deleteFile2($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload2'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload3')) {
                $file = $request->file('upload3');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if ($this->deleteFile3($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload3'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload4')) {
                $file = $request->file('upload4');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if ($this->deleteFile4($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload4'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('upload5')) {
                $file = $request->file('upload5');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/blogs');
                if ($this->deleteFile5($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['upload5'] = $imageName;

            }
            //file upload...
            if ($request->hasFile('video')) {
                $file = $request->file('video');
                $ext = $file->getClientOriginalExtension();
                $videoName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/videos/blogs');
                if ($this->deleteVideo($criteria) && !$file->move($uploadPath, $videoName)) {
                    return redirect()->back();
                }

                $data['video'] = $videoName;

            }

        }
        if (Blog::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('blogs')->with('success', 'Blog has been updated');


        }

    }


    public function updateBlogStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $blog = Blog::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $blog->status = 1;
            }
            if (isset($_POST['active'])) {
                $blog->status = 0;
            }

            if ($blog->update()) {
                return redirect()->back()->with('success', 'Blog Status has been changed successfully');
            }
        }

        return false;
    }


}
