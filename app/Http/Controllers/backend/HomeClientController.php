<?php

namespace App\Http\Controllers\backend;

use App\HomeClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeClientController extends BackendController
{
    public function index()
    {
        $clientData = HomeClient::orderBy('id', 'ASC')->get();
        $this->data('clientData', $clientData);
        return view($this->pagePath . '.home.client.show-client', $this->data);
    }

    public function ShowClientInTable ()
    {
        $clientData = HomeClient::orderBy('id', 'ASC')->get();
        $this->data('clientData', $clientData);
        return view($this->pagePath . '.home.client.show-client-table', $this->data);
    }

    public function addClient(Request $request)
    {
        if ($request->isMethod('get')) {

            return view($this->pagePath . '.home.client.show-client', $this->data);
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'comment' => 'required',
            ]);

            $data['name'] = $request->name;
            $data['comment'] = $request->comment;
            $data['designation'] = $request->designation;

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/home/client');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

            if (HomeClient::create($data)) {
                return redirect()->route('home-client')->with('success', 'Client Testimony has been added');

            }
        }

    }

    public function deleteFile($id)
    {
        $criteria = $id;
        $findData = HomeClient::findOrFail($criteria);
        $filename = $findData->image;
        $deletePath = public_path('uploads/images/home/client/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function editClientAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $data['name'] = $request->name;
            $data['designation'] = $request->designation;
            $data['comment'] = $request->comment;


            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/home/client');
                if ($this->deleteFile($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

        }
        if (HomeClient::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('home-client')->with('success', 'Information has been updated');


        }

    }

    public function deleteClient(Request $request)
    {
        $criteria = $request->criteria;
        $findData = HomeClient::findOrFail($criteria);
        if ($this->deleteFile($criteria) && $findData->delete()) {

            return redirect()->route('client-table')->with('success', 'Client has been deleted');
        }

    }

    public function updateClientStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $service = HomeClient::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $service->status = 1;
            }
            if (isset($_POST['active'])) {
                $service->status = 0;
            }

            if ($service->update()) {
                return redirect()->back()->with('success', 'Client Testimony Status has been changed successfully');
            }
        }

        return false;
    }
}
