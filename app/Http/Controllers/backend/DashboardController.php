<?php

namespace App\Http\Controllers\backend;

use App\Admin;
use App\Application;
use App\Blog;
use App\Comment;
use App\Contact;
use App\Portfolio;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends BackendController
{


    public function index()
    {

        $contactData = Contact::orderBy('id', 'DESC')->get();
        $this->data('contactData', $contactData);

//        $CountContact = Contact::where("created_at", ">=", date("Y-m-d H:i:s", strtotime('-1 minute', time())))->count();
        $CountContact = Contact::all()->count();
        $this->data('CountContact', $CountContact);

        $applicationData = Application::orderBy('id', 'DESC')->get();
        $this->data('applicationData', $applicationData);

        $CountApplication = Application::all()->count();
        $this->data('CountApplication', $CountApplication);

        $CommentData = Comment::orderBy('id', 'DESC')->get();
        $this->data('$CommentData', $CommentData);

        $CountComment = Comment::all()->count();
        $this->data('CountComment', $CountComment);

        $CountUser = Admin::where('status', '=', 1)->count();
        $this->data('CountUser', $CountUser);

        $CountPortfolio = Portfolio::where('status', '=', 1)->count();
        $this->data('CountPortfolio', $CountPortfolio);

        $CountService = Service::where('status', '=', 1)->count();
        $this->data('CountService', $CountService);

        $CountBlog = Blog::where('status', '=', 1)->count();
        $this->data('CountBlog', $CountBlog);


        return view($this->pagePath . '.dashboard.dashboard', $this->data);
    }

}

