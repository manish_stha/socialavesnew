<?php

namespace App\Http\Controllers\backend;

use App\Application;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApplicationsController extends BackendController
{
    public function index()
    {
        $PostData = Post::orderBy('id', 'DESC')->get();
        $this->data('PostData', $PostData);

        $applicationData = Application::orderBy('id', 'DESC')->get();
        $this->data('applicationData', $applicationData);
        return view($this->pagePath . '.career.application.show-applications', $this->data);
    }

    public function deleteFile($id)
    {
        $criteria = $id;
        $findData = Application::findOrFail($criteria);
        $filename = $findData->cv;
        $deletePath = public_path('uploads/application/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;  //image nahuda ni delete garna
    }

    public function deleteApplication(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Application::findOrFail($criteria);
        if ($this->deleteFile($criteria) && ($findData->delete())) {

            return redirect()->route('applications')->with('success', 'Application has been deleted');
        }

    }
}
