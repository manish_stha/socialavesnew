<?php

namespace App\Http\Controllers\backend;

use App\Service;
use App\ServiceGallery;
use App\ServiceofService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesOfServicesController extends Controller
{

    protected $backendPath = 'backend.';
    protected $pagePath = '';

    protected $innerservices = null;

    public function __construct(ServiceofService $innerservices)
    {
        $this->pagePath = $this->backendPath . 'pages';

        $this->innerservices = $innerservices;

    }

    public function index()
    {
        $InnerData = ServiceofService::orderBy('id', 'ASC')->get();
        $this->data('InnerData', $InnerData);
        return view($this->pagePath . '.services.innerservices.show-inner', $this->data);
    }

    public function addServiceinServices(Request $request)
    {
        if ($request->isMethod('get')) {

            $criteria = $request->criteria;
            $findata = Service::findOrFail($criteria);
            $this->data('ServiceData', $findata);
            return view($this->pagePath . '.services.innerservices.add-inner', $this->data);
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                    'title' => 'required',
                    'description' => 'required',
                    'gallery.*' => 'sometimes|image|max:20000'
                ]
                ,
                [
                    'gallery.*.image' => 'The file you choosed for gallery is not acceptable.'
                ]
            );

            $data['service_id'] = $request->service_id;
            $data['title'] = $request->title;
            $data['description'] = $request->description;
            $data['link1'] = $request->link1;
            $data['link2'] = $request->link2;

            $data = $request->except('gallery');

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/inner-services/single-image');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;
            }

            //preview image upload...
            if ($request->hasFile('preview1')) {
                $file = $request->file('preview1');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/inner-services/preview-images');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview1'] = $imageName;

            }

            //preview image upload...
            if ($request->hasFile('preview2')) {
                $file = $request->file('preview2');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/inner-services/preview-images');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview2'] = $imageName;

            }

            //preview image upload...
            if ($request->hasFile('preview3')) {
                $file = $request->file('preview3');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/inner-services/preview-images');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview3'] = $imageName;

            }

            //to add gallery through add inner services form
            $this->innerservices->fill($data);
            $status = $this->innerservices->save();
            if ($status) {
                if ($request->gallery) {
                    foreach ($request->gallery as $uploaded_gallery) {
                        $gallery_name = uploadImage($uploaded_gallery, 'images/services/inner-services/gallery');
                        if ($gallery_name) {
                            $gallery_data = array(
                                'innerservice_id' => $this->innerservices->id,
                                'gallery' => $gallery_name
                            );
                            $gallery_obj = new ServiceGallery();
                            $gallery_obj->fill($gallery_data);
                            $gallery_obj->save();
                        }
                    }
                }

                return redirect()->back()->with('success', 'Contents for Service have been created');

            }

        }
    }

    public function updateServiceinServicesStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $service = ServiceofService::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $service->status = 1;
            }
            if (isset($_POST['active'])) {
                $service->status = 0;
            }

            if ($service->update()) {
                return redirect()->back()->with('success', 'Status has been changed successfully');
            }
        }

        return false;
    }

    public function deleteSingleImage($id)
    {
        $criteria = $id;
        $findData = ServiceofService::findOrFail($criteria);
        $filename = $findData->image;
        $deletePath = public_path('uploads/images/services/inner-services/single-image/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deletePreview1($id)
    {
        $criteria = $id;
        $findData = ServiceofService::findOrFail($criteria);
        $filename = $findData->preview1;
        $deletePath = public_path('uploads/images/services/inner-services/preview-images/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deletePreview2($id)
    {
        $criteria = $id;
        $findData = ServiceofService::findOrFail($criteria);
        $filename = $findData->preview2;
        $deletePath = public_path('uploads/images/services/inner-services/preview-images/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deletePreview3($id)
    {
        $criteria = $id;
        $findData = ServiceofService::findOrFail($criteria);
        $filename = $findData->preview3;
        $deletePath = public_path('uploads/images/services/inner-services/preview-images/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteServiceinServices(Request $request)
    {
        $criteria = $request->criteria;

        $this->innerservices = $this->innerservices->with('gallery')->find($request->criteria);
        $findData = ServiceofService::findOrFail($criteria);

        $gallery = $this->innerservices->gallery;

        $del = $this->deleteSingleImage($criteria) && $this->deletePreview1($criteria) && $this->deletePreview2($criteria) && $this->deletePreview3($criteria) && ($findData->delete());
        if ($del) {
            if ($gallery->count()) {
                foreach ($gallery as $deleteGallery) {
                    deleteImage($deleteGallery->gallery, 'images/services/inner-services/gallery');
                }
            }
            return redirect()->back()->with('success', 'Service has been deleted');

        }
    }

    public function editServiceinServices(Request $request)
    {
        $criteria = $request->criteria;
        $InnerData = ServiceofService::findOrFail($criteria);
        $this->data('InnerData', $InnerData);

        //to fetch service related gallery
        $galleryData = ServiceGallery::where('innerservice_id', '=', $InnerData->id)->orderBy('id', 'ASC')->get();
        $this->data('galleryData', $galleryData);

        return view($this->pagePath . '.services.innerservices.edit-inner', $this->data);
    }

    public function editServiceinServicesAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                    'title' => 'required',
                    'description' => 'required',
                    'gallery.*' => 'sometimes|image|max:20000'
                ]
                ,
                [
                    'gallery.*.image' => 'The file you choosed for gallery is not acceptable.'
                ]
            );

            $data['title'] = $request->title;
            $data['description'] = $request->description;
            $data['link1'] = $request->link1;
            $data['link2'] = $request->link2;

            $data = $request->except('gallery');

            $criteria = $request->criteria;

            //finds criteria related row from the table
            $this->innerservices = $this->innerservices->with('gallery')->find($request->criteria);

            if (!$this->innerservices) {
                request()->session()->flash('error', "Service does not exist");
                return redirect()->back();
            }

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/inner-services/single-image');
                if ($this->deleteSingleImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;
            }

            //preview image upload...
            if ($request->hasFile('preview1')) {
                $file = $request->file('preview1');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/inner-services/preview-images');
                if ($this->deletePreview1($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview1'] = $imageName;

            }

            //preview image upload...
            if ($request->hasFile('preview2')) {
                $file = $request->file('preview2');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/inner-services/preview-images');
                if ($this->deletePreview2($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview2'] = $imageName;

            }

            //preview image upload...
            if ($request->hasFile('preview3')) {
                $file = $request->file('preview3');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/inner-services/preview-images');
                if ($this->deletePreview3($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview3'] = $imageName;

            }
        }

        //to update gallery through add inner services form
        $this->innerservices->fill($data);
        $status = $this->innerservices->save();
        if ($status) {
            if ($request->gallery) {
                foreach ($request->gallery as $uploaded_gallery) {
                    $gallery_name = uploadImage($uploaded_gallery, 'images/services/inner-services/gallery');
                    if ($gallery_name) {
                        $gallery_data = array(
                            'innerservice_id' => $this->innerservices->id,
                            'gallery' => $gallery_name
                        );
                        $gallery_obj = new ServiceGallery();
                        $gallery_obj->fill($gallery_data);
                        $gallery_obj->save();
                    }
                }
            }

            return redirect()->route('service-in-services')->with('success', 'Contents for Service have been updated');

        }

    }
}
