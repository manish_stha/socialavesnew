<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class AdminController extends BackendController
{
    public function index()
    {
        $adminData = Admin::orderBy('id', 'DESC')->get();
        $this->data('adminData', $adminData);
        return view($this->pagePath . '.users.show-users', $this->data);
    }

    public function addUser(Request $request)
    {
        if ($request->isMethod('get')) {
            return view($this->pagePath . '.users.add-user', $this->data);
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'fullname' => 'required|min:3|max:50',
                'username' => 'required|min:3|max:20|unique:admins,username',
                'email' => 'required|email|unique:admins,email',
                'password' => 'required|min:8|max:20|confirmed',
            ]);

            $data['fullname'] = $request->fullname;
            $data['username'] = $request->username;
            $data['email'] = $request->email;
            $data['privilege'] = $request->privilege;
            $data['password'] = bcrypt($request->password);


        }

        if (Admin::create($data)) {
            return redirect()->route('users')->with('success', 'User has been added');

        }

    }

    public function deleteUser(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Admin::findOrFail($criteria);
        if ($findData->delete()) {

            return redirect()->back()->with('success', 'User has been deleted');
        }
    }

    public function updateUserStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $admin = Admin::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $admin->status = 1;
            }
            if (isset($_POST['active'])) {
                $admin->status = 0;
            }

            if ($admin->update()) {
                return redirect()->route('users')->with('success', 'User Status has been updated successfully');
            }
        }

        return false;
    }


    public function editUser(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Admin::findOrFail($criteria);
        $this->data('adminData', $findData);
        return view($this->pagePath . '.users.edit-user', $this->data);

    }

    public function editUserAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'fullname' => 'required|min:3|max:50',
                'username' => 'required|min:3|max:20|', [
                    Rule::unique('admins', 'username')->ignore($criteria)
                ],
                'email' => 'required|email|', [
                    Rule::unique('admins', 'email')->ignore($criteria)
                ]
            ]);

            $data['fullname'] = $request->fullname;
            $data['username'] = $request->username;
            $data['email'] = $request->email;
            $data['privilege'] = $request->privilege;

        }

        if (Admin::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('users')->with('success', 'User Data has been updated');
        }

    }


}
