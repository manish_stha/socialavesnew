<?php

namespace App\Http\Controllers\backend;

use App\HomeServiceHeader;
use App\Service;
use Illuminate\Http\Request;

class ServiceController extends BackendController
{
    public function index()
    {
        //to fetch service header
        $serviceHeaderData = HomeServiceHeader::orderBy('id', 'DESC')->get();
        $this->data('serviceHeaderData', $serviceHeaderData);

        $serviceData = Service::orderBy('id', 'ASC')->paginate(10);
        $this->data('serviceData', $serviceData);
        return view($this->pagePath . '.services.show-services', $this->data);
    }

    //Edit Service Header
    public function editServiceHeaderAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'title' => 'required',
                'subtitle' => 'required'
            ]);

            $data['title'] = $request->title;
            $data['subtitle'] = $request->subtitle;

        }
        if (HomeServiceHeader::where('id', '=', $criteria)->update($data)) {
            return redirect()->back()->with('success', 'Service Header has been updated');


        }

    }

    public function ServiceTable()
    {
        $serviceData = Service::orderBy('id', 'ASC')->get();
        $this->data('serviceData', $serviceData);
        return view($this->pagePath . '.services.show-service-table', $this->data);
    }

    public function addService(Request $request)
    {
        if ($request->isMethod('get')) {
            $this->data('serviceData', Service::all());
            return view($this->pagePath . '.services.add-service', $this->data);
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'title' => 'required',
                'subtitle' => 'required',
                'icon' => 'required',
                'image' => 'required',
                'description' => 'required',
            ]);

            $data['title'] = $request->title;
            $data['subtitle'] = $request->subtitle;
            $data['innertitle'] = $request->innertitle;
            $data['innersubtitle'] = $request->innersubtitle;
            $data['description'] = $request->description;
            $data['description1'] = $request->description1;
            $data['diduknow'] = $request->diduknow;

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

            //Icon upload...
            if ($request->hasFile('icon')) {
                $file = $request->file('icon');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/icons');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['icon'] = $imageName;
            }

            //file upload...
            if ($request->hasFile('innerimage')) {
                $file = $request->file('innerimage');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/innerimage');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['innerimage'] = $imageName;

            }

            //file upload...
            if ($request->hasFile('themeimage')) {
                $file = $request->file('themeimage');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/themeimage');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['themeimage'] = $imageName;

            }

            if (Service::create($data)) {
                return redirect()->route('services')->with('success', 'Service has been added');

            }
        }

    }

    public function updateServiceStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $service = Service::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $service->status = 1;
            }
            if (isset($_POST['active'])) {
                $service->status = 0;
            }

            if ($service->update()) {
                return redirect()->back()->with('success', 'Service Status has been changed successfully');
            }
        }

        return false;
    }

    public function deleteIcon($id)
    {
        $criteria = $id;
        $findData = Service::findOrFail($criteria);
        $filename = $findData->icon;
        $deletePath = public_path('uploads/images/services/icons/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteImage($id)
    {
        $criteria = $id;
        $findData = Service::findOrFail($criteria);
        $filename = $findData->image;
        $deletePath = public_path('uploads/images/services/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteInnerImage($id)
    {
        $criteria = $id;
        $findData = Service::findOrFail($criteria);
        $filename = $findData->innerimage;
        $deletePath = public_path('uploads/images/services/innerimage/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteThemeImage($id)
    {
        $criteria = $id;
        $findData = Service::findOrFail($criteria);
        $filename = $findData->themeimage;
        $deletePath = public_path('uploads/images/services/themeimage/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteService(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Service::findOrFail($criteria);
        if ($this->deleteIcon($criteria) && $this->deleteImage($criteria) && $this->deleteInnerImage($criteria) && $this->deleteThemeImage($criteria) && $findData->delete()) {

            return redirect()->back()->with('success', 'Service has been deleted');
        }
    }

    public function editService(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Service::findOrFail($criteria);
        $this->data('serviceData', $findData);
        return view($this->pagePath . '.services.edit-service', $this->data);
    }

    public function editServiceAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'title' => 'required',
                'subtitle' => 'required',
                'description' => 'required',
            ]);

            $data['title'] = $request->title;
            $data['subtitle'] = $request->subtitle;
            $data['innertitle'] = $request->innertitle;
            $data['innersubtitle'] = $request->innersubtitle;
            $data['description'] = $request->description;
            $data['description1'] = $request->description1;
            $data['diduknow'] = $request->diduknow;

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services');
                if ($this->deleteImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

            //file upload...
            if ($request->hasFile('icon')) {
                $file = $request->file('icon');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/icons');
                if ($this->deleteIcon($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['icon'] = $imageName;

            }

            //file upload...
            if ($request->hasFile('innerimage')) {
                $file = $request->file('innerimage');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/innerimage');
                if ($this->deleteInnerImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['innerimage'] = $imageName;

            }

            //file upload...
            if ($request->hasFile('themeimage')) {
                $file = $request->file('themeimage');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/themeimage');
                if ($this->deleteThemeImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['themeimage'] = $imageName;

            }

        }

        if (Service::where('id', '=', $criteria)->update($data)) {
            return redirect()->back()->with('success', 'Service has been updated');
        }


    }


}
