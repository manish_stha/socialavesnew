<?php

namespace App\Http\Controllers\backend;

use App\Footer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FooterLogoController extends BackendController
{
    public function index()
    {
        $footerData = Footer::orderBy('id', 'DESC')->get();
        $this->data('footerData', $footerData);

        return view($this->pagePath . '.home.footer.show-footer', $this->data);
    }

    public function ShowFooterLogoInTable()
    {
        $footerData = Footer::orderBy('id', 'DESC')->get();
        $this->data('footerData', $footerData);

        return view($this->pagePath . '.home.footer.show-footer-table', $this->data);
    }

    public function addFooterLogo(Request $request)
    {
        if ($request->isMethod('get')) {
            $footerData = Footer::orderBy('id', 'DESC')->get();
            $this->data('footerData', $footerData);
            return view($this->pagePath . '.home.footer.show-footer', $this->data);
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'image' => 'required',
            ]);


            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/home/footer-logo');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }


            if (Footer::create($data)) {
                return redirect()->route('footer-logo')->with('success', 'Footer logo have been added');

            }
        }

    }


    public function deleteImage($id)  //to delete previous logo while changing the logo
    {
        $criteria = $id;
        $findData = Footer::findOrFail($criteria);
        $filename = $findData->image;
        $deletePath = public_path('uploads/images/home/footer-logo/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function editFooterLogoAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/home/footer-logo');
                if ($this->deleteImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

        }
        if (Footer::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('footer-logo')->with('success', 'Logo has been updated successfully');


        }

    }

    public function deleteFooterLogo(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Footer::findOrFail($criteria);
        if ($this->deleteImage($criteria) && ($findData->delete())) {

            return redirect()->route('footer-logo-table')->with('success', 'Footer Logo has been deleted');
        }
    }

    public function updateFooterLogoStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $logo = Footer::findorFail($criteria);

            if (isset($_POST['active'])) {
                $logo->status = '0';
            }
            if (isset($_POST['inactive'])) {
                $logo->status = '1';
            }

            if ($logo->update()) {
                return redirect()->route('footer-logo-table')->with('success', 'Logo Status has been updated successfully');
            }
        }

        return false;
    }
}
