<?php

namespace App\Http\Controllers\backend;

use App\ClientTestimony;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientTestimonyController extends BackendController
{
    public function index()
    {
        $ClientTestimonyData = ClientTestimony::orderBy('id', 'DESC')->get();
        $this->data('ClientTestimonyData', $ClientTestimonyData);
        return view($this->pagePath . '.client-testimony.show-client-testimonies', $this->data);
    }
    
     public function ClientTestimonyTable()
    {
        $ClientTestimonyData = ClientTestimony::orderBy('id', 'DESC')->get();
        $this->data('ClientTestimonyData', $ClientTestimonyData);
        return view($this->pagePath . '.client-testimony.show-client-testimonies-table', $this->data);
    }

    public function addClientTestimony(Request $request)
    {
        if ($request->isMethod('get')) {

            return redirect()->back();
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required',
                'preview_image' => 'required|image',
            ]);

            $data['title'] = $request->title;
            $data['link'] = $request->link;

            //file upload...
            if ($request->hasFile('preview_image')) {
                $file = $request->file('preview_image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/client-testimony');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview_image'] = $imageName;

            }

            //file upload...
            if ($request->hasFile('video')) {
                $file = $request->file('video');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/videos/testimonies');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['video'] = $imageName;

            }

            if (ClientTestimony::create($data)) {
                return redirect()->route('client-testimonies')->with('success', 'Client Testimonial has been added');

            }
        }
    }

    public function updateClientTestimonyStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {

            $criteria = $request->criteria;
            $testimony = ClientTestimony::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $testimony->status = 1;
            }
            if (isset($_POST['active'])) {
                $testimony->status = 0;
            }

            if ($testimony->update()) {
                return redirect()->route('client-testimonies')->with('success', 'Client Testimony Status has been changed successfully');
            }
        }

        return false;
    }

    public function deleteVideo($id)
    {
        $criteria = $id;
        $findData = ClientTestimony::findOrFail($criteria);
        $filename = $findData->video;
        $deletePath = public_path('uploads/videos/testimonies/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deletePreviewImage($id)
    {
        $criteria = $id;
        $findData = ClientTestimony::findOrFail($criteria);
        $filename = $findData->preview_image;
        $deletePath = public_path('uploads/images/client-testimony/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteClientTestimony(Request $request)
    {
        $criteria = $request->criteria;
        $findData = ClientTestimony::findOrFail($criteria);

        if ($this->deletePreviewImage($criteria) && $this->deleteVideo($criteria) && ($findData->delete())) {

            return redirect()->route('client-testimonies')->with('success', 'Testimony has been deleted');
        }

    }

    public function editClientTestimony(Request $request)
    {

        $criteria = $request->criteria;
        $findData = ClientTestimony::findOrFail($criteria);
        $this->data('ClientTestimonyData', $findData);
        return view($this->pagePath . '.client-testimony.edit-client-testimony', $this->data);

    }


    public function editClientTestimonyAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required',
                'preview_image' => 'image',
            ]);

            $criteria = $request->criteria;

            $data['title'] = $request->title;
            $data['link'] = $request->link;

            //file upload...
            if ($request->hasFile('preview_image')) {
                $file = $request->file('preview_image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/client-testimony');
                if ($this->deletePreviewImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview_image'] = $imageName;

            }

            //file upload...
            if ($request->hasFile('video')) {
                $file = $request->file('video');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/videos/testimonies');
                if ($this->deleteVideo($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['video'] = $imageName;

            }

        }
        if (ClientTestimony::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('client-testimonies')->with('success', 'Testimony has been updated successfully');


        }
    }
}
