<?php

namespace App\Http\Controllers\backend;

use App\Term;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermsController extends BackendController
{
    public function index()
    {

        $termsData = Term::orderBy('id', 'DESC')->get();
        $this->data('termsData', $termsData);
        return view($this->pagePath . '.terms & conditions.show-terms', $this->data);
    }


    public function addTerms(Request $request)
    {
        if ($request->isMethod('get')) {
            $this->data('termsData', Term::all());
            return view($this->pagePath . '.terms & conditions.add-terms', $this->data);
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'title' => 'required',
            ]);

            $data['title'] = $request->title;


            if (Term::create($data)) {
                return redirect()->route('terms')->with('success', 'Terms & Conditions has been submitted successfully');

            }
        }

    }

    public function deleteTerms(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Term::findOrFail($criteria);
        if ($findData->delete()) {

            return redirect()->route('terms')->with('success', 'Terms & Condition has been deleted');
        }

    }

    public function editTerms(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Term::findOrFail($criteria);
        $this->data('termsData', $findData);
        return view($this->pagePath . '.terms & conditions.edit-terms', $this->data);
    }

    public function editTermsAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'title' => 'required',
            ]);

            $data['title'] = $request->title;

        }
        if (Term::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('terms')->with('success', 'Terms & Conditions has been updated');
        }

    }

    public function updateTermsStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $term = Term::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $term->status = 1;
            }
            if (isset($_POST['active'])) {
                $term->status = 0;
            }

            if ($term->update()) {
                return redirect()->route('terms')->with('success', 'Terms & Conditions Status has been changed successfully');
            }
        }

        return false;
    }
}
