<?php

namespace App\Http\Controllers\backend;

use App\Logo;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends BackendController
{
    public function index()
    {
        //just to display the logo in banner page of backend
        $logoData = Logo::orderBy('id', 'DESC')->get();
        $this->data('logoData', $logoData);

        //just to display the video in banner page of backend
        $videoData = Video::orderBy('id', 'DESC')->take(1)->get();
        $this->data('videoData', $videoData);


        return view($this->pagePath . '.home.banner.show-banner', $this->data);
    }

    //for site logo
    public function addLogo(Request $request)
    {
        if ($request->isMethod('get')) {
            $logoData = Logo::orderBy('id', 'DESC')->get();
            $this->data('logoData', $logoData);
            return view($this->pagePath . '.home.banner.show-logos', $this->data);
        }

        if ($request->isMethod('post')) {


            //file upload...
            if ($request->hasFile('logo')) {
                $file = $request->file('logo');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/logo');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['logo'] = $imageName;

            }


            if (Logo::create($data)) {
                return redirect()->route('logos')->with('success', 'Website logo have been added');

            }
        }

    }

    public function showLogos()
    {
        $logoData = Logo::orderBy('id', 'DESC')->get();
        $this->data('logoData', $logoData);
        return view($this->pagePath . '.home.banner.show-logos', $this->data);
    }

    public function deleteLogo($id)  //to delete previous logo while changing the logo
    {
        $criteria = $id;
        $findData = Logo::findOrFail($criteria);
        $filename = $findData->logo;
        $deletePath = public_path('uploads/images/logo/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function editLogoAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            //file upload...
            if ($request->hasFile('logo')) {
                $file = $request->file('logo');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/logo');
                if ($this->deleteLogo($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['logo'] = $imageName;

            }

        }
        if (Logo::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('banner')->with('success', 'Logo has been updated successfully');


        }

    }

    public function deleteBannerLogo(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Logo::findOrFail($criteria);
        if ($this->deleteLogo($criteria) && ($findData->delete())) {

            return redirect()->route('logos')->with('success', 'Banner Logo has been deleted');
        }
    }

    public function updateLogoStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $logo = Logo::findorFail($criteria);

            if (isset($_POST['active'])) {
                $logo->status = '0';
            }
            if (isset($_POST['inactive'])) {
                $logo->status = '1';
            }

            if ($logo->update()) {
                return redirect()->route('logos')->with('success', 'Logo Status has been updated successfully');
            }
        }

        return false;
    }

    //for background video
    public function addVideo(Request $request)
    {
        if ($request->isMethod('get')) {
            $videoData = Video::orderBy('id', 'DESC')->get();
            $this->data('videoData', $videoData);
            return view($this->pagePath . '.home.banner.show-videos', $this->data);
        }

        if ($request->isMethod('post')) {

            //file upload...
            if ($request->hasFile('video')) {
                $file = $request->file('video');
                $ext = $file->getClientOriginalExtension();
                $videoName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/videos/home/banner');
                if (!$file->move($uploadPath, $videoName)) {
                    return redirect()->back();
                }

                $data['video'] = $videoName;

            }


            if (Video::create($data)) {
                return redirect()->route('videos')->with('success', 'Video has been added');

            }
        }

    }

    public function showVideos()
    {
        $videoData = Video::orderBy('id', 'DESC')->get();
        $this->data('videoData', $videoData);
        return view($this->pagePath . '.home.banner.show-videos', $this->data);
    }

    public function deleteVideo($id)  //to delete previous video while changing the video
    {
        $criteria = $id;
        $findData = Video::findOrFail($criteria);
        $filename = $findData->video;
        $deletePath = public_path('uploads/videos/home/banner/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function editVideoAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            //file upload...
            if ($request->hasFile('video')) {
                $file = $request->file('video');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/videos/home/banner');
                if ($this->deleteVideo($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['video'] = $imageName;

            }
        }
        if (Video::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('banner')->with('success', 'Video has been changed successfully');


        }

    }

    public function updateVideoStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $video = Video::findorFail($criteria);

            if (isset($_POST['active'])) {
                $video->status = '0';
            }
            if (isset($_POST['inactive'])) {
                $video->status = '1';
            }

            if ($video->update()) {
                return redirect()->route('videos')->with('success', 'Video Status has been updated successfully');
            }
        }

        return false;
    }

    public function deleteBannerVideo(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Video::findOrFail($criteria);
        if ($this->deleteVideo($criteria) && ($findData->delete())) {

            return redirect()->route('videos')->with('success', 'Video has been deleted');
        }
    }


}
