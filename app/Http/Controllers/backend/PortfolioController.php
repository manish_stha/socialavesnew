<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Portfolio;
use App\PortfolioGallery;
use App\PortfolioGallery1;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PortfolioController extends Controller
{
    protected $backendPath = 'backend.';
    protected $pagePath = '';

    protected $portfolio = null;

    public function __construct(Portfolio $portfolio)
    {
        $this->pagePath = $this->backendPath . 'pages';

        $this->portfolio = $portfolio;

    }


    public function index()
    {
        $portfolioData = Portfolio::orderBy('id', 'DESC')->paginate(15);
        $this->data('portfolioData', $portfolioData);

        return view($this->pagePath . '.portfolio.show-portfolio', $this->data);
    }

    public function ShowPortfolioList()
    {
        $portfolioData = Portfolio::orderBy('id', 'DESC')->get();
        $this->data('portfolioData', $portfolioData);

        return view($this->pagePath . '.portfolio.show-portfolio-table', $this->data);
    }


    public function addPortfolio(Request $request)
    {
        if ($request->isMethod('get')) {

            return view($this->pagePath . '.portfolio.add-portfolio', $this->data);
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                    'title' => 'required',
                    'preview_image' => 'required|image',
                    'thumbnail_image' => 'sometimes|image',
                    'subtitle' => 'required',
                    'category' => 'required',
                    'gallery1.*' => 'sometimes|image|max:20000',
                    'gallery.*' => 'sometimes|image|max:20000'
                ]
                ,
                [
                    'gallery1.*.image' => 'The file you choosed is not acceptable.',
                    'gallery.*.image' => 'The file you choosed for gallery is not acceptable.'
                ]
            );

            $data['title'] = $request->title;
            $data['category'] = $request->category;
            $data['subtitle'] = $request->subtitle;
            $data['client'] = $request->client;
            $data['url'] = $request->url;
            $data['platform'] = $request->platform;
            $data['role'] = $request->role;
            $data['description'] = $request->description;
            $data['link'] = $request->link;
            $data['videotext'] = $request->videotext;


            $data = $request->except('gallery1', 'gallery');


            //file upload...
            if ($request->hasFile('preview_image')) {
                $file = $request->file('preview_image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/preview_image');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview_image'] = $imageName;

            }

            //file upload...
            if ($request->hasFile('singleimage')) {
                $file = $request->file('singleimage');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/single-image');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['singleimage'] = $imageName;

            }

            //video upload...
            if ($request->hasFile('video')) {
                $file = $request->file('video');
                $ext = $file->getClientOriginalExtension();
                $videoName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/videos/portfolio');
                if (!$file->move($uploadPath, $videoName)) {
                    return redirect()->back();
                }

                $data['video'] = $videoName;

            }

            //file upload...
            if ($request->hasFile('thumbnail_image')) {
                $file = $request->file('thumbnail_image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/thumbnail_image');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['thumbnail_image'] = $imageName;

            }


            //slider upload...
            if ($request->hasFile('slider1')) {
                $file = $request->file('slider1');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider1'] = $imageName;
            }

            //slider upload...
            if ($request->hasFile('slider2')) {
                $file = $request->file('slider2');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider2'] = $imageName;
            }

            //slider upload...
            if ($request->hasFile('slider3')) {
                $file = $request->file('slider3');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider3'] = $imageName;
            }

            //slider upload...
            if ($request->hasFile('slider4')) {
                $file = $request->file('slider4');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider4'] = $imageName;
            }

            //slider upload...
            if ($request->hasFile('slider5')) {
                $file = $request->file('slider5');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider5'] = $imageName;
            }

            $this->portfolio->fill($data);
            $status = $this->portfolio->save();
            if ($status) {

                //to add gallery through add portfolio form
                if ($request->gallery) {
                    foreach ($request->gallery as $uploaded_gallery) {
                        $gallery_name = uploadImage($uploaded_gallery, 'images/portfolio/gallery');
                        if ($gallery_name) {
                            $gallery_data = array(
                                'portfolio_id' => $this->portfolio->id,
                                'gallery' => $gallery_name
                            );
                            $gallery_obj = new PortfolioGallery();
                            $gallery_obj->fill($gallery_data);
                            $gallery_obj->save();
                        }
                    }
                }

                //to add gallery1 through add portfolio form
                if ($request->gallery1) {
                    foreach ($request->gallery1 as $uploaded_gallery) {
                        $gallery_name = uploadImage($uploaded_gallery, 'images/portfolio/gallery1');
                        if ($gallery_name) {
                            $gallery_data = array(
                                'portfolio_id' => $this->portfolio->id,
                                'gallery1' => $gallery_name
                            );
                            $gallery_obj = new PortfolioGallery1();
                            $gallery_obj->fill($gallery_data);
                            $gallery_obj->save();
                        }
                    }
                }

                return redirect()->route('portfolios')->with('success', 'Portfolio has been created');

            }

        }
    }

    public function updatePortfolioStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $data = Portfolio::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $data->status = 1;
            }
            if (isset($_POST['active'])) {
                $data->status = 0;
            }

            if ($data->update()) {
                return redirect()->back()->with('success', 'Portfolio Status has been changed successfully');
            }
        }

        return false;
    }


    public function deletePreviewImage($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->preview_image;
        $deletePath = public_path('uploads/images/portfolio/preview_image/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteSingleImage($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->singleimage;
        $deletePath = public_path('uploads/images/portfolio/single-image/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteVideo($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->video;
        $deletePath = public_path('uploads/videos/portfolio/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteThumbnailImage($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->thumbnail_image;
        $deletePath = public_path('uploads/images/portfolio/thumbnail_image/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteSlider1($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->slider1;
        $deletePath = public_path('uploads/images/portfolio/sliders/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteSlider2($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->slider2;
        $deletePath = public_path('uploads/images/portfolio/sliders/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteSlider3($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->slider3;
        $deletePath = public_path('uploads/images/portfolio/sliders/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteSlider4($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->slider4;
        $deletePath = public_path('uploads/images/portfolio/sliders/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteSlider5($id)
    {
        $criteria = $id;
        $findData = Portfolio::findOrFail($criteria);
        $filename = $findData->slider5;
        $deletePath = public_path('uploads/images/portfolio/sliders/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }


    //to delete gallery images from show portfolio table itself

    public function deletePortfolio(Request $request)
    {
        $criteria = $request->criteria;

        $this->portfolio = $this->portfolio->with('gallery', 'gallery1')->find($request->criteria);
        $findData = Portfolio::findOrFail($criteria);

        $gallery = $this->portfolio->gallery;

        $gallery1 = $this->portfolio->gallery1;

        $del = $this->deletePreviewImage($criteria) && $this->deleteSingleImage($criteria) && $this->deleteVideo($criteria) && $this->deleteThumbnailImage($criteria) && $this->deleteSlider1($criteria) && $this->deleteSlider2($criteria) && $this->deleteSlider3($criteria) && $this->deleteSlider4($criteria) && $this->deleteSlider5($criteria) && ($findData->delete());

        if ($del) {

            //Delete Gallery Images
            if ($gallery->count()) {
                foreach ($gallery as $deleteGallery) {
                    deleteImage($deleteGallery->gallery, 'images/portfolio/gallery');
                }
            }

            //Delete Gallery1 Images
            if ($gallery1->count()) {
                foreach ($gallery1 as $deleteGallery) {
                    deleteImage($deleteGallery->gallery1, 'images/portfolio/gallery1');
                }
            }
            return redirect()->back()->with('success', 'Portfolio has been deleted');

        }

    }

    public function editPortfolio(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Portfolio::findOrFail($criteria);
        $this->data('portfolioData', $findData);

        //to fetch portfolio related gallery
        $galleryData = PortfolioGallery::where('portfolio_id', '=', $findData->id)->orderBy('id', 'ASC')->get();
        $this->data('galleryData', $galleryData);

        //to fetch portfolio related gallery1
        $FirstGalleryData = PortfolioGallery1::where('portfolio_id', '=', $findData->id)->orderBy('id', 'ASC')->get();
        $this->data('FirstGalleryData', $FirstGalleryData);


        return view($this->pagePath . '.portfolio.edit-portfolio', $this->data);
    }

    public function editPortfolioAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                    'title' => 'required',
                    'preview_image' => 'image',
                    'thumbnail_image' => 'sometimes|image',
                    'subtitle' => 'required',
                    'category' => 'required',
                    'gallery1.*' => 'sometimes|image|max:10000',
                    'gallery.*' => 'sometimes|image|max:10000'
                ]
                ,
                [
                    'gallery1.*.image' => 'The file you choosed is not acceptable.',
                    'gallery.*.image' => 'The file you choosed for gallery is not acceptable.'
                ]
            );

            $criteria = $request->criteria;

            //finds criteria related row from the table
            $this->portfolio = $this->portfolio->with('gallery1', 'gallery')->find($request->criteria);

            if (!$this->portfolio) {
                request()->session()->flash('error', "Portfolio does not exist");
                return redirect()->back();
            }

            $data = $request->except('gallery1', 'gallery');

            $data['title'] = $request->title;
            $data['category'] = $request->category;
            $data['subtitle'] = $request->subtitle;
            $data['client'] = $request->client;
            $data['url'] = $request->url;
            $data['platform'] = $request->platform;
            $data['role'] = $request->role;
            $data['description'] = $request->description;
            $data['link'] = $request->link;
            $data['videotext'] = $request->videotext;


            //file upload...
            if ($request->hasFile('preview_image')) {
                $file = $request->file('preview_image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/preview_image');
                if ($this->deletePreviewImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['preview_image'] = $imageName;

            }


            //file upload...
            if ($request->hasFile('singleimage')) {
                $file = $request->file('singleimage');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/single-image');
                if ($this->deleteSingleImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['singleimage'] = $imageName;

            }

            //video upload...
            if ($request->hasFile('video')) {
                $file = $request->file('video');
                $ext = $file->getClientOriginalExtension();
                $videoName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/videos/portfolio');
                if ($this->deleteVideo($criteria) && !$file->move($uploadPath, $videoName)) {
                    return redirect()->back();
                }

                $data['video'] = $videoName;

            }

            //file upload...
            if ($request->hasFile('thumbnail_image')) {
                $file = $request->file('thumbnail_image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/thumbnail_image');
                if ($this->deleteThumbnailImage($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['thumbnail_image'] = $imageName;

            }

            //slider upload...
            if ($request->hasFile('slider1')) {
                $file = $request->file('slider1');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if ($this->deleteSlider1($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider1'] = $imageName;
            }

            //slider upload...
            if ($request->hasFile('slider2')) {
                $file = $request->file('slider2');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if ($this->deleteSlider2($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider2'] = $imageName;
            }

            //slider upload...
            if ($request->hasFile('slider3')) {
                $file = $request->file('slider3');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if ($this->deleteSlider3($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider3'] = $imageName;
            }

            //slider upload...
            if ($request->hasFile('slider4')) {
                $file = $request->file('slider4');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if ($this->deleteSlider4($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider4'] = $imageName;
            }

            //slider upload...
            if ($request->hasFile('slider5')) {
                $file = $request->file('slider5');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/portfolio/sliders');
                if ($this->deleteSlider5($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['slider5'] = $imageName;
            }


        }

        $this->portfolio->fill($data);
        $status = $this->portfolio->save();
        if ($status) {

            //to update gallery through add portfolio form
            if ($request->gallery) {
                foreach ($request->gallery as $uploaded_gallery) {
                    $gallery_name = uploadImage($uploaded_gallery, 'images/portfolio/gallery');
                    if ($gallery_name) {
                        $gallery_data = array(
                            'portfolio_id' => $this->portfolio->id,
                            'gallery' => $gallery_name
                        );
                        $gallery_obj = new PortfolioGallery();
                        $gallery_obj->fill($gallery_data);
                        $gallery_obj->save();
                    }
                }
            }

            //to add gallery1 through add portfolio form
            if ($request->gallery1) {
                foreach ($request->gallery1 as $uploaded_gallery) {
                    $gallery_name = uploadImage($uploaded_gallery, 'images/portfolio/gallery1');
                    if ($gallery_name) {
                        $gallery_data = array(
                            'portfolio_id' => $this->portfolio->id,
                            'gallery1' => $gallery_name
                        );
                        $gallery_obj = new PortfolioGallery1();
                        $gallery_obj->fill($gallery_data);
                        $gallery_obj->save();
                    }
                }
            }

            return redirect()->route('portfolios')->with('success', 'Portfolio has been updated');

        }


    }
}
