<?php

namespace App\Http\Controllers\backend;

use App\HomeServiceHeader;
use App\HomeTeamHeader;
use App\Testimony;
use Illuminate\Http\Request;

class TestimonyController extends BackendController
{
    public function index()
    {
        //to fetch team header
        $teamHeaderData = HomeTeamHeader::orderBy('id', 'DESC')->get();
        $this->data('teamHeaderData', $teamHeaderData);

        $testimonyData = Testimony::orderBy('id', 'ASC')->paginate(15);
        $this->data('testimonyData', $testimonyData);
        return view($this->pagePath . '.testimony.show-testimonies', $this->data);
    }

    //Edit Team Header
    public function editTeamHeaderAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'title' => 'required',
                'subtitle' => 'required'
            ]);

            $data['title'] = $request->title;
            $data['subtitle'] = $request->subtitle;

        }
        if (HomeTeamHeader::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('testimonies')->with('success', 'Team Header has been updated');
        }

    }

    public function TestimonyTable()
    {
        $testimonyData = Testimony::orderBy('id', 'DESC')->get();
        $this->data('testimonyData', $testimonyData);
        return view($this->pagePath . '.testimony.show-team-testimonies-table', $this->data);
    }

    public function addTestimony(Request $request)
    {
        if ($request->isMethod('get')) {
            $this->data('testimonyData', Testimony::all());
            return view($this->pagePath . '.testimony.add-testimony', $this->data);
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'testimony' => 'required',
            ]);

            $data['name'] = $request->name;
            $data['testimony'] = $request->testimony;
            $data['designation'] = $request->designation;
            
            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;   //imageko name override nahos vanera md5 gareko ani chadai name change garnalai microtime
                $uploadPath = public_path('uploads/images/our-testimony');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

            if (Testimony::create($data)) {
                return redirect()->route('testimonies')->with('success', 'Testimony has been added');

            }
        }
    }

    public function updateTestimonyStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $testimony = Testimony::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $testimony->status = 1;
            }
            if (isset($_POST['active'])) {
                $testimony->status = 0;
            }

            if ($testimony->update()) {
                return redirect()->back()->with('success', 'Testimony Status has been changed successfully');
            }
        }

        return false;
    }

    public function deleteFile($id)
    {
        $criteria = $id;
        $findData = Testimony::findOrFail($criteria);
        $filename = $findData->image;
        $deletePath = public_path('uploads/images/our-testimony/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;  //image nahuda ni delete garna
    }

    public function deleteTestimony(Request $request)
    {
        $criteria = $request->criteria;

        $findData = Testimony::findOrFail($criteria);
        if ($this->deleteFile($criteria) && ($findData->delete())) {

            return redirect()->back()->with('success', 'Testimony has been deleted');
        }

    }

    public function editTestimony(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Testimony::findOrFail($criteria);
        $this->data('testimonyData', $findData);
        return view($this->pagePath . '.testimony.edit-testimony', $this->data);
    }

    public function editTestimonyAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {  //form submit garna lagda..
            $criteria = $request->criteria;

            $this->validate($request, [
                'testimony' => 'required',
            ]);

            $data['name'] = $request->name;
            $data['testimony'] = $request->testimony;
            $data['designation'] = $request->designation;
            


            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/our-testimony');
                if ($this->deleteFile($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

        }
        if (Testimony::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('testimonies')->with('success', 'Testimony has been updated');


        }

    }


}
