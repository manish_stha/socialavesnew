<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    public function login(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('admin-auth.admin-login');
        }

        if ($request->isMethod('post')) {
            $email = $request->email;
            $password = $request->password;
            $rememberMe = isset($request->remember) ? true : false;


            if (Auth::guard('admin')->attempt(['email' => $email, 'password' => $password], $rememberMe)) {

                $userStatus = Auth::guard('admin')->user()->status;
                if ($userStatus == 1)
                    return redirect()->intended(route('dashboard'));
                else {
                    return redirect()->back()->with('error', 'Your access to the dashboard has expired');

                }

            } else {
                return redirect()->back()->with('error', 'Username and Password is not correct');

            }
        }
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->intended(route('admin-login'));
    }
}
