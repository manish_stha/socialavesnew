<?php

namespace App\Http\Controllers\backend;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends BackendController
{
    public function index()
    {
        $postData = Post::orderBy('id', 'DESC')->get();
        $this->data('postData', $postData);
        return view($this->pagePath . '.career.post.show-posts', $this->data);
    }

    public function addPost(Request $request)
    {
        if ($request->isMethod('get')) {
            return view($this->pagePath . '.career.post.add-post', $this->data);
        }

        if ($request->isMethod('post')) {  //form submit garna lagda..
            $this->validate($request, [
                'job_type' => 'required',
                'about' => 'required',
                'works' => 'required',
                'needs' => 'required',
                'bonus_skills' => 'required',
            ]);

            $data['title'] = $request->title;
            $data['number'] = $request->number;
            $data['job_type'] = $request->job_type;
            $data['description'] = $request->description;
            $data['about'] = $request->about;
            $data['works'] = $request->works;
            $data['needs'] = $request->needs;
            $data['bonus_skills'] = $request->bonus_skills;
        }

        if (Post::create($data)) {
            return redirect()->route('posts')->with('success', 'Post has been added');

        }
    }

    public function updatePostStatus(Request $request)
    {

        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $blog = Post::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $blog->status = 1;
            }
            if (isset($_POST['active'])) {
                $blog->status = 0;
            }

            if ($blog->update()) {
                return redirect()->route('posts')->with('success', 'Post Status has been changed successfully');
            }
        }

        return false;

    }

    public function deletePost(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Post::findOrFail($criteria);
        if ($findData->delete()) {

            return redirect()->route('posts')->with('success', 'Post has been deleted');
        }

    }

    public function editPost(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Post::findOrFail($criteria);
        $this->data('postData', $findData);
        return view($this->pagePath . '.career.post.edit-post', $this->data);


    }

    public function editPostAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {  //form submit garna lagda..
            $criteria = $request->criteria;

            $this->validate($request, [
                'job_type' => 'required',
                'about' => 'required',
                'works' => 'required',
                'needs' => 'required',
                'bonus_skills' => 'required',
            ]);

            $data['title'] = $request->title;
            $data['number'] = $request->number;
            $data['job_type'] = $request->job_type;
            $data['description'] = $request->description;
            $data['about'] = $request->about;
            $data['works'] = $request->works;
            $data['needs'] = $request->needs;
            $data['bonus_skills'] = $request->bonus_skills;


            if (Post::where('id', '=', $criteria)->update($data)) {
                return redirect()->route('posts')->with('success', 'Post has been updated');


            }
        }

    }


}
