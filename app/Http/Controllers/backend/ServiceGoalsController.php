<?php

namespace App\Http\Controllers\backend;

use App\Service;
use App\ServiceGoal;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceGoalsController extends BackendController
{
    public function index()
    {
        $GoalData = ServiceGoal::orderBy('id', 'ASC')->get();
        $this->data('GoalData', $GoalData);
        return view($this->pagePath . '.services.goals.show-goals', $this->data);

    }

    public function addServiceGoals(Request $request)
    {
        if ($request->isMethod('get')) {

            $criteria = $request->criteria;
            $findata = Service::findOrFail($criteria);
            $this->data('ServiceData', $findata);
            return view($this->pagePath . '.services.goals.add-goals', $this->data);
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'image' => 'required|image',
            ]);

            $data['service_id'] = $request->service_id;
            $data['title'] = $request->title;
            $data['description'] = $request->description;

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/goals');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

            if (ServiceGoal::create($data)) {
                return redirect()->back()->with('success', 'Service Contents have been added');

            }
        }
    }

    public function updateServiceGoalsStatus(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;
            $service = ServiceGoal::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $service->status = 1;
            }
            if (isset($_POST['active'])) {
                $service->status = 0;
            }

            if ($service->update()) {
                return redirect()->back()->with('success', 'Status has been changed successfully');
            }
        }

        return false;
    }

    public function deleteFile($id)
    {
        $criteria = $id;
        $findData = ServiceGoal::findOrFail($criteria);
        $filename = $findData->image;
        $deletePath = public_path('uploads/images/services/goals/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteServiceGoals(Request $request)
    {
        $criteria = $request->criteria;
        $findData = ServiceGoal::findOrFail($criteria);
        if ($this->deleteFile($criteria) && $findData->delete()) {

            return redirect()->back()->with('success', 'Service Goal has been deleted');
        }
    }

    public function editServiceGoals(Request $request)
    {
        $criteria = $request->criteria;
        $GoalData = ServiceGoal::findOrFail($criteria);
        $this->data('GoalData', $GoalData);
        return view($this->pagePath . '.services.goals.edit-goals', $this->data);
    }

    public function editServiceGoalsAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'image' => 'image',
            ]);

            $data['title'] = $request->title;
            $data['description'] = $request->description;


            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/services/goals');
                if ($this->deleteFile($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }
        }

        if (ServiceGoal::where('id', '=', $criteria)->update($data)) {
            return redirect()->route('service-goals')->with('success', 'Service Content has been updated');


        }

    }

}
