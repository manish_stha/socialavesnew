<?php

namespace App\Http\Controllers\backend;

use App\PortfolioGallery1;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfolioGallery1Controller extends Controller
{
    public function deleteGallery($id)
    {
        $criteria = $id;
        $findData = PortfolioGallery1::findOrFail($criteria);
        $filename = $findData->gallery1;
        $deletePath = public_path('uploads/images/portfolio/gallery1/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deletePortfolioGallery(Request $request)
    {
        $criteria = $request->criteria;
        $findData = PortfolioGallery1::findOrFail($criteria);
        if ($this->deleteGallery($criteria) && $findData->delete()) {

            return redirect()->back()->with('success', 'File has been deleted');
        }

    }
}
