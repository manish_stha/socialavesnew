<?php

namespace App\Http\Controllers\backend;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends BackendController
{
    public function index()
    {

        $contactData = Contact::orderBy('id', 'DESC')->get();
        $this->data('contactData', $contactData);
        return view($this->pagePath . '.contact.show-contacts', $this->data);
    }




    public function deleteContact(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Contact::findOrFail($criteria);
        if ($findData->delete()) {

            return redirect()->route('contacts')->with('success', 'Contact has been deleted');
        }

    }

    public function viewContact(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Contact::findOrFail($criteria);
        $this->data('contactData', $findData);
        return view($this->pagePath . '.contact.view-contact', $this->data);
    }

}
