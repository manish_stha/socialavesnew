<?php

namespace App\Http\Controllers\backend;

use App\Blog;
use App\Quote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuoteController extends BackendController
{
    public function index()
    {
        $quoteData = Quote::orderBy('id', 'DESC')->get();
        $this->data('quoteData', $quoteData);
        return view($this->pagePath . '.blog.quote.show-quotes', $this->data);
    }

    public function addQuote(Request $request)
    {
        if ($request->isMethod('get')) {
            $this->data('blogData', Blog::all());
            $this->data('quoteData', Quote::all());
            return view($this->pagePath . '.blog.quote.add-quote', $this->data);
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'quote' => 'required',
            ]);

            $data['blog_id'] = $request->title;  //input ko dropdown options ko value ma blog_id pass gareko hunchha//yata name matra title ho..
            $data['quote'] = $request->quote;
            $data['writer'] = $request->writer;



            if (Quote::create($data)) {
                return redirect()->route('quotes')->with('success', 'Quote has been added');

            }
        }

    }

    public function deleteQuote(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Quote::findOrFail($criteria);
        if ($findData->delete()) {

            return redirect()->route('quotes')->with('success', 'Quote has been deleted');
        }

    }

    public function editQuote(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Quote::findOrFail($criteria);
        $this->data('quoteData', $findData);
        $this->data('blogData', Blog::all());
        return view($this->pagePath . '.blog.quote.edit-quote', $this->data);


    }

    public function editQuoteAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {  //form submit garna lagda..
            $criteria = $request->criteria;

            $data['blog_id'] = $request->title;
            $data['quote'] = $request->quote;
            $data['writer'] = $request->writer;


            if (Quote::where('id', '=', $criteria)->update($data)) {
                return redirect()->route('quotes')->with('success', 'Quote has been updated');


            }
        }

    }

    public function updateQuoteStatus(Request $request)
    {
        if ($request->isMethod('get')) {  //get bata aauchha bhane return back
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;    //criteria bhanne name deko chha hidden method bata aako ho jun chai hamro id ho..
            $blog = Quote::findorFail($criteria);

            if (isset($_POST['inactive'])) {
                $blog->status = 1;
            }
            if (isset($_POST['active'])) {
                $blog->status = 0;
            }

            if ($blog->update()) {
                return redirect()->route('quotes')->with('success', 'Quote Status has been changed successfully');
            }
        }

        return false;
    }


}
