<?php

namespace App\Http\Controllers\backend;

use App\Fact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FactController extends BackendController
{
    public function index()
    {
        $FactData = Fact::orderBy('id', 'ASC')->get();
        $this->data('FactData', $FactData);
        return view($this->pagePath . '.home.facts.show-facts', $this->data);
    }

    public function addFact(Request $request)
    {
        if ($request->isMethod('get')) {

            return redirect()->back();
        }

        if ($request->isMethod('post')) {

            $data['title1'] = $request->title1;
            $data['number1'] = $request->number1;
            $data['title2'] = $request->title2;
            $data['number2'] = $request->number2;
            $data['title3'] = $request->title3;
            $data['number3'] = $request->number3;
            $data['title4'] = $request->title4;
            $data['number4'] = $request->number4;


            if (Fact::create($data)) {
                return redirect()->back()->with('success', 'Facts has been added');

            }
        }

    }

    public function editFactAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $data['title1'] = $request->title1;
            $data['number1'] = $request->number1;
            $data['title2'] = $request->title2;
            $data['number2'] = $request->number2;
            $data['title3'] = $request->title3;
            $data['number3'] = $request->number3;
            $data['title4'] = $request->title4;
            $data['number4'] = $request->number4;


        }

        if (Fact::where('id', '=', $criteria)->update($data)) {
            return redirect()->back()->with('success', 'Facts has been updated');


        }

    }
}
