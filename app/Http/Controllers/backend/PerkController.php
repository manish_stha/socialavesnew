<?php

namespace App\Http\Controllers\backend;

use App\Perk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PerkController extends BackendController
{
    public function index()
    {
        $perkData = Perk::orderBy('id', 'ASC')->get();
        $this->data('perkData', $perkData);
        return view($this->pagePath . '.career.work-perks.show-perks', $this->data);
    }

    //to display perks in table
    public function ShowPerkInTable()
    {
        $perkData = Perk::orderBy('id', 'DESC')->get();
        $this->data('perkData', $perkData);
        return view($this->pagePath . '.career.work-perks.show-perks-table', $this->data);
    }

    public function addPerk(Request $request)
    {
        if ($request->isMethod('get')) {

            return view($this->pagePath . '.career.work-perks.show-perks', $this->data);
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'title' => 'required',
            ]);

            $data['title'] = $request->title;

            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/career/work-perks');
                if (!$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

            if (Perk::create($data)) {
                return redirect()->back()->with('success', 'Work has been added');

            }
        }

    }

    public function deleteFile($id)
    {
        $criteria = $id;
        $findData = Perk::findOrFail($criteria);
        $filename = $findData->image;
        $deletePath = public_path('uploads/images/career/work-perks/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }


    public function editPerkAction(Request $request)
    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $criteria = $request->criteria;

            $this->validate($request, [
                'title' => 'required',
            ]);

            $data['title'] = $request->title;


            //file upload...
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $imageName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/images/career/work-perks');
                if ($this->deleteFile($criteria) && !$file->move($uploadPath, $imageName)) {
                    return redirect()->back();
                }

                $data['image'] = $imageName;

            }

        }
        if (Perk::where('id', '=', $criteria)->update($data)) {
            return redirect()->back()->with('success', 'Work Perk has been updated');


        }

    }

    public function deletePerk(Request $request)
    {
        $criteria = $request->criteria;
        $findData = Perk::findOrFail($criteria);
        if ($this->deleteFile($criteria) && $findData->delete()) {

            return redirect()->route('work-perk-table')->with('success', 'Work Perk has been deleted');
        }

    }

}
