<?php

namespace App\Http\Controllers\backend;

use App\ServiceGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceGalleryController extends BackendController
{
    public function deleteGallery($id)
    {
        $criteria = $id;
        $findData = ServiceGallery::findOrFail($criteria);
        $filename = $findData->gallery;
        $deletePath = public_path('uploads/images/services/inner-services/gallery/' . $filename);
        if (file_exists($deletePath) && is_file($deletePath)) {
            return unlink($deletePath);
        }
        return true;
    }

    public function deleteServiceGallery(Request $request)
    {
        $criteria = $request->criteria;
        $findData = ServiceGallery::findOrFail($criteria);
        if ($this->deleteGallery($criteria) && $findData->delete()) {

            return redirect()->back()->with('success', 'File has been deleted');
        }

    }
}
