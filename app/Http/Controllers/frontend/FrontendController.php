<?php

namespace App\Http\Controllers\frontend;

use App\Blog;
use App\Brand;
use App\ClientTestimony;
use App\Footer;
use App\HomeClient;
use App\HomeServiceHeader;
use App\HomeTeamHeader;
use App\Logo;
use App\Portfolio;
use App\Service;
use App\Testimony;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    protected $frontendPath = 'frontend.';
    protected $pagePath = '';

    public function __construct()
    {
        $this->pagePath = $this->frontendPath . 'pages';

        $logoData = Logo::where('status', '=', 1)->orderBy('id', 'DESC')->take(1)->get(); //to change the banner logo
        $this->data('logoData', $logoData);

        $footerData = Footer::where('status', '=', 1)->orderBy('id', 'DESC')->take(1)->get();  //to change footer logo
        $this->data('footerData', $footerData);

        $serviceData = Service::where('status', '=', 1)->get();  //to fetch service's title in top nav of every page
        $this->data('serviceData', $serviceData);

        //to fetch service header
        $serviceHeaderData = HomeServiceHeader::orderBy('id', 'DESC')->take(1)->get();
        $this->data('serviceHeaderData', $serviceHeaderData);

        //to fetch team header
        $teamHeaderData = HomeTeamHeader::orderBy('id', 'DESC')->take(1)->get();
        $this->data('teamHeaderData', $teamHeaderData);

        $teamData = Testimony::where('status', '=', 1)->orderBy('id', 'ASC')->get();
        $this->data('teamData', $teamData);

        //Testimonials Videos
        $ClientTestimonyData = ClientTestimony::where('status', '=', 1)->inRandomOrder()->take(1)->get();
        $this->data('ClientTestimonyData', $ClientTestimonyData);

        //Text Testimonials
        $clientData = HomeClient::where('status', '=', 1)->orderBy('id', 'ASC')->get();
        $this->data('clientData', $clientData);

        //Client Brands
        $brandData = Brand::where('status', '=', 1)->orderBy('id', 'ASC')->get();
        $this->data('brandData', $brandData);

        //Portfolios in Other Pages (Except Portfolio)
        $PortfolioDataInOtherPages = Portfolio::where('status', '=', 1)->orderBy('id', 'DESC')->take(3)->get();
        $this->data('PortfolioDataInOtherPages', $PortfolioDataInOtherPages);

    }
}
