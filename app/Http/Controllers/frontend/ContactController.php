<?php

namespace App\Http\Controllers\frontend;

use App\Admin;
use App\Contact;
use App\Mail\WelcomeMail;
use App\Notifications\ApplicationReceived;
use App\Notifications\FeedbackReceived;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use UxWeb\SweetAlert\SweetAlert;

class ContactController extends FrontendController
{
    public function addContact(Request $request)
    {

        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'name' => 'required|string',
                'comment' => 'required',
                'email' => 'email|required',
                'phone' => 'required|numeric|digits:10'
            ],
                [
                    'phone.numeric' => 'Phone number must be only of digits.',
                    'phone.digits' => 'Phone number must be of 10 digits.',
                ]
            );

            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['company'] = $request->company;
            $data['comment'] = $request->comment;


            if (Contact::create($data)) {

//                Mail through blade
                Mail::to('shresthamanees@gmail.com')->send(new WelcomeMail($data));

                //Database Notification
//                Auth::guard('web')->user()->notify(new FeedbackReceived());

                //Mail Notification
//                \Illuminate\Support\Facades\Notification::route('mail', 'shresthamanees@gmail.com')
//                    ->notify(new FeedbackReceived());

//                return redirect()->back()->with('success', 'Thanks for contacting us, we will get back to you shortly.');

                SweetAlert::success('Thank you for connecting with us. We will get back to you shortly.', 'Message Sent!')->autoclose('60000');
                return redirect()->back();
            }
        }

    }
}
