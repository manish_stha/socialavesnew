<?php

namespace App\Http\Controllers\frontend;

use App\Application;
use App\Notifications\ApplicationReceived;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;

class SendApplicationController extends FrontendController
{
    public function addApplication(Request $request)

    {
        if ($request->isMethod('get')) {
            return redirect()->back();
        }


        if ($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email',
                'address' => 'required',
                'contact' => 'required|numeric|digits:10',
                'cv' => 'required|mimes:doc,pdf,docx,jpg,jpeg,png|max:25600',
            ],
                [
                    'contact.numeric' => 'Phone number must be only of digits.',
                    'contact.digits' => 'Phone number must be of 10 digits.',
                ]
            );

            $data['post_id'] = $request->post_id;
            $data['name'] = $request->name;
            $data['email'] = $request->email;
            $data['address'] = $request->address;
            $data['contact'] = $request->contact;

            //file upload...
            if ($request->hasFile('cv')) {
                $file = $request->file('cv');
                $ext = $file->getClientOriginalExtension();
                $fileName = md5(microtime()) . '.' . $ext;
                $uploadPath = public_path('uploads/application');
                if (!$file->move($uploadPath, $fileName)) {
                    return redirect()->back();
                }

                $data['cv'] = $fileName;
            }

        }

        if (Application::create($data)) {

            SweetAlert::success('Thank you for sending your application. We will get back to you shortly.', 'Application Sent!')->autoclose('60000');
            return redirect()->back();

        }

    }
}
