<?php

namespace App\Http\Controllers\frontend;

use App\Comment;
use App\Notifications\CommentReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use UxWeb\SweetAlert\SweetAlert;


class CommentController extends FrontendController
{
    public function addComment(Request $request)
    {

        if ($request->isMethod('get')) {
            return redirect()->back();
        }

        if ($request->isMethod('post')) {

            $this->validate($request, [
                'name' => 'required',
                'email' => 'email|required',
                'comment' => 'required',
            ]);


            $comment = new Comment;

            $comment->blog_id = $request->input('blog_id');
            $comment->name = $request->input('name');
            $comment->email = $request->input('email');
            $comment->comment = $request->input('comment');

            $status = $comment->save();
            if ($status == 1) {
                SweetAlert::success('Thank you for your valuable comment', 'Hurray!!!')->autoclose('60000');
                return redirect()->back();
            }


        }

    }
}
