<?php

namespace App\Http\Controllers\frontend;

use App\Blog;
use App\Brand;
use App\ClientTestimony;
use App\Comment;
use App\Footer;
use App\HomeClient;
use App\HomeService;
use App\HomeTeam;
use App\Logo;
use App\Fact;
use App\Perk;
use App\Portfolio;
use App\PortfolioGallery;
use App\PortfolioGallery1;
use App\Post;
use App\Service;
use App\ServiceGallery;
use App\ServiceGoal;
use App\ServiceofService;
use App\Story;
use App\Testimony;
use App\Video;
use Illuminate\Http\Request;


class ApplicationController extends FrontendController
{
    public function index()
    {

        //for dynamic web title
        $title = "Social Aves | A Leading Digital Marketing Agency";
        $this->data('title', $title);

        $blogData = Blog::where('status', '=', 1)->orderBy('id', 'DESC')->take(3)->get();
        $this->data('blogData', $blogData);

        $videoData = Video::where('status', '=', 1)->orderBy('id', 'DESC')->take(1)->get();
        $this->data('videoData', $videoData);

        //to run data in owl carousel accordingly
        $clientData = HomeClient::where('status', '=', 1)->orderBy('id', 'ASC')->get();
        $this->data('clientData', $clientData);

        $FirstClientData = HomeClient::where('status', '=', 1)->get()->first();
        $this->data('FirstClientData', $FirstClientData);

        $SecondClientData = HomeClient::where('status', '=', 1)->skip(1)->first();
        $this->data('SecondClientData', $SecondClientData);

        $LastClientData = HomeClient::where('status', '=', 1)->get()->last();
        $this->data('LastClientData', $LastClientData);
        
        $FactData = Fact::orderBy('id', 'ASC')->get();
        $this->data('FactData', $FactData);

        return view($this->pagePath . '.home', $this->data);

    }

    public function ErrorInLoginPage()
    {
        //here no need to create an object cause there is no value returned in 404.blade page
        return view('errors.404');
    }


    public function Blog()
    {
        $title = "Our Blogs | Social Aves";
        $this->data('title', $title);

        $blogData = Blog::where('status', '=', 1)->orderBy('id', 'DESC')->Paginate(6);
        $this->data('blogData', $blogData);
        return view($this->pagePath . '.blogs.blog', $this->data);
    }

    //to view blog in detail
    public function ViewBlog(Request $request)
    {

        $criteria = $request->id;
        $findData = Blog::findOrFail($criteria);


        $this->data('title', $findData->title . ' | Blogs | Social Aves');  //makes blog's title dynamic

        $this->data('blogData', $findData);


        $this->data('key', Portfolio::where('id', '=', 4)->get());


        $RelatedBlogData = Blog::where('status', '=', 1)->orderBy('id', 'DESC')->take(4)->get();    //for related blog section
        $this->data('RelatedBlogData', $RelatedBlogData);

        $commentData = Comment::where('status', '=', 1)->orderBY('id', 'DESC')->get();   //to display related comments in blog's inner page
        $this->data('commentData', $commentData);

        return view($this->pagePath . '.blogs.inner-blog', $this->data);
    }

    public
    function Team()
    {
        $title = "Our Team | Social Aves";
        $this->data('title', $title);

        $CountPostData = Post::where('status', '=', 1)->count();   //counts Posts with status == 1
        $this->data('CountPostData', $CountPostData);

        $postData = Post::where('status', '=', 1)->orderBy('id', 'DESC')->take(2)->get();
        $this->data('postData', $postData);

        $teamData = Testimony::where('status', '=', 1)->orderBy('id', 'ASC')->get();
        $this->data('teamData', $teamData);
        return view($this->pagePath . '.team', $this->data);
    }

    public
    function Career()
    {
        $title = "Career | Social Aves";
        $this->data('title', $title);

        $storyData = Story::where('status', '=', 1)->take(1)->get();
        $this->data('storyData', $storyData);

        $perkData = Perk::orderBy('id', 'DESC')->get();
        $this->data('perkData', $perkData);

        $CountPostData = Post::where('status', '=', 1)->count();
        $this->data('CountPostData', $CountPostData);

        $postData = Post::where('status', '=', 1)->orderBy('id', 'DESC')->get();
        $this->data('postData', $postData);


        return view($this->pagePath . '.career.career', $this->data);
    }


    public
    function VacancyDetail(Request $request)
    {

        $criteria = $request->id;
        $findData = Post::findOrFail($criteria);

        $this->data('title', $findData->title . ' | Career | Social Aves');        //for dynamic web title

        $this->data('postData', $findData);

        return view($this->pagePath . '.career.vacancy-detail', $this->data);
    }


    public
    function Service()
    {
        $title = "Services | Social Aves";
        $this->data('title', $title);

        $serviceData = Service::where('status', '=', 1)->get();
        $this->data('serviceData', $serviceData);

        return view($this->pagePath . '.services.service', $this->data);
    }

    public
    function ServiceDetails(Request $request)
    {
        $criteria = $request->id;
        $findData = Service::findOrFail($criteria);

        $this->data('title', $findData->title . ' | Services | Social Aves');        //for dynamic web title
        $this->data('ServiceData', $findData);

        //to fetch service related goals
        $GoalData = ServiceGoal::where('service_id', '=', $findData->id)->where('status', '=', 1)->orderBy('id', 'ASC')->get();
        $this->data('GoalData', $GoalData);

        //to make nav-link active in first inner service of Content Marketing
        $FirstInnerServiceData = ServiceofService::where('status', '=', 1)->where('service_id', '=', $findData->id)->get()->first();
        $this->data('FirstInnerServiceData', $FirstInnerServiceData);

        //to fetch service related services
        $InnerServiceData = ServiceofService::where('service_id', '=', $findData->id)->where('status', '=', 1)->orderBy('id', 'ASC')->get();
        $this->data('InnerServiceData', $InnerServiceData);

        //to fetch innerservice related gallery
        $GalleryData = ServiceGallery::orderBy('id', 'DESC')->get();
        $this->data('GalleryData', $GalleryData);

        return view($this->pagePath . '.services.service-details', $this->data);
    }

    public
    function Portfolio()
    {
        $title = "Our Works | Social Aves";
        $this->data('title', $title);

        $portfolioData = Portfolio::where('status', '=', 1)->orderBy('id', 'DESC')->Paginate(12);
        $this->data('portfolioData', $portfolioData);

        return view($this->pagePath . '.portfolio.portfolio', $this->data);
    }

    public
    function PortfolioDetail(Request $request)
    {

        $criteria = $request->id;
        $findData = Portfolio::findOrFail($criteria);

        $this->data('title', $findData->title . ' | Our Works | Social Aves');        //for dynamic web title

        $this->data('portfolioData', $findData);

        //to fetch portfolio related gallery1
        $FirstGalleryData = PortfolioGallery1::where('portfolio_id', '=', $findData->id)->orderBy('id', 'ASC')->get();
        $this->data('FirstGalleryData', $FirstGalleryData);

        //to fetch portfolio related gallery
        $galleryData = PortfolioGallery::where('portfolio_id', '=', $findData->id)->orderBy('id', 'ASC')->get();
        $this->data('galleryData', $galleryData);

        //to fetch related portfolio
        $RelatedPortfolioData = Portfolio::where('status', '=', 1)->where('id', '!=', $findData->id)->inRandomOrder()->take(3)->get();
        $this->data('RelatedPortfolioData', $RelatedPortfolioData);


        return view($this->pagePath . '.portfolio.portfolio-detail', $this->data);
    }


    public
    function Client()
    {
        $title = "Client Testimony | Social Aves";
        $this->data('title', $title);

        $clientData = ClientTestimony::orderBy('id', 'DESC')->get();
        $this->data('clientData', $clientData);
        return view($this->pagePath . '.client', $this->data);
    }


    public
    function Contact()
    {
        $title = "Contact Us | Social Aves";
        $this->data('title', $title);

        return view($this->pagePath . '.contact', $this->data);
    }


}
