<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeServiceHeader extends Model
{
    protected $fillable = [
        'title',
        'subtitle',
    ];
}
