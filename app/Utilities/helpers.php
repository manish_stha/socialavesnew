<?php

if (!function_exists('uploadImage')) {
    function uploadImage($file, $dir, $thumb = false)
    {
        $path = public_path() . '/uploads/' . $dir;

        if (!File::exists($path)) {
            File::makeDirectory($path, 0777, true, true);
        }

        $image_name = ucfirst('gallery') . "-" . date('Ymdhis') . rand(0, 9999) . "." . $file->getClientOriginalExtension();
        $success = $file->move($path, $image_name);
        if ($success) {
            return $image_name;
        }
    }
}

if (!function_exists('deleteImage')) {
    function deleteImage($image_name, $dir)
    {

        $path = public_path() . '/uploads/' . $dir;
        if ($image_name != null && file_exists($path . '/' . $image_name)) {
            $status = unlink($path . '/' . $image_name);
            if ($status) {
                if (file_exists($path . '/Thumb-' . $image_name)) {
                    unlink($path . '/Thumb-' . $image_name);
                }
                return true;
            } else {
                return false;
            }
        } else {
            return false;


        }
    }
}