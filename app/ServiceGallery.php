<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGallery extends Model
{
    protected $fillable =
        [
            'innerservice_id',
            'gallery'
        ];
}
