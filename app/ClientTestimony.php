<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientTestimony extends Model
{
    protected $fillable = [
        'title',
        'preview_image',
        'link',
        'video',
        'status'
    ];
}
