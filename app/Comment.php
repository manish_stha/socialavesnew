<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'blog_id',
        'name',
        'email',
        'comment',
        'status'
    ];

    public function blog()
    {
        return $this->belongsTo('App\Blog', 'blog_id');
    }

    public function reply()
    {
        return $this->hasMany('App\Reply', 'comment_id', 'id');

    }

    //===============Any method declared as static is accessible without the creation of an object=====//
    public static function countComment($id)
    {
        $countComment = Comment::all()->where('blog_id', $id)
            ->where('status', '=', 1)  //to count comment with status 1
            ->count();
        return $countComment;
    }

}
