<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioGallery extends Model
{

    protected $fillable =
        [
            'portfolio_id',
            'gallery'
        ];
}
