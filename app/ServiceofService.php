<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceofService extends Model
{
    protected $fillable = [
        'service_id',
        'image',
        'title',
        'description',
        'link1',
        'preview1',
        'link2',
        'preview2',
        'link3',
        'preview3',
        'status',
    ];

    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }

    public function gallery()
    {
        return $this->hasMany('App\ServiceGallery', 'innerservice_id', 'id');

    }
}
