<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = [
        'post_id',
        'name',
        'email',
        'address',
        'contact',
        'cv',
    ];

    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }
}
