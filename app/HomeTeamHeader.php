<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeTeamHeader extends Model
{
    protected $fillable = [
        'title',
        'subtitle',
    ];
}
