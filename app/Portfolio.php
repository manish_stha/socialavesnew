<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
//    public function getSlug($title)
//    {
//        $slug = \Str::slug($title);
//        if ($this->where('slug', $slug)->count()) {
//            $slug .= date("Ymdhis") . rand(0, 9999);
//        }
//        return $slug;
//    }

    protected $fillable =
        [
            'title',
            'slug',
            'preview_image',
            'category',
            'subtitle',
            'client',
            'url',
            'platform',
            'role',
            'description',
            'singleimage',
            'link',
            'video',
            'thumbnail_image',
            'videotext',
            'slider1',
            'slider2',
            'slider3',
            'slider4',
            'slider5',
            'visit_counts',
            'status'
        ];

    public function gallery()
    {
        return $this->hasMany('App\PortfolioGallery', 'portfolio_id', 'id');

    }

    public function gallery1()
    {
        return $this->hasMany('App\PortfolioGallery1', 'portfolio_id', 'id');

    }
}
