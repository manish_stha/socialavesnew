<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perk extends Model
{
    protected $fillable = [
        'title',
        'image',
        'status',
    ];
}
