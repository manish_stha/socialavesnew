<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortfolioGallery1 extends Model
{
    protected $fillable =
        [
            'portfolio_id',
            'gallery1'
        ];
}
