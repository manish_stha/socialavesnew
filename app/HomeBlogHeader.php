<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeBlogHeader extends Model
{
    protected $fillable = [
        'title',
        'subtitle',
    ];
}
