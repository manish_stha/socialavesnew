{{--@extends('errors::minimal')--}}

{{--@section('title', __('Not Found'))--}}
{{--@section('code', '404')--}}
{{--@section('message', __('Not Found'))--}}


        <!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="{{url('frontend/font/fontawesome/css/all.min.css')}}">
    <!-- Animation on Scrolling -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css"/>
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <!-- For Video and Image Gallery CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"/>
    <!-- Main Stylesheet File -->
    <link href="{{url('frontend/css/main.css')}}" rel="stylesheet" type="text/css">
    <!-- Pre-Loading js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>

    <!-- Favicons -->
    <link href="{{url('frontend/images/favicon.ico')}}" rel="icon">


    <title>Page Not Found | Social Aves</title>

</head>

<body id="top" class="main-body">


<!-- Preloader -->
<div id="preloader">
    <div id="status">Make your brand talk</div>
</div>
<!-- End Preloader -->


<!-- Header Navigation -->
<section class="header-nav p-0">
    <div class="header-logo">
        <a href="{{route('index')}}" data-toggle="popover" data-trigger="hover"
           data-content="Make your brand talk">
            <img class="logo" src="{{url('frontend/images/original_logo.svg')}}" width="60%" height="auto"/>
        </a>
    </div>

    <div class="hire">
        <a class="btn btn-hire mt-0" href="{{url('career')}}">HIRE ME</a>
    </div>
    <div class="menu-text">MENU</div>

    <div class="open-menu">
        <img class="p-0" src="{{url('frontend/images/menu.svg')}}" alt=""/>
    </div>
</section>
<!-- End Header Navigation -->

<!-- ==================================
     MENU
     ==================================
-->

<!-- Menu Nav -->
<section class="nav-overlay p-0 ">
    <div class="header-logo">
        <a href="{{route('index')}}" data-toggle="popover" data-trigger="hover"
           data-content="Make your brand talk">
            <img class="logo d-inline-block align-top" src="{{url('frontend/images/original_logo.svg')}}"
                 width="60%" height="auto">
        </a>

    </div>

    <div class="close-menu">
        <img src="{{url('frontend/images/close.svg')}}" alt=""/>
    </div>

    <!-- Overlay Menu -->
    <div class="overlay-menu">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h6 class="mb-4 menu-title">Navigation</h6>
                </div>
                <div class="col-lg-6">

                    <div class="menu mb-4">
                        <ul>
                            <li><a href="{{route('service')}}" title="Our Services">Explore our services</a></li>
                            <li><a href="{{route('portfolio')}}" title="Our Works">Portfolio</a></li>
                            <li><a href="{{route('team')}}" title="Our Team">Meet our Team</a></li>
                            <li><a href="{{route('blog')}}" title="Our Blogs">Blogs</a></li>
                            <li><a href="{{route('career')}}" title="Make your career at Social Aves">Career</a>
                            </li>
                            <li><a href="{{route('contact')}}" title="Contact with us">Contact Us</a></li>
                        </ul>
                    </div>


                </div>

                <div class="col-lg-6 mt-2 mb-4">
                    <div class="contact-info">
                        <h6>CALL US TODAY</h6>
                        <h4 class="mb-0 color-red">+977-1-5537595</h4>
                        <p class="text-muted mb-4">Sunday to Friday 10:00 am – 6:00 pm</p>

                        <h6>VISIT THE OFFICE</h6>
                        <p class="text-muted mb-4">Jawalakhel Chowk, Lalitpur, Nepal<br>
                            GPO BOX No. 8354</p>

                        <h6>DROP A LINE</h6>
                        <p class=" text-muted mb-4">info@socialaves.com</p>
                    </div>

                    <!-- Menu Social -->
                    <div class="menu-social">
                        <h6>FIND US ON</h6>
                        <li><a href="https://www.facebook.com/socialaves/" target="_blank"><i
                                        class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://twitter.com/SocialAves" target="_blank"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li><a href="https://www.instagram.com/avessocial/" target="_blank"><i
                                        class="fab fa-instagram"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCJ2gYCjdce1idG5MJY4hbvw" target="_blank"><i
                                        class="fab fa-youtube"></i></a></li>
                        <li class="m-0"><a href="https://www.linkedin.com/company/social-aves-pvt-ltd-/"
                                           target="_blank"><i class="fab fa-linkedin"></i></a></li>
                    </div>
                    <!-- End Menu Social -->
                </div>

                <div class="col-lg-12">
                    <div class=" mb-4">©2020 Social Aves. All Rights Reserved. <br>Developed by <a
                                href="https://socialaves.com/" target="_blank">Social
                            Aves</a></div>
                </div>

            </div>

        </div>
    </div>
    <!-- End Overlay Menu -->


</section>
<!-- End Overlay Menu -->


<section class="notfound">
    <div class="container text-center notfound-404">
        <div class="row">
            <div class="col col-center" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="300">
                <h1>Page Not Found</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-center " data-aos="fade-up" data-aos-easing="ease" data-aos-delay="600">
                <p>The page you are looking for is not available.</p>
                <a href="{{route('index')}}">Go To Home</a>
            </div>
        </div>
    </div>
</section>


<!-- ==================================
 FOOTER
 ==================================
-->
<footer class="footer bgdark">
    <div class="container text-center">
        <div class="row">

            <div class="col-lg-12">
                <img class="col-center" src="{{url('frontend/images/footerlogo.png')}}">
            </div>

            <div class="col-lg-12 my-5">
                <div class="footer-social">
                    <li><a href="https://www.facebook.com/socialaves/" target="_blank"><i
                                    class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://twitter.com/SocialAves" target="_blank"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li><a href="https://www.instagram.com/avessocial/" target="_blank"><i
                                    class="fab fa-instagram"></i></a></li>
                    <li><a href="https://www.youtube.com/channel/UCJ2gYCjdce1idG5MJY4hbvw" target="_blank"><i
                                    class="fab fa-youtube"></i></a></li>
                    <li class="m-0"><a href="https://www.linkedin.com/company/social-aves-pvt-ltd-/"
                                       target="_blank"><i class="fab fa-linkedin"></i></a></li>
                </div>
            </div>
            <div class="col-lg-12">
                <p>©{{date('Y')}} Social Aves. All Rights Reserved. <br>Developed by <a href="{{route('index')}}">Social
                        Aves</a>
                </p>
            </div>
            <div class="go-top">
                <a class="smoothscroll" href="#top" data-toggle="popover" data-trigger="hover"
                   data-content="Back to Top"><i class="fas fa-arrow-up" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- ==================================
   END FOOTER
   ==================================
-->


<!-- AOS JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- Preloader JS -->
<script src="https://cdn.jsdelivr.net/npm/jquery-bez@1.0.11/src/jquery.bez.js"></script>
<!-- Owl Carousel JS -->
<script src="{{url('frontend/jquery/owl.carousel.js')}}"></script>
<!-- Fancy box JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js"></script>


<!-- custom script
================================================== -->
<script src="{{url('frontend/jquery/action.js')}}" charset="utf-8"></script>
<script src="{{url('frontend/custom/custom.js')}}" charset="utf-8"></script>


</body>
</html>
