<?php

//============frontend login route===============

Auth::routes();

//============frontend route===============


Route::group(['namespace' => 'frontend'], function () {

    Route::any('/', 'ApplicationController@index')->name('index');

    Route::any('/login', 'ApplicationController@ErrorInLoginPage');
    Route::any('/register', 'ApplicationController@ErrorInLoginPage');

    Route::any('/services', 'ApplicationController@Service')->name('service');
    Route::any('/services/{title}/{id?}', 'ApplicationController@ServiceDetails')->name('view-service');

    Route::any('/portfolios', 'ApplicationController@Portfolio')->name('portfolio');
    Route::any('/portfolios/{title}/{id?}', 'ApplicationController@PortfolioDetail')->name('view-portfolio');
    Route::any('/blogs', 'ApplicationController@Blog')->name('blog');
    Route::any('/blog/{title}/{id?}', 'ApplicationController@ViewBlog')->name('view-blog');
    Route::any('/our-team', 'ApplicationController@Team')->name('team');
    Route::any('/career', 'ApplicationController@Career')->name('career');


    //to display the details according to the vacancy post
    Route::any('career/{title}/{id?}', 'ApplicationController@VacancyDetail')->name('vacancy-detail');

    Route::any('/contact-us', 'ApplicationController@Contact')->name('contact');

    Route::any('add-contact', 'ContactController@addContact')->name('add-contact');


    //to submit application from frontend
    Route::any('add-application', 'SendApplicationController@addApplication')->name('add-application');

    Route::any('add-comment', 'CommentController@addComment')->name('add-comment');

    Route::any('add-reply', 'ReplyController@addReply')->name('add-reply');


});


//============backend route===============

Route::group(['namespace' => 'backend', 'prefix' => '@admin'], function () {

    //===========Routes to reset the password=====//
    Route::post('admin-password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::any('admin-password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('admin-password/reset', 'AdminAuth\ResetPasswordController@reset')->name('admin.password.request');
    Route::get('admin-password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm')->name('admin.password.reset');


    Route::any('admin-login', 'AdminLoginController@login')->name('admin-login');

    Route::group(['middleware' => 'auth:admin'], function () {
        Route::any('/', 'DashboardController@index')->name('dashboard');


        Route::any('admin-logout', 'AdminLoginController@logout')->name('admin-logout');

        //==========user routes============//
        //user_type middleware is defined on Kernel.php, a/c to which only Super Admin can modify users
        Route::group(['prefix' => 'users', 'middleware' => 'user_type'], function () {
            Route::any('/', 'AdminController@index')->name('users');
            Route::any('add-user', 'AdminController@addUser')->name('add-user');
            Route::any('delete-user/{criteria?}', 'AdminController@deleteUser')->name('delete-user');
            Route::any('update-user-status', 'AdminController@updateUserStatus')->name('update-user-status');
            Route::any('edit-user/{criteria?}', 'AdminController@editUser')->name('edit-user');
            Route::any('edit-user-action', 'AdminController@editUserAction')->name('edit-user-action');
        });

        //========homepage routes============//
        Route::group(['prefix' => 'home'], function () {

            Route::any('banner', 'BannerController@index')->name('banner');

            //logo through banner
            Route::any('banner/add-logo', 'BannerController@addLogo')->name('add-logo');
            Route::any('banner/logos', 'BannerController@showLogos')->name('logos');
            Route::any('banner/change-logo', 'BannerController@editLogoAction')->name('edit-logo-action');
            Route::any('banner/delete-logo/{criteria?}', 'BannerController@deleteBannerLogo')->name('delete-logo');
            Route::any('banner/update-logo-status', 'BannerController@updateLogoStatus')->name('update-logo-status');

            //background video through banner
            Route::any('banner/add-video', 'BannerController@addVideo')->name('add-video');
            Route::any('banner/videos', 'BannerController@showVideos')->name('videos');
            Route::any('banner/change-video', 'BannerController@editVideoAction')->name('edit-video-action');
            Route::any('banner/delete-video/{criteria?}', 'BannerController@deleteBannerVideo')->name('delete-video');
            Route::any('banner/update-video-status', 'BannerController@updateVideoStatus')->name('update-video-status');
            
             //Interesting facts of home page
            Route::any('facts', 'FactController@index')->name('facts');
            Route::any('add-fact', 'FactController@addFact')->name('add-fact');
            Route::any('edit-fact-action', 'FactController@editFactAction')->name('edit-fact-action');

            //clients of home page
            Route::any('client', 'HomeClientController@index')->name('home-client');
            Route::any('client-table', 'HomeClientController@ShowClientInTable')->name('client-table'); //to display all member in the table
            Route::any('add-client', 'HomeClientController@addClient')->name('add-home-client');
            Route::any('edit-client-information', 'HomeClientController@editClientAction')->name('edit-home-client-action');
            Route::any('delete-client/{criteria?}', 'HomeClientController@deleteClient')->name('delete-home-client');
            Route::any('update-client-status', 'HomeClientController@updateClientStatus')->name('update-home-client-status');

            //footer logo
            Route::any('footer-logo', 'FooterLogoController@index')->name('footer-logo');
            Route::any('footer-logo-table', 'FooterLogoController@ShowFooterLogoInTable')->name('footer-logo-table'); //to display all member in the table
            Route::any('add-footer-logo', 'FooterLogoController@addFooterLogo')->name('add-footer-logo');
            Route::any('edit-footer-logo', 'FooterLogoController@editFooterLogoAction')->name('edit-footer-logo-action');
            Route::any('delete-footer-logo/{criteria?}', 'FooterLogoController@deleteFooterLogo')->name('delete-footer-logo');
            Route::any('update-footer-logo-status', 'FooterLogoController@updateFooterLogoStatus')->name('update-footer-logo-status');

            //Client Brands
            Route::any('client-brands', 'BrandController@index')->name('client-brands');
            Route::any('client-brands-list', 'BrandController@BrandInList')->name('client-brands-list');
            Route::any('add-brand', 'BrandController@addBrand')->name('add-brand');
            Route::any('edit-brand-action', 'BrandController@editBrandAction')->name('edit-brand-action');
            Route::any('delete-brand/{criteria?}', 'BrandController@deleteBrand')->name('delete-brand');
            Route::any('update-brand-status', 'BrandController@updateBrandStatus')->name('update-brand-status');


        });

        //==========blog routes============//
        Route::group(['prefix' => 'blogs'], function () {
            Route::any('/', 'BlogController@index')->name('blogs');
            Route::any('blog-list', 'BlogController@BlogTable')->name('blog-list');
            Route::any('add-blog', 'BlogController@addBlog')->name('add-blog');
            Route::any('delete-blog/{criteria?}', 'BlogController@deleteBlog')->name('delete-blog');
            Route::any('edit-blog/{criteria?}', 'BlogController@editBlog')->name('edit-blog');
            Route::any('edit-blog-action', 'BlogController@editBlogAction')->name('edit-blog-action');
            Route::any('update-blog-status', 'BlogController@updateBlogStatus')->name('update-blog-status');

            //==========blog quote routes============//
            Route::any('quotes', 'QuoteController@index')->name('quotes');
            Route::any('add-quote', 'QuoteController@addQuote')->name('add-quote');
            Route::any('delete-quote/{criteria?}', 'QuoteController@deleteQuote')->name('delete-quote');
            Route::any('edit-quote/{criteria?}', 'QuoteController@editQuote')->name('edit-quote');
            Route::any('edit-quote-action', 'QuoteController@editQuoteAction')->name('edit-quote-action');
            Route::any('update-quote-status', 'QuoteController@updateQuoteStatus')->name('update-quote-status');

        });

        //==========team testimony routes============//
        Route::group(['prefix' => 'team-testimonies'], function () {
            Route::any('/', 'TestimonyController@index')->name('testimonies');
            Route::any('team-testimony-list', 'TestimonyController@TestimonyTable')->name('team-testimony-list');
            Route::any('add-team-testimony', 'TestimonyController@addTestimony')->name('add-testimony');
            Route::any('delete-team-testimony/{criteria?}', 'TestimonyController@deleteTestimony')->name('delete-testimony');
            Route::any('edit-team-testimony/{criteria?}', 'TestimonyController@editTestimony')->name('edit-testimony');
            Route::any('edit-team-testimony-action', 'TestimonyController@editTestimonyAction')->name('edit-testimony-action');
            Route::any('update-team-testimony-status', 'TestimonyController@updateTestimonyStatus')->name('update-testimony-status');

            //Edits team header
            Route::any('edit-team-header-action', 'TestimonyController@editTeamHeaderAction')->name('edit-home-team-header-action');

        });


        //==========employee story routes============//
        Route::group(['prefix' => 'employee-story'], function () {
            Route::any('/', 'StoryController@index')->name('employee-story');
            Route::any('employee-story-list', 'StoryController@StoryTable')->name('employee-story-list');
            Route::any('add-employee-story', 'StoryController@addStory')->name('add-employee-story');
            Route::any('delete-employee-story/{criteria?}', 'StoryController@deleteStory')->name('delete-employee-story');
            Route::any('edit-employee-story/{criteria?}', 'StoryController@editStory')->name('edit-employee-story');
            Route::any('edit-employee-story-action', 'StoryController@editStoryAction')->name('edit-employee-story-action');
            Route::any('update-employee-story-status', 'StoryController@updateStoryStatus')->name('update-employee-story-status');

        });


        //==========client testimony routes============//
        Route::group(['prefix' => 'client-testimonies'], function () {
            Route::any('/', 'ClientTestimonyController@index')->name('client-testimonies');
            Route::any('client-testimonies-table', 'ClientTestimonyController@ClientTestimonyTable')->name('client-testimonies-table');
            Route::any('add-client-testimony', 'ClientTestimonyController@addClientTestimony')->name('add-client-testimony');
            Route::any('delete-client-testimony/{criteria?}', 'ClientTestimonyController@deleteClientTestimony')->name('delete-client-testimony');
            Route::any('edit-client-testimony/{criteria?}', 'ClientTestimonyController@editClientTestimony')->name('edit-client-testimony');
            Route::any('edit-client-testimony-action', 'ClientTestimonyController@editClientTestimonyAction')->name('edit-client-testimony-action');
            Route::any('update-client-testimony-status', 'ClientTestimonyController@updateClientTestimonyStatus')->name('update-client-testimony-status');

        });

        //==========career routes============//
        Route::group(['prefix' => 'career'], function () {

            //==========work perks routes============//

            Route::any('work-perk', 'PerkController@index')->name('work-perks');
            Route::any('work-perk-table', 'PerkController@ShowPerkInTable')->name('work-perk-table');
            Route::any('add-work-perk', 'PerkController@addPerk')->name('add-work-perk');
            Route::any('delete-work-perk/{criteria?}', 'PerkController@deletePerk')->name('delete-work-perk');
            Route::any('edit-work-perk/{criteria?}', 'PerkController@editPerk')->name('edit-work-perk');
            Route::any('edit-work-perk-action', 'PerkController@editPerkAction')->name('edit-work-perk-action');
            Route::any('update-work-perk-status', 'PerkController@updatePerkStatus')->name('update-work-perk-status');

            //==========vacancy post routes============//

            Route::any('posts', 'PostController@index')->name('posts');
            Route::any('add-post', 'PostController@addPost')->name('add-post');
            Route::any('delete-post/{criteria?}', 'PostController@deletePost')->name('delete-post');
            Route::any('edit-post/{criteria?}', 'PostController@editPost')->name('edit-post');
            Route::any('edit-post-action', 'PostController@editPostAction')->name('edit-post-action');
            Route::any('update-post-status', 'PostController@updatePostStatus')->name('update-post-status');


            //==========applications routes============//

            Route::any('applications', 'ApplicationsController@index')->name('applications');
            Route::any('delete-application/{criteria?}', 'ApplicationsController@deleteApplication')->name('delete-application');

        });


        //==========service routes============//
        Route::group(['prefix' => 'services'], function () {
            Route::any('/', 'ServiceController@index')->name('services');

            //just to edit service header
            Route::any('edit-service-header-action', 'ServiceController@editServiceHeaderAction')->name('edit-home-service-header-action');

            Route::any('services-list', 'ServiceController@ServiceTable')->name('services-list');
            Route::any('add-service', 'ServiceController@addService')->name('add-service');
            Route::any('delete-service/{criteria?}', 'ServiceController@deleteService')->name('delete-service');
            Route::any('edit-service/{criteria?}', 'ServiceController@editService')->name('edit-service');
            Route::any('edit-service-action', 'ServiceController@editServiceAction')->name('edit-service-action');
            Route::any('update-service-status/{criteria?}', 'ServiceController@updateServiceStatus')->name('update-service-status');

            //==========services goals routes============//
            Route::any('/service-goals', 'ServiceGoalsController@index')->name('service-goals');
            Route::any('add-service-goals/{criteria?}', 'ServiceGoalsController@addServiceGoals')->name('add-service-goals');
            Route::any('delete-service-goals/{criteria?}', 'ServiceGoalsController@deleteServiceGoals')->name('delete-service-goals');
            Route::any('edit-service-goals/{criteria?}', 'ServiceGoalsController@editServiceGoals')->name('edit-service-goals');
            Route::any('edit-service-goals-action', 'ServiceGoalsController@editServiceGoalsAction')->name('edit-service-goals-action');
            Route::any('update-service-goals-status/{criteria?}', 'ServiceGoalsController@updateServiceGoalsStatus')->name('update-service-goals-status');

            //==========services in services routes============//
            Route::any('/services-in-service', 'ServicesOfServicesController@index')->name('service-in-services');
            Route::any('add-services-in-service/{criteria?}', 'ServicesOfServicesController@addServiceinServices')->name('add-service-in-services');
            Route::any('delete-service-in-service/{criteria?}', 'ServicesOfServicesController@deleteServiceinServices')->name('delete-service-in-services');
            Route::any('edit-service-in-service/{criteria?}', 'ServicesOfServicesController@editServiceinServices')->name('edit-service-in-services');
            Route::any('edit-service-in-service-action', 'ServicesOfServicesController@editServiceinServicesAction')->name('edit-service-in-services-action');
            Route::any('update-service-in-service-status/{criteria?}', 'ServicesOfServicesController@updateServiceinServicesStatus')->name('update-service-in-services-status');

            Route::any('delete-service-gallery/{criteria?}', 'ServiceGalleryController@deleteServiceGallery')->name('delete-service-gallery');

        });


        //==========portfolio routes============//
        Route::group(['prefix' => 'portfolios'], function () {
            Route::any('/', 'PortfolioController@index')->name('portfolios');
            Route::any('/portfolio-list', 'PortfolioController@ShowPortfolioList')->name('portfolio-list');


            //to perform actions for portfolio
            Route::any('add-portfolio', 'PortfolioController@addPortfolio')->name('add-portfolio');
            Route::any('delete-portfolio/{criteria?}', 'PortfolioController@deletePortfolio')->name('delete-portfolio');
            Route::any('edit-portfolio/{criteria?}', 'PortfolioController@editPortfolio')->name('edit-portfolio');
            Route::any('edit-portfolio-action', 'PortfolioController@editPortfolioAction')->name('edit-portfolio-action');
            Route::any('update-portfolio-status', 'PortfolioController@updatePortfolioStatus')->name('update-portfolio-status');

            //Delete First Gallery
            Route::any('delete-portfolio-image/{criteria?}', 'PortfolioGallery1Controller@deletePortfolioGallery')->name('delete-portfolio-image');

            //Delete Second Gallery
            Route::any('delete-portfolio-gallery/{criteria?}', 'PortfolioGalleryController@deletePortfolioGallery')->name('delete-portfolio-gallery');


        });

        //==========terms routes============//
        Route::group(['prefix' => 'terms-&-conditions'], function () {
            Route::any('/', 'TermsController@index')->name('terms');
            Route::any('add-terms-&-conditions', 'TermsController@addTerms')->name('add-terms');
            Route::any('delete-terms-&-conditions/{criteria?}', 'TermsController@dleteTerms')->name('delete-terms');
            Route::any('edit-terms-&-conditions/{criteria?}', 'TermsController@editTerms')->name('edit-terms');
            Route::any('edit-terms-&-conditions-action/{criteria?}', 'TermsController@editTermsAction')->name('edit-terms-action');
            Route::any('update-terms-&-conditions-status/{criteria?}', 'TermsController@updateTermsStatus')->name('update-terms-status');
        });

        //==========comment routes============//
        Route::group(['prefix' => 'comments'], function () {
            Route::any('/', 'CommentController@index')->name('comments');
            Route::any('delete-comment/{criteria?}', 'CommentController@deleteComment')->name('delete-comment');
            Route::any('update-comment-status/{criteria?}', 'CommentController@updateCommentStatus')->name('update-comment-status');

        });

        //==========contact routes============//
        Route::group(['prefix' => 'contacts'], function () {
            Route::any('/', 'ContactController@index')->name('contacts');

            Route::any('delete-contact/{criteria?}', 'ContactController@deleteContact')->name('delete-contact');
            Route::any('view-contact/{criteria?}', 'ContactController@viewContact')->name('view-contact');
        });


    });


});


