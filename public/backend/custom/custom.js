$(document).ready(function () {
    setTimeout(function () {
        $('.alert').hide('slow');

    }, 2000);

});

//SummerNote
$(document).ready(function () {
    $('textarea').summernote({
        placeholder: '',
        tabsize: 2,
        height: 300
    });

});

//show and hide div elements based on dropdown selection
$(document).ready(function () {
    $("select").change(function () {
        $(this).find("option:selected").each(function () {
            let optionValue = $(this).attr("value");
            if (optionValue) {
                $(".choose").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else {
                $(".choose").hide();
            }
        });
    }).change();
});


//admin login form validation
function validate() {

    let email = document.getElementById("email").value;
    let password = document.getElementById("password").value;
    let error_message1 = document.getElementById("error_message1");
    let error_message2 = document.getElementById("error_message2");

    error_message1.style.padding = "2px";
    error_message1.style.color = "red";

    error_message2.style.padding = "2px";
    error_message2.style.color = "red";


    let text;


    let regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

    if (email.length === null || email.length === 0) {
        $("#email").addClass("is-invalid");
        text = "Email field can't be empty.";
        error_message1.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === false) {
        $("#email").addClass("is-invalid");
        text = "Please enter valid email.";
        error_message1.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === true || email.length !== null || email.length !== 0) {
        $("#email").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message1.innerHTML = text;
    }


    if (password.length === null || password.length === 0) {
        $("#password").addClass("is-invalid");
        text = "Password field can't be empty.";
        error_message2.innerHTML = text;
        return false;
    }

    else {
        $("#password").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message2.innerHTML = text;
    }


}




