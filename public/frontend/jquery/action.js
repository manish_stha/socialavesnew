/* ===================================================================
 * Social Aves Pvt.Ltd - Main JS
 *
 * ------------------------------------------------------------------- */


/* Preloading
* ------------------------------------------------------ */
$(document).ready(function () {
    paceOptions = {
        ajax: true,
        document: true,
        eventLag: false
    };

    Pace.on('done', function () {
        $('#status').delay(550).animate({top: '30%', opacity: '0'}, 3000, $.bez([0.19, 1, 0.22, 1]));

        $('#preloader').delay(550).animate({top: '-100%', opacity: '1'}, 3000, $.bez([0.19, 1, 0.22, 1]));
        $('html').delay(850).css({'overflow': 'visible'});
    });
});


/* Scroll Effects on Menu and Back top
* ------------------------------------------------------ */
$(document).ready(function () {
    $(window).on('scroll', function () {

        if ($(window).scrollTop() > 150) {
            $('.open-menu').addClass('opaque');
        }
        else {
            $('.open-menu').removeClass('opaque');
        }

    });
    $(window).on('scroll', function () {
        if ($(window).scrollTop() > 500) {
            $(".go-top").fadeIn(400);
        } else {
            $(".go-top").fadeOut(400);
        }
    });
});


/* Open Menu and Close Menu Effects
* ------------------------------------------------------ */
$(document).ready(function () {
    $('.open-menu').on('click', function () {
        $('.nav-overlay').addClass('open');
    });

    $('.close-menu').on('click', function () {
        $('.nav-overlay').removeClass('open');
    });
});


$(document).ready(function () {
    $('.open-menu').on('click', function () {
        $('.main-body').toggleClass('open');
    });
    $('.close-menu').on('click', function () {
        $('.main-body').removeClass('open');
    });
});


/* Smooth Scrolling
* ------------------------------------------------------ */
$(document).ready(function () {
    // Add smooth scrolling to all links
    $("a").on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 1000, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});


/* Popover
* ------------------------------------------------------ */
$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});

/* Carousel
* ------------------------------------------------------ */
$(document).ready(function () {
    $('.carousel').carousel({
        interval: 5000
    });
});

$(document).ready(function () {

    $("#logo-carousel").owlCarousel({
        loop: true,
        autoplay: true,
        navigation: false,
        items: 6,
        mouseDrag: true,
        touchDrag: true,
        autoWidth: false,
        autoHeight: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1200: {
                items: 5
            },
            1360: {
                items: 6
            },
            1920: {
                items: 6
            }
        }

    });

    $('.slide-one-item').owlCarousel({
        center: false,
        items: 1,
        loop: true,
        stagePadding: 0,
        margin: 0,
        smartSpeed: 1500,
        autoplay: true,
        pauseOnHover: false,
        dots: false,
        nav: false

    });

});

$(document).ready(function () {

    $('#carousel-client').owlCarousel({
        mouseDrag: false,
        autoplay: false,
        loop: true,
        margin: 2,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 3
            }
        }
    });

    $('.owl-prev').click(function () {
        $active = $('.owl-item .item.show');
        $('.owl-item .item.show').removeClass('show');
        $('.owl-item .item').removeClass('next');
        $('.owl-item .item').removeClass('prev');
        $active.addClass('next');
        if ($active.is('.first')) {
            $('.owl-item .last').addClass('show');
            $('.first').addClass('next');
            $('.owl-item .last').parent().prev().children('.item').addClass('prev');
        }
        else {
            $active.parent().prev().children('.item').addClass('show');
            if ($active.parent().prev().children('.item').is('.first')) {
                $('.owl-item .last').addClass('prev');
            }
            else {
                $('.owl-item .show').parent().prev().children('.item').addClass('prev');
            }
        }
    });

    $('.owl-next').click(function () {
        $active = $('.owl-item .item.show');
        $('.owl-item .item.show').removeClass('show');
        $('.owl-item .item').removeClass('next');
        $('.owl-item .item').removeClass('prev');
        $active.addClass('prev');
        if ($active.is('.last')) {
            $('.owl-item .first').addClass('show');
            $('.owl-item .first').parent().next().children('.item').addClass('prev');
        }
        else {
            $active.parent().next().children('.item').addClass('show');
            if ($active.parent().next().children('.item').is('.last')) {
                $('.owl-item .first').addClass('next');
            }
            else {
                $('.owl-item .show').parent().next().children('.item').addClass('next');
            }
        }
    });

});

$(document).ready(function () {

    $("#carousel-portfolio").owlCarousel({

        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        loop: true,
        topOnHover: true,
        autoplay: true,
        items: 1,
        paginationSpeed: 400,
        singleItem: true,
        animateOut: 'fadeOut'

    });


    $('.block-1').owlCarousel({
        center: false,
        items: 1,
        loop: true,
        stagePadding: 0,
        margin: 0,
        autoplay: true,
        nav: false,
        responsive: {
            600: {
                margin: 0,
                nav: true,
                items: 2
            },
            1000: {
                margin: 0,
                stagePadding: 0,
                nav: true,
                items: 3
            },
            1500: {
                margin: 0,
                stagePadding: 0,
                nav: true,
                items: 4
            }
        }
    });

});

/* Collaspe
* ------------------------------------------------------ */

$('.seeMore').click(function () {
    var $this = $(this);
    $this.toggleClass('seeMore');
    if ($this.hasClass('seeMore')) {
        $this.text('See More');
    } else {
        $this.text('See Less');
    }
});


/* Upload the file
* ------------------------------------------------------ */
$(document).ready(function () {
// Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
});

/* PLUS AND MINUS ICON EFFECT
* ------------------------------------------------------ */
$(document).ready(function () {
    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function () {
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
});


$(document).ready(function () {
    if ($('.text-slider').length == 1) {
        var typed_strings = $('.text-slider-items').text();
        var typed = new Typed('.text-slider', {
            strings: typed_strings.split(','),
            typeSpeed: 80,
            loop: true,
            backDelay: 1100,
            backSpeed: 30
        });
    }
});


/* COUNTER EFFECT
* ------------------------------------------------------ */

$(document).ready(function () {
    $('.counter-count').counterUp({
        delay: 15,
        time: 2000
    });

});

$(document).ready(function () {
    $("div.bhoechie-tab-menu>div.list-group>a").click(function (e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
    });
});

/* CATEGORY EFFECT
* ------------------------------------------------------ */

$(document).ready(function () {
    if ($('.gallery_f_inner').length) {
        // Activate isotope in container
        $(".gallery_f_inner").imagesLoaded(function () {
            $(".gallery_f_inner").isotope({
                layoutMode: 'fitRows',
                animationOptions: {
                    duration: 750,
                    easing: 'linear'
                }
            });
        });

        // Add isotope click function
        $(".gallery_filter li").on('click', function () {
            $(".gallery_filter li").removeClass("active");
            $(this).addClass("active");

            var selector = $(this).attr("data-filter");
            $(".gallery_f_inner").isotope({
                filter: selector,
                animationOptions: {
                    duration: 450,
                    easing: "linear",
                    queue: false,
                }
            });
            return false;
        });
    }
});

/* Animation on Scroll
* ------------------------------------------------------ */
AOS.init();


