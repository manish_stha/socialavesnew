// to stop form submission before captcha box is checked
$(function () {
    $('#myform').submit(function (event) {
        let verified = grecaptcha.getResponse();
        if (verified.length === 0) {
            alert('Please Fill The Captcha First');
            event.preventDefault();
        }
    });
});


//to submit form without reloading the page
$(document).ready(function () {

    $('#blogcomment').on('submit', function (e) {
        e.preventDefault();

        $.ajax({
            type: "POST",
            url: "https://socialaves.website/add-comment",
            data: $('#blogcomment').serialize(),
            success: function (response) {
                console.log(response)
                alert("Thank you for your comment");
            },
            error: function (error) {
                console.log(error)
                alert("Data not saved");

            }
        });
    });
});


//to load data without page refreshment
// $(document).ready(function () {
//     setInterval(function () {
//         $('#show_comments').load('http://localhost:8080/Social%20Aves/public/add-comment')
//     }, 3000);
// });


// contact form validation

function ValidateContactForm() {
    let name = document.getElementById("name").value;
    let phone = document.getElementById("phone").value;
    let email = document.getElementById("email").value;
    let message = document.getElementById("message").value;
    let error_message1 = document.getElementById("error_message1");
    let error_message2 = document.getElementById("error_message2");
    let error_message3 = document.getElementById("error_message3");
    let error_message4 = document.getElementById("error_message4");

    error_message1.style.color = "red";
    error_message2.style.color = "red";
    error_message3.style.color = "red";
    error_message4.style.color = "red";

    let text;


    let regName = /[^a-z,^A-Z ]/g;

    if (regName.test($("#name").val()) === true) {
        $("#name").addClass("is-invalid");
        text = "Numbers and special characters are not allowed.";
        error_message1.innerHTML = text;
        return false;
    }

    if (name.length === null || name.length === 0) {    // === and !== are used for strict comparison || we can use any of them

        $("#name").addClass("is-invalid");
        text = "Name field can't be empty.";
        error_message1.innerHTML = text;
        return false;
    }

    if (name.length < 3) {

        $("#name").addClass("is-invalid");
        text = "Name should contain more than 3 characters.";
        error_message1.innerHTML = text;
        return false;
    }

    if (regName.test($("#name").val()) === false || name.length !== null || name.length !== 0 || name.length > 3) {

        $("#name").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message1.innerHTML = text;
    }


    let regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

    if (email.length === null || email.length === 0) {
        $("#email").addClass("is-invalid");
        text = "Email field can't be empty.";
        error_message2.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === false) {
        $("#email").addClass("is-invalid");
        text = "Please enter valid email.";
        error_message2.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === true || email.length !== null || email.length !== 0) {
        $("#email").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message2.innerHTML = text;
    }


    let regPhone = /[^0-9 ]/g;

    if (regPhone.test($("#phone").val()) === true) {
        $("#phone").addClass("is-invalid");
        text = "Alphabets and special characters are not allowed.";
        error_message3.innerHTML = text;
        return false;
    }

    if (isNaN(phone) || phone.length !== 10) {
        text = "Phone number must contain exactly 10 digits.";
        error_message3.innerHTML = text;
        return false;
    }

    if (regPhone.test($("#phone").val()) === false || isNaN(phone) || phone.length === 10) {
        $("#phone").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message3.innerHTML = text;
    }


    if (message.length === null || message.length === 0) {
        $("#message").addClass("is-invalid");
        text = "Message field can't be empty.";
        error_message4.innerHTML = text;
        return false;
    }

    else {
        $("#message").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message4.innerHTML = text;
    }


}


function ValidateServiceForm() {
    let name = document.getElementById("name").value;
    let phone = document.getElementById("phone").value;
    let email = document.getElementById("email").value;
    let company = document.getElementById("company").value;
    let message = document.getElementById("message").value;
    let error_message1 = document.getElementById("error_message1");
    let error_message2 = document.getElementById("error_message2");
    let error_message3 = document.getElementById("error_message3");
    let error_message4 = document.getElementById("error_message4");
    let error_message5 = document.getElementById("error_message5");

    error_message1.style.color = "red";
    error_message2.style.color = "red";
    error_message3.style.color = "red";
    error_message4.style.color = "red";
    error_message5.style.color = "red";

    let text;

    let regName = /[^a-z,^A-Z ]/g;

    if (regName.test($("#name").val()) === true) {
        $("#name").addClass("is-invalid");
        text = "Numbers and special characters are not allowed.";
        error_message1.innerHTML = text;
        return false;
    }

    if (name.length === null || name.length === 0) {

        $("#name").addClass("is-invalid");
        text = "Name field can't be empty.";
        error_message1.innerHTML = text;
        return false;
    }

    if (name.length < 3) {

        $("#name").addClass("is-invalid");
        text = "Name should contain more than 3 characters.";
        error_message1.innerHTML = text;
        return false;
    }

    if (regName.test($("#name").val()) === false || name.length !== null || name.length !== 0 || name.length > 3) {

        $("#name").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message1.innerHTML = text;
    }


    let regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

    if (email.length === null || email.length === 0) {
        $("#email").addClass("is-invalid");
        text = "Email field can't be empty.";
        error_message2.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === false) {
        $("#email").addClass("is-invalid");
        text = "Please enter valid email.";
        error_message2.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === true || email.length !== null || email.length !== 0) {
        $("#email").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message2.innerHTML = text;
    }


    let regPhone = /[^0-9 ]/g;

    if (regPhone.test($("#phone").val()) === true) {
        $("#phone").addClass("is-invalid");
        text = "Alphabets and special characters are not allowed.";
        error_message3.innerHTML = text;
        return false;
    }

    if (isNaN(phone) || phone.length !== 10) {
        text = "Phone number must contain exactly 10 digits.";
        error_message3.innerHTML = text;
        return false;
    }

    if (regPhone.test($("#phone").val()) === false || isNaN(phone) || phone.length === 10) {
        $("#phone").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message3.innerHTML = text;
    }


    if (company.length === null || company.length === 0) {
        $("#company").addClass("is-invalid");
        text = "Company field can't be empty.";
        error_message4.innerHTML = text;
        return false;
    }

    else {
        $("#company").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message4.innerHTML = text;
    }


    if (message.length === null || message.length === 0) {
        $("#message").addClass("is-invalid");
        text = "Message field can't be empty.";
        error_message5.innerHTML = text;
        return false;
    }

    else {
        $("#message").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message5.innerHTML = text;
    }


}


function ValidateApplicationForm() {
    let name = document.getElementById("name").value;
    let phone = document.getElementById("phone").value;
    let email = document.getElementById("email").value;
    let address = document.getElementById("address").value;
    let cv = document.getElementById("cv").value;
    let error_message1 = document.getElementById("error_message1");
    let error_message2 = document.getElementById("error_message2");
    let error_message3 = document.getElementById("error_message3");
    let error_message4 = document.getElementById("error_message4");
    let error_message5 = document.getElementById("error_message5");

    error_message1.style.color = "red";
    error_message2.style.color = "red";
    error_message3.style.color = "red";
    error_message4.style.color = "red";
    error_message5.style.color = "red";

    let text;

    let regName = /[^a-z,^A-Z ]/g;

    if (regName.test($("#name").val()) === true) {
        $("#name").addClass("is-invalid");
        text = "Numbers and special characters are not allowed.";
        error_message1.innerHTML = text;
        return false;
    }

    if (name.length == null || name.length === 0) {

        $("#name").addClass("is-invalid");
        text = "Name field can't be empty.";
        error_message1.innerHTML = text;
        return false;
    }

    if (name.length < 3) {

        $("#name").addClass("is-invalid");
        text = "Name should contain more than 3 characters.";
        error_message1.innerHTML = text;
        return false;
    }

    if (regName.test($("#name").val()) === false || name.length != null || name.length !== 0 || name.length > 3) {

        $("#name").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message1.innerHTML = text;
    }


    let regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

    if (email.length === null || email.length === 0) {
        $("#email").addClass("is-invalid");
        text = "Email field can't be empty.";
        error_message2.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === false) {
        $("#email").addClass("is-invalid");
        text = "Please enter valid email.";
        error_message2.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === true || email.length != null || email.length !== 0) {
        $("#email").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message2.innerHTML = text;
    }


    let regPhone = /[^0-9 ]/g;

    if (regPhone.test($("#phone").val()) === true) {
        $("#phone").addClass("is-invalid");
        text = "Alphabets and special characters are not allowed.";
        error_message3.innerHTML = text;
        return false;
    }

    if (isNaN(phone) || phone.length !== 10) {
        text = "Phone number must contain exactly 10 digits.";
        error_message3.innerHTML = text;
        return false;
    }

    if (regPhone.test($("#phone").val()) === false || isNaN(phone) || phone.length === 10) {
        $("#phone").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message3.innerHTML = text;
    }

    if (address.length === null || address.length === 0) {
        $("#address").addClass("is-invalid");
        text = "Address field can't be empty.";
        error_message4.innerHTML = text;
        return false;
    }

    else {

        $("#address").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message4.innerHTML = text;
    }


    if (cv.length === 0) {
        $("#cv").addClass("is-invalid");
        text = "Please upload your valid CV.";
        error_message5.innerHTML = text;
        return false;
    }

    let file = document.querySelector("#cv");

    if (/\.(doc|docx|pdf|jpg|jpeg|png)$/i.test(file.files[0].name) === false) {

        $("#cv").addClass("is-invalid");
        text = "Your file must be type of doc/docx/pdf/jpg/jpeg/png";
        error_message5.innerHTML = text;
        return false;

    }

    if (cv.length !== 0 || /\.(doc|docx|pdf|jpg|jpeg|png)$/i.test(file.files[0].name) === true) {
        $("#cv").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message5.innerHTML = text;
    }

    //to check the file size
    let FileSize = file.files[0].size / 1024 / 1024;
    if (FileSize > 25) {  //here is the max size checked
        alert('File size exceeds 25 MB.');
        event.preventDefault();
    }


}


function ValidateBlogCommentForm() {
    let name = document.getElementById("name").value;
    let email = document.getElementById("email").value;
    let comment = document.getElementById("comment").value;
    let error_message1 = document.getElementById("error_message1");
    let error_message2 = document.getElementById("error_message2");
    let error_message3 = document.getElementById("error_message3");

    error_message1.style.color = "red";
    error_message2.style.color = "red";
    error_message3.style.color = "red";

    let text;

    let regName = /[^a-z,^A-Z ]/g;


    if (regName.test($("#name").val()) === true) {
        $("#name").addClass("is-invalid");
        text = "Numbers and special characters are not allowed.";
        error_message1.innerHTML = text;
        return false;
    }

    if (name.length === null || name.length === 0) {

        $("#name").addClass("is-invalid");
        text = "Name field can't be empty.";
        error_message1.innerHTML = text;
        return false;
    }

    if (name.length < 3) {

        $("#name").addClass("is-invalid");
        text = "Name should contain more than 3 characters.";
        error_message1.innerHTML = text;
        return false;
    }

    if (regName.test($("#name").val()) === false || name.length !== null || name.length !== 0 || name.length > 3) {

        $("#name").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message1.innerHTML = text;
    }


    let regMail = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;

    if (email.length === null || email.length === 0) {
        $("#email").addClass("is-invalid");
        text = "Email field can't be empty.";
        error_message2.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === false) {
        $("#email").addClass("is-invalid");
        text = "Please enter valid email.";
        error_message2.innerHTML = text;
        return false;
    }

    if (regMail.test($("#email").val()) === true || email.length !== null || email.length !== 0) {
        $("#email").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message2.innerHTML = text;
    }

    if (comment.length === null || comment.length === 0) {
        $("#comment").addClass("is-invalid");
        text = "Comment field can't be empty.";
        error_message3.innerHTML = text;

        return false;
    }

    else {
        $("#comment").removeClass("is-invalid").addClass("is-valid");
        text = "";
        error_message3.innerHTML = text;

    }


}









