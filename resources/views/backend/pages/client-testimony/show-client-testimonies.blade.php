@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12">
                    <a href="{{route('client-testimonies-table')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        Check List</a>

                    <a class="btn btn-success" data-toggle="modal"
                       data-target="#add" style="margin-left: 15px"><i
                                class="fa fa-plus"></i>
                        Add New Testimony</a>

                    {{--modal begins--}}
                    <div class="modal fade" id="add" tabindex="-1" role="dialog"
                         aria-labelledby="basicModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel"><b>Testimonials Videos</b></h4>
                                </div>

                                <form action="{{route('add-client-testimony')}}" method="post"
                                      enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-sm-5" style="margin-left: 10px">
                                            <div class="form-group form-group-sm">
                                                <label for="image">Video Background Image*</label>
                                                <input type="file" name="preview_image" class="form-control"
                                                       required>
                                                <a href="" style="color: red">{{$errors->first('preview_image')}}</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6" style="margin-left: 10px">
                                            <div class="form-group form-group-sm">
                                                <label>Select Required Field*</label>
                                                <select class="form-control" required>
                                                    <option disabled selected value="">--Select Option--</option>
                                                    <option value="Video">Upload Video</option>
                                                    <option value="Link">Video Link</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row Video choose">
                                        <div class="col-sm-5" style="margin-left: 10px">
                                            <div class="form-group form-group-sm">
                                                <label for="image">Video</label>
                                                <input type="file" name="video" class="form-control">
                                                <a href="" style="color: red">{{$errors->first('video')}}</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row Link choose">
                                        <div class="col-sm-11" style="margin-top: 5px;margin-left: 10px">
                                            <div class="form-group form-group-sm">
                                                <label for="name">Video Link</label>
                                                <input type="text" name="link" class="form-control"
                                                       value="{{old('title')}}">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-11" style="margin-top: 5px;margin-left: 10px">
                                            <div class="form-group form-group-sm">
                                                <label for="name">Video Text*</label>
                                                <input name="title" required="required" class="form-control"
                                                       value="{{old('title')}}">
                                                <a href="" style="color: red">{{$errors->first('title')}}</a>
                                            </div>
                                            <button class="btn btn-primary"><i
                                                        class="fa fa-save"></i> Save
                                            </button>
                                        </div>
                                    </div>


                                </form>


                            </div>
                        </div>
                    </div>
                    {{--modal ends--}}


                    <div class="x_panel" style="margin-top: 3px">
                        <div class="x_title">
                            <h2>Testimonial Videos</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="row">
                            @include('backend.layouts.messages')
                            @foreach ($errors->all() as $error)
                                <h2 style="color: red"> {{$error}} </h2>
                            @endforeach
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-center"></div>
                                            <div class="clearfix"></div>

                                            @foreach($ClientTestimonyData  as $testimony)
                                                <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                    <div class="well profile_view">
                                                        <div class="col-sm-12">
                                                            <img src="{{url('uploads/images/client-testimony/'.$testimony->preview_image)}}"
                                                                 class="img-responsive">

                                                            <h4 style="font-size: 16px;color: #23272b;margin-top: 15px"><?=strip_tags($testimony->title)?></h4>
                                                            <hr>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-pencil"></i> Created
                                                                    At: {{$testimony->created_at->format('m/d/Y H:i')}}
                                                                </li>
                                                            </ul>

                                                        </div>
                                                        <div class="col-xs-12 bottom text-center">
                                                            <div class="col-xs-12 col-sm-12 emphasis">

                                                                <a href="{{route('edit-client-testimony').'/'.$testimony->id}}"
                                                                   class="btn btn-primary pull-left"><i
                                                                            class="fa fa-edit"></i> Edit</a>
                                                                <form action="{{route('update-client-testimony-status')}}"
                                                                      method="post" class="pull-left">
                                                                    {{csrf_field()}}
                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$testimony->id}}">
                                                                    @if($testimony->status==1)
                                                                        <button name="active" class="btn btn-danger">
                                                                            Hide
                                                                        </button>
                                                                    @else
                                                                        <button name="inactive" class="btn btn-success">
                                                                            Publish
                                                                        </button>
                                                                    @endif
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection