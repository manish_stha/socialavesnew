@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('client-testimonies')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Testimonies</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Update Client Testimony</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{route('edit-client-testimony-action')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <input type="hidden" value="{{$ClientTestimonyData->id}}" name="criteria">


                                        <div class="form-group form-group-lg">
                                            <label for="name">Video Background Image*</label>
                                            <input type="file" name="preview_image"
                                                   class="btn btn-default btn-sm">
                                        </div>
                                        <a style="color: red">{{$errors->first('preview_image')}}</a>

                                        <div class="row" style="margin-top: 23px">
                                            <div class="col-md-12">
                                                <img src="{{url('uploads/images/client-testimony/' .$ClientTestimonyData->preview_image)}}"
                                                     alt=""
                                                     class="img-responsive thumbnail">
                                            </div>
                                        </div>


                                        <div class="row">
                                            @if($ClientTestimonyData->link !== null)
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label>Video Link</label>
                                                        <input type="text" class="form-control" name="link"
                                                               value="{{$ClientTestimonyData->link}}" required>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>

                                        <div class="row">
                                            @if($ClientTestimonyData->video !== null)
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label>Video</label>
                                                        <input type="file" class="btn btn-default btn-sm"
                                                               name="video">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>


                                        <div class="form-group form-group-lg" style="margin-top: 10px">
                                            <label>Video Text*</label>
                                            <input type="text" class="form-control" name="title"
                                                   value="{{$ClientTestimonyData->title}}" required>
                                        </div>
                                        <a href="" style="color: red">{{$errors->first('title')}}</a>

                                        <div class="form-group form-group-lg" style="margin-top: 20px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i>
                                                Save
                                                Changes
                                            </button>


                                        </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection