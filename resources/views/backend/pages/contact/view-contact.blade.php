@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('contacts')}}" style="font-size: medium;font-size: small"
                       class="btn btn-success btn-xs"><i
                                class="fa fa-backward"></i> Go To Contact List</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">View Contact</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-12">
                                    <form action="" method="post" enctype="multipart/form-data">

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group form-group-sm">
                                                    <label for="name" style="font-size: medium">Name:</label>
                                                    <p style="font-weight: bold">{{$contactData->name}}</p>

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group form-group-sm">
                                                    <label for="name" style="font-size: medium">Email:</label>
                                                    <p style="font-weight: bold">{{$contactData->email}}</p>

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group form-group-sm">
                                                    <label for="name" style="font-size: medium">Phone:</label>
                                                    <p style="font-weight: bold">{{$contactData->phone}}</p>


                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group form-group-sm">
                                                    <label for="name" style="font-size: medium">Company:</label>
                                                    <p style="font-weight: bold">{{$contactData->company}}</p>


                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label for="comment" style="font-size: medium">Comment:</label>
                                            <p style="font-weight: bold">{{$contactData->comment}}</p>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection