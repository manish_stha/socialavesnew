@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('services')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> Check Service List</a>

            
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Service's Goals</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">

                                @include('backend.layouts.messages')

                                <div class="col-md-6">
                                    <form action="{{route('add-service-goals')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <input type="hidden" name="service_id" value="{{$ServiceData->id}}">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label style="font-size: 24px">{{$ServiceData->title}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Title of Goal*</label>
                                                    <input type="text" name="title" id="title" class="form-control"
                                                           placeholder="Enter Title of Service" required>
                                                </div>
                                                <a href="" style="color: red">{{$errors->first('title')}}</a>

                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="description">Description*</label>
                                            <textarea name="description" class="form-control"></textarea>
                                            <a href="" style="color: red">{{$errors->first('description')}}</a>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Image*</label>
                                                    <input type="file" class="btn btn-default btn-sm" name="image"
                                                           required>
                                                </div>
                                            </div>
                                            <a href="" style="color: red">{{$errors->first('image')}}</a>
                                        </div>

                                        <div class="form-group form-group-lg" style="margin-top: 10px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Save
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection