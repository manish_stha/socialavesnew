@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('services')}}" style="font-size: small;" class="btn btn-success btn-xs"><i
                                class="fa fa-backward"></i>
                        Back</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Services</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Title</th>
                                        <th>Subtitle</th>
                                        <th>Description</th>
                                        <th>More Description</th>
                                        <th>Image</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($serviceData as $key=>$service)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$service->title}}</td>
                                            <td><?= str_limit(strip_tags($service->subtitle), '100')?></td>
                                            <td><?= str_limit(strip_tags($service->description), '100')?></td>
                                            <td><?= str_limit(strip_tags($service->description1), '100')?></td>
                                            <td>
                                                <img src="{{url('uploads/images/services/'.$service->image)}}" alt=""
                                                     width="30">
                                            </td>
                                            <td>
                                                <form action="{{route('update-service-status')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="criteria" value="{{$service->id}}">
                                                    @if($service->status==1)
                                                        <button name="active" class="btn btn-success btn-xs">
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    @else
                                                        <button name="inactive" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                            </td>
                                            <td>
                                                <a href="{{route('edit-service').'/'.$service->id}}"
                                                   class="btn btn-success btn-xs"><i class="fa fa-edit" title="Edit Service"></i></a>
                                                {{--<a href="{{route('delete-service').'/'.$service->id}}"--}}
                                                {{--onclick="return confirm('Are you sure?')"--}}
                                                {{--class="btn btn-danger btn-xs"><i--}}
                                                {{--class="fa fa-trash" title="Delete Service"></i></a>--}}
                                            </td>
                                            @endforeach
                                        </tr>
                                    </tbody>


                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection