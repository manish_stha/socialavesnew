@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('services-list')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        Check List</a>
                    <a href="{{route('add-service')}}" style="margin-left: 15px" data-toggle="modal"
                       class="btn btn-success"><i
                                class="fa fa-plus"></i>
                        Add New Service</a>

                    {{--<a href="{{route('service-in-services')}}" style="font-size: small;" data-toggle="modal"--}}
                    {{--class="btn btn-success btn-xs pull-right"><i--}}
                    {{--class="fa fa-list"></i>--}}
                    {{--Services in Service</a>--}}
                    {{--<a href="{{route('service-goals')}}" style="font-size: small;"--}}
                    {{--class="btn btn-success btn-xs pull-right"><i--}}
                    {{--class="fa fa-list"></i>--}}
                    {{--Goals</a>--}}


                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Services</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @include('backend.layouts.messages')

                        <div class="x_content">

                            <div class="row">
                                @foreach($serviceHeaderData as $header)

                                    <div class="col-md-6 col-sm-4 col-xs-12 profile_details">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <h4 class="brief"><b>Service Title &
                                                        Subtitle</b>
                                                </h4>
                                                <div class="left col-xs-12">
                                                    <p><strong>Title: {{ strip_tags($header->title)}} </strong></p>
                                                    <p>
                                                        <strong>Subtitle: <?= strip_tags($header->subtitle)?></strong>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">

                                                <div class="col-xs-12 col-sm-6 emphasis center-margin">

                                                    <button type="button" data-toggle="modal"
                                                            data-target="#{{$header->id}}"
                                                            class="btn btn-primary btn-sm">
                                                        <i class="fa fa-edit"> </i> Edit
                                                    </button>

                                                    {{--modal begins--}}
                                                    <div class="modal fade" id="{{$header->id}}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="basicModal" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-hidden="true">&times;
                                                                    </button>
                                                                    <h4 class="modal-title"
                                                                        style="font-family: Montserrat;font-size: large"
                                                                        id="myModalLabel"><b>Update
                                                                            Service Header Here</b></h4>
                                                                </div>
                                                                <form action="{{route('edit-home-service-header-action')}}"
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    {{csrf_field()}}
                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$header->id}}">

                                                                    <div class="col-sm-12" style="margin-top: 5px">
                                                                        <div class="form-group form-group-lg">
                                                                            <label for="title">Title</label>
                                                                            <input type="text" name="title"
                                                                                   value="{{strip_tags($header->title)}}"
                                                                                   id="title"
                                                                                   class="form-control"
                                                                                   placeholder="Enter Title of Service"
                                                                                   required>
                                                                            <a href=""
                                                                               style="color: red">{{$errors->first('title')}}</a>

                                                                        </div>
                                                                        <div class="form-group form-group-lg">
                                                                            <label for="body">Subtitle</label>
                                                                            <textarea name="subtitle"
                                                                                      required="required"
                                                                                      id="description_id2"
                                                                                      class="form-control">{!!  strip_tags($header->subtitle) !!}</textarea>
                                                                            <a href=""
                                                                               style="color: red">{{$errors->first('subtitle')}}</a>

                                                                        </div>
                                                                        <button class="btn btn-primary"><i
                                                                                    class="fa fa-save"></i> Save Changes
                                                                        </button>
                                                                    </div>

                                                                </form>


                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{--modal ends--}}

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>

                            <div class="row">

                                @foreach($serviceData as $service)
                                    <div class="col-md-12 col-sm-4 col-xs-12 profile_details">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <div class="left col-xs-7">
                                                    <h2><strong><?=strip_tags($service->title)?></strong></h2>
                                                    <p style="text-align: justify-all">
                                                        <b><?=str_limit(strip_tags($service->subtitle), '100')?></b></p>
                                                    <p style="text-align: justify-all"><?=str_limit(strip_tags($service->description), '400')?></p>

                                                    <hr>
                                                    <ul class="list-unstyled" style="font-size: medium">
                                                        <li><i class="fa fa-pencil"></i> Created
                                                            At: {{$service->created_at->format('m/d/Y H:i')}}
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="right col-xs-5 text-center">
                                                    <img src="{{url('uploads/images/services/'.$service->image)}}"
                                                         height="160" width="160" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">
                                                <div class="col-xs-12 col-sm-12 emphasis">

                                                    <a href="{{route('edit-service').'/'.$service->id}}"
                                                       class="btn btn-primary pull-left"><i
                                                                class="fa fa-edit"></i> Edit</a>

                                                    <form class="pull-left" action="{{route('update-service-status')}}"
                                                          method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="criteria" value="{{$service->id}}">
                                                        @if($service->status==1)
                                                            <button name="active" class="btn btn-danger">
                                                                Hide
                                                            </button>
                                                        @else
                                                            <button name="inactive" class="btn btn-success">
                                                                Publish
                                                            </button>
                                                        @endif
                                                    </form>

                                                    {{--for Social Media Marketing--}}
                                                    @if($service->id === 8)

                                                        <a href="{{route('add-service-in-services').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Inner Services</a>
                                                        <a href="{{route('service-in-services')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>

                                                        <a href="{{route('add-service-goals').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Goals</a>
                                                        <a href="{{route('service-goals')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>
                                                    @endif

                                                    {{--for Content Marketing--}}
                                                    @if($service->id === 9)
                                                        <a href="{{route('add-service-in-services').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Services</a>
                                                        <a href="{{route('service-in-services')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>
                                                    @endif

                                                    {{--for SEO--}}
                                                    @if($service->id === 10)

                                                        <a href="{{route('add-service-goals').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Goals</a>
                                                        <a href="{{route('service-goals')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>
                                                    @endif

                                                    {{--for SEM--}}
                                                    @if($service->id === 11)
                                                        <a href="{{route('add-service-in-services').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Advantages</a>
                                                        <a href="{{route('service-in-services')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>

                                                        <a href="{{route('add-service-goals').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Basic Concepts</a>
                                                        <a href="{{route('service-goals')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>
                                                    @endif

                                                    {{--for Influncer Marketing--}}
                                                    @if($service->id === 12)
                                                        <a href="{{route('add-service-goals').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Goals</a>
                                                        <a href="{{route('service-goals')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>

                                                        <a href="{{route('add-service-in-services').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Video</a>
                                                        <a href="{{route('service-in-services')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>
                                                    @endif

                                                    {{--for Web D&D--}}
                                                    @if($service->id === 13)
                                                        <a href="{{route('add-service-in-services').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Does Your?</a>
                                                        <a href="{{route('service-in-services')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>

                                                        <a href="{{route('add-service-goals').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Our Expertise</a>
                                                        <a href="{{route('service-goals')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>

                                                    @endif

                                                    {{--for Video Marketing--}}
                                                    @if($service->id === 14)
                                                        <a href="{{route('add-service-goals').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Goals</a>
                                                        <a href="{{route('service-goals')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>

                                                    @endif

                                                    {{--{--for Email Marketing--}}
                                                    @if($service->id === 15)
                                                        <a href="{{route('add-service-goals').'/'.$service->id}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-plus"></i> Goals</a>
                                                        <a href="{{route('service-goals')}}"
                                                           class="btn btn-primary pull-right"><i
                                                                    class="fa fa-list"></i></a>

                                                    @endif


                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>


                            <div class="pull-right">
                                {{$serviceData->render()}}
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /page content -->
@endsection

