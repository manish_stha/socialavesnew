@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('services')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Services</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Update Service</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">

                                @include('backend.layouts.messages')

                                <div class="col-md-6">
                                    <form action="{{route('edit-service-action')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <input type="hidden" name="criteria" value="{{$serviceData->id}}">

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label for="title">Title of Service*</label>
                                                            <input type="text" name="title" id="title"
                                                                   value="{{$serviceData->title}}" class="form-control"
                                                                   placeholder="Enter Title of Service" required>
                                                            <a href=""
                                                               style="color: red">{{$errors->first('title')}}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group form-group-lg">
                                                    <label for="subtitle">Subtitle*</label>
                                                    <textarea name="subtitle" id="description_id"
                                                              class="form-control">{{$serviceData->subtitle}}</textarea>
                                                    <a href="" style="color: red">{{$errors->first('subtitle')}}</a>
                                                </div>
                                                <div class="form-group form-group-lg">
                                                    <label for="description">Description*</label>
                                                    <textarea name="description" id="description_id"
                                                              class="form-control">{{$serviceData->description}}</textarea>
                                                    <a href="" style="color: red">{{$errors->first('description')}}</a>
                                                </div>
                                                <div class="form-group form-group-lg">
                                                    <label for="description">More Description (If Description contains
                                                        more than one paragraph)</label>
                                                    <textarea name="description1"
                                                              class="form-control">{{$serviceData->description1}}</textarea>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label>Icon*</label>
                                                            <input type="file" class="btn btn-default btn-sm"
                                                                   name="icon">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <img src="{{url('uploads/images/services/icons/'.$serviceData->icon)}}"
                                                             alt="" class="img-responsive"
                                                             style="margin-top: 23px">
                                                    </div>
                                                </div>

                                                <br>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label for="image">Image*</label>
                                                            <input type="file" class="btn btn-default btn-sm" id="image"
                                                                   name="image">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <img src="{{url('uploads/images/services/'.$serviceData->image)}}"
                                                             alt="" class="img-responsive"
                                                             style="margin-top: 23px">
                                                    </div>
                                                </div>

                                                <br>

                                                @if($serviceData->themeimage !== null)
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group form-group-lg">
                                                                <label for="image">Theme Image</label>
                                                                <input type="file" class="btn btn-default btn-sm"
                                                                       id="image"
                                                                       name="themeimage">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <img src="{{url('uploads/images/services/themeimage/'.$serviceData->themeimage)}}"
                                                         alt="" class="img-responsive"
                                                         style="margin-top: 23px">
                                                @endif
                                                <br>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label for="title">Did You Know</label>
                                                            <input type="text" name="diduknow"
                                                                   value="{{$serviceData->diduknow}}"
                                                                   class="form-control" required>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if($serviceData->innertitle !== '')
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group form-group-lg">
                                                                <label for="title">Inner Title</label>
                                                                <input type="text" name="innertitle" id="title"
                                                                       value="{{$serviceData->innertitle}}"
                                                                       class="form-control"
                                                                       placeholder="Enter Title of Service">
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                                @if($serviceData->innersubtitle !== '')
                                                    <div class="form-group form-group-lg">
                                                        <label for="subtitle">Inner Subtitle</label>
                                                        <textarea name="innersubtitle" id="description_id"
                                                                  class="form-control">{{$serviceData->innersubtitle}}</textarea>
                                                    </div>
                                                @endif

                                                <div class="form-group form-group-lg" style="margin-top: 5px">
                                                    <button class="btn btn-success"><i class="fa fa-save"></i>
                                                        Save Changes
                                                    </button>

                                                </div>
                                            </div>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection