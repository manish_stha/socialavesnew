@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('services')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Services</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Services in a Service</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">

                                @foreach ($errors->all() as $error)
                                    <h2 style="color: red"> {{$error}} </h2>
                                @endforeach

                                @include('backend.layouts.messages')

                                <div class="col-md-6">
                                    <form action="{{route('add-service-in-services')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <input type="hidden" name="service_id" value="{{$ServiceData->id}}">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label style="font-size: 24px">{{$ServiceData->title}}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Title of Service*</label>
                                                    <input type="text" name="title" id="title" value="{{old('title')}}"
                                                           class="form-control"
                                                           placeholder="Enter Title of Service" required>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="description">Description*</label>
                                            <textarea name="description"
                                                      class="form-control">{{old('description')}}</textarea>
                                        </div>

                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label>Select Required Field*</label>
                                                    <select class="form-control" required>
                                                        <option disabled selected>--Select Option--</option>
                                                        <option value="Image">Image</option>
                                                        <option value="Gallery">Gallery & Videos</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Image choose">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Image</label>
                                                    <input type="file" class="btn btn-default btn-sm" name="image">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Gallery choose">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Gallery</label>
                                                    <input type="file" class="btn btn-default btn-sm" name="gallery[]"
                                                           multiple>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Gallery choose">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label>Video Link</label>
                                                    <input type="text" name="link1" value="{{old('link1')}}"
                                                           class="form-control"
                                                           placeholder="Place the link for the First Video">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="video">Video Background Image</label>
                                                    <input type="file" name="preview1"
                                                           class="btn btn-default btn-sm">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Gallery choose">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label>Video Link</label>
                                                    <input type="text" name="link2" value="{{old('link2')}}"
                                                           class="form-control"
                                                           placeholder="Place the link for the Second Video">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="video">Video Background Image</label>
                                                    <input type="file" name="preview2"
                                                           class="btn btn-default btn-sm">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg" style="margin-top: 10px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Save Changes
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection