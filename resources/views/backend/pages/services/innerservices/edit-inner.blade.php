@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('services')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Services</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Update Service's Goals</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">

                                @include('backend.layouts.messages')

                                @foreach ($errors->all() as $error)
                                    <h2 style="color: red"> {{$error}} </h2>

                                @endforeach

                                <div class="col-md-6">
                                    <form action="{{route('edit-service-in-services-action')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <input type="hidden" name="criteria" value="{{$InnerData->id}}">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Title of Service*</label>
                                                    <input type="text" name="title" id="title"
                                                           value="{{$InnerData->title}}" class="form-control"
                                                           placeholder="Enter Title of Service" required>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="description">Description*</label>
                                            <textarea name="description"
                                                      class="form-control">{{$InnerData->description}}</textarea>
                                        </div>

                                        @if($InnerData->image !== null)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label for="image">Image</label>
                                                        <input type="file" class="btn btn-default btn-sm" name="image">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="{{url('uploads/images/services/inner-services/single-image/' .$InnerData->image)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail">
                                                </div>
                                            </div>
                                        @endif

                                        @if(count($galleryData))
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label for="image">Gallery</label>
                                                        <input type="file" class="btn btn-default btn-sm"
                                                               name="gallery[]"
                                                               multiple>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        <div class="row" style="margin-top: 10px">
                                            @foreach($galleryData as $data)
                                                <a href="{{route('delete-service-gallery').'/'.$data->id}}"
                                                   style="margin-left: 50%"
                                                   onclick="return confirm('Are you sure?')"
                                                   class="btn btn-sm btn-danger btn-round">X</a>
                                                <img src="{{url('uploads/images/services/inner-services/gallery/' .$data->gallery)}}"
                                                     alt=""
                                                     class="img-responsive thumbnail">
                                            @endforeach
                                        </div>

                                        @if($InnerData->link1 !== null OR $InnerData->link2 !== null)
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label>Video Link</label>
                                                        <input type="text" name="link1" value="{{$InnerData->link1}}"
                                                               class="form-control"
                                                               placeholder="Place the link for the First Video">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label for="video">Video Background Image</label>
                                                        <input type="file" name="preview1"
                                                               class="btn btn-default btn-sm">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <img src="{{url('uploads/images/services/inner-services/preview-images/' .$InnerData->preview1)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail">
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label>Video Link</label>
                                                        <input type="text" name="link2" value="{{$InnerData->link2}}"
                                                               class="form-control"
                                                               placeholder="Place the link for the Second Video">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label for="video">Video Background Image</label>
                                                        <input type="file" name="preview2"
                                                               class="btn btn-default btn-sm">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <img src="{{url('uploads/images/services/inner-services/preview-images/' .$InnerData->preview2)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail">
                                                </div>
                                            </div>
                                        @endif

                                        <div class="form-group form-group-lg" style="margin-top: 10px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Save Changes
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection