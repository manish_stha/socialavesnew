@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('users')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        All Users</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Update User Info</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <form action="{{route('edit-user-action')}}" method="post"
                                                  enctype="multipart/form-data">
                                                {{csrf_field()}}

                                                @include('backend.layouts.required')

                                                <input type="hidden" name="criteria" value="{{$adminData->id}}">

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label for="name">Full Name*</label>
                                                            <input type="text" id="name" name="fullname"
                                                                   value="{{$adminData->fullname}}"
                                                                   placeholder="Enter Your Full Name"
                                                                   class="form-control">
                                                            <a href=""
                                                               style="color: red">{{$errors->first('fullname')}}</a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label for="username">User Name*</label>
                                                            <input type="text" id="username" name="username"
                                                                   value="{{$adminData->username}}"
                                                                   placeholder="Enter User Name" class="form-control">
                                                            <a href=""
                                                               style="color: red">{{$errors->first('username')}}</a>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label for="privilege">Select User Privilege*</label>
                                                            <select name="privilege" class="form-control" required>
                                                                <option value="{{$adminData->privilege}}">{{$adminData->privilege}}
                                                                </option>
                                                                <option value="Super Admin">Super Admin</option>
                                                                <option value="Admin">Admin</option>
                                                                <option value="User">User</option>
                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label for="email">Email*</label>
                                                            <input type="text" id="email" name="email"
                                                                   value="{{$adminData->email}}"
                                                                   placeholder="Enter Your Email" class="form-control">
                                                            <a href=""
                                                               style="color: red">{{$errors->first('emaill')}}</a>

                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group form-group-lg" style="margin-top: 10px">
                                                    <button class="btn btn-success"><i class="fa fa-edit"></i> Update
                                                    </button>

                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection