@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('employee-story-list')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        Check List</a>
                    <a href="{{route('add-employee-story')}}" class="btn btn-success"><i
                                class="fa fa-pencil"></i>
                        Add New Story</a>

                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Employee Stories</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @include('backend.layouts.messages')

                        <div class="x_content">

                            <div class="row">

                                @foreach($storyData as $value)
                                    <div class="col-md-12 col-sm-4 col-xs-12 profile_details">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <div class="left col-xs-7">
                                                    <h2><b><?=strip_tags($value->header)?></b></h2>
                                                    <h5><b><?=strip_tags($value->title1)?></b></h5>
                                                    <p style="text-align: justify-all"><?=str_limit(strip_tags($value->description1), '400')?></p>
                                                    <p>
                                                        .................
                                                    </p>
                                                    <hr>
                                                    <ul class="list-unstyled" style="margin-top: 80px;;font-size: medium">
                                                        <li><i class="fa fa-pencil"></i> Created
                                                                At: {{$value->created_at->format('j-F,Y')}}
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="right col-xs-5 text-center">
                                                    <img src="{{url('uploads/images/stories/'.$value->image1)}}"
                                                         class="img-responsive" width="70%" height="50%" alt="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">
                                                <div class="col-xs-12 col-sm-6 emphasis">

                                                    <a href="{{route('edit-employee-story').'/'.$value->id}}"
                                                       class="btn btn-primary pull-left"><i
                                                                class="fa fa-edit"></i> Edit</a>
                                                    <form style="margin-left: 900px"
                                                          action="{{route('update-employee-story-status')}}"
                                                          method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="criteria" value="{{$value->id}}">
                                                        @if($value->status==1)
                                                            <button name="active" class="btn btn-danger">
                                                                 Hide
                                                            </button>
                                                        @else
                                                            <button name="inactive" class="btn btn-success">
                                                                 Publish
                                                            </button>
                                                        @endif
                                                    </form>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>


    </div>

    </div>
    <!-- /page content -->
@endsection

