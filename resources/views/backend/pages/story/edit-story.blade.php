@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12">
                    <a href="{{route('employee-story')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Stories</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Edit Employee Story Here</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{route('edit-employee-story-action')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        @include('backend.layouts.required')
                                        <input type="hidden" name="criteria" value="{{$storyData->id}}">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="name">Main Header*</label>
                                                    <input type="text" name="header" value="{{$storyData->header}}"
                                                           required="required"
                                                           placeholder="Enter Your Name" class="form-control">
                                                    <a href="" style="color: red">{{$errors->first('header')}}</a>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="upload">Employee's Name*</label>
                                                    <input type="text" name="name"
                                                           class="form-control" value="{{$storyData->name}}"
                                                           required="required">
                                                    <a href="" style="color: red">{{$errors->first('name')}}</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="upload">Designation*</label>
                                                    <input type="text" name="designation"
                                                           class="form-control" value="{{$storyData->designation}}"
                                                           required="required">
                                                    <a href="" style="color: red">{{$errors->first('designation')}}</a>
                                                </div>
                                            </div>
                                        </div>

                                        <br>


                                        <div class="form-group form-group-lg">
                                            <label for="upload">First Image</label>
                                            <input type="file" name="image1" class="btn btn-default btn-sm">
                                        </div>

                                        <div class="row">
                                            @if($storyData->image1 !== null)
                                                <div class="col-md-12">
                                                    <img src="{{url('uploads/images/stories/' .$storyData->image1)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail"
                                                         style="margin-top: 23px">
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="upload">Second Image*</label>
                                            <input type="file" name="image2"
                                                   class="btn btn-default btn-sm">
                                        </div>

                                        <div class="row">
                                            @if($storyData->image2 !== null)
                                                <div class="col-md-12">
                                                    <img src="{{url('uploads/images/stories/' .$storyData->image2)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail"
                                                         style="margin-top: 23px">
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="upload">Third Image*</label>
                                            <input type="file" name="image3"
                                                   class="btn btn-default btn-sm">
                                        </div>

                                        <div class="row">
                                            @if($storyData->image3 !== null)
                                                <div class="col-md-12">
                                                    <img src="{{url('uploads/images/stories/' .$storyData->image3)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail"
                                                         style="margin-top: 23px">
                                                </div>
                                            @endif
                                        </div>


                                        <br>

                                        <div class="form-group form-group-lg">
                                            <label for="title">Title*</label>
                                            <input type="text" id="title" name="title1" required="required"
                                                   value="{{$storyData->title1}}"
                                                   placeholder="Title of Content" class="form-control">
                                            <a href="" style="color: red">{{$errors->first('title1')}}</a>

                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="body">Description*</label>
                                            <textarea name="description1" required="required" id="description_id1"
                                                      class="form-control">{!! $storyData->description1 !!}</textarea>
                                            <a href="" style="color: red">{{$errors->first('description1')}}</a>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="title">Title*</label>
                                            <input type="text" id="title" name="title2" required="required"
                                                   value="{{$storyData->title2}}"
                                                   placeholder="Ticontroltle of Content" class="form-control">
                                            <a href="" style="color: red">{{$errors->first('title2')}}</a>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="body">Description*</label>
                                            <textarea name="description2" required="required" id="description_id"
                                                      class="form-control">{!! $storyData->description2 !!}</textarea>
                                            <a href="" style="color: red">{{$errors->first('description2')}}</a>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="title">Title</label>
                                            <input type="text" id="title" name="title3" required="required"
                                                   value="{{$storyData->title3}}" placeholder="Title of Content"
                                                   class="form-control">
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="body">Description</label>
                                            <textarea name="description3" required="required" id="description_id2"
                                                      class="form-control">{!! $storyData->description3 !!}</textarea>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="title">Title</label>
                                            <input type="text" name="title4" required="required"
                                                   value="{{$storyData->title4}}"
                                                   placeholder="Title of Content" class="form-control">
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="body">Description</label>
                                            <textarea name="description4" required="required" id="description_id3"
                                                      class="form-control">{!! $storyData->description4 !!}</textarea>
                                        </div>

                                        <div class="form-group form-group-lg" style="margin-top: 10px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Save Changes
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection