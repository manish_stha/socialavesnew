@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('employee-story')}}" class="btn btn-success"><i
                                class="fa fa-backward"></i>
                        Back</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Employee Story List</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Title</th>
                                        <th>Name</th>
                                        <th>Photo</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($storyData as $key=>$value)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$value->header}}</td>
                                            <td>{{$value->name}}</td>
                                            <td>
                                                <img src="{{url('uploads/images/stories/'.$value->image1)}}"
                                                     width="30"
                                                     alt="">
                                            </td>


                                            <td>
                                                <form action="{{route('update-employee-story-status')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="criteria" value="{{$value->id}}">
                                                    @if($value->status==1)
                                                        <button name="active" class="btn btn-success btn-xs">
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    @else
                                                        <button name="inactive" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                            </td>
                                            <td>{{$value->created_at->diffForHumans()}}</td>
                                            <td>
                                                <a href="{{route('edit-employee-story').'/'.$value->id}}"
                                                   class="btn btn-success btn-xs"><i class="fa fa-edit" title="Edit Story"></i></a>
                                                <a href="{{route('delete-employee-story').'/'.$value->id}}"
                                                   onclick="return confirm('Are you sure?')"
                                                   class="btn btn-danger btn-xs"><i
                                                            class="fa fa-trash" title="Delete Story"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>


                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection