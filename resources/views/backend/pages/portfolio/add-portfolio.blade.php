@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12">

                    <a href="{{route('portfolios')}}"
                       class="btn btn-success"><i
                                class="fa fa-list"></i> All Portfolios</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Make Portfolio Here</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')

                                @foreach ($errors->all() as $error)
                                    <h2 style="color: red"> {{$error}} </h2>
                                @endforeach

                                <div class="col-md-6">
                                    <form action="{{route('add-portfolio')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Title*</label>
                                                    <input type="text" id="name" name="title" value="{{old('title')}}"
                                                           required="required"
                                                           placeholder="Enter Title Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Preview
                                                        Image*
                                                    </label>
                                                    <input type="file" name="preview_image"
                                                           class="btn btn-default btn-sm" required>
                                                    <a href="#"
                                                       style="color: red">{{$errors->first('preview_image')}}</a>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="category">Select Category*</label>
                                                    <select name="category" class="form-control" required>
                                                        <option disabled selected value="">--Select Category--</option>
                                                        <option value="Branding">Branding</option>
                                                        <option value="Campaign">Campaign</option>
                                                        <option value="Client Project">Client Project</option>
                                                        <option value="Graphic Design">Graphic Design</option>
                                                        <option value="Infographics">Infographics</option>
                                                        <option value="Logo & Identity">Logo & Identity</option>
                                                        <option value="Microsite Development">Microsite Development
                                                        </option>
                                                        <option value="Photography">Photography</option>
                                                        <option value="Social Media Post">Social Media Post</option>
                                                        <option value="Video">Video</option>
                                                        <option value="Website">Website</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-top: 8px">
                                            <label for="body">Subtitle*</label>
                                            <textarea name="subtitle" class="form-control"></textarea>
                                            <a href="" style="color: red">{{$errors->first('subtitle')}}</a>
                                        </div>

                                        <br>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Client's Name*</label>
                                                    <input type="text" id="client" name="client"
                                                           value="{{old('client')}}"
                                                           required="required"
                                                           placeholder="Enter Client's Name Here"
                                                           class="form-control">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">URL*</label>
                                                    <input type="text" id="" name="url" value="{{old('url')}}"
                                                           required="required"
                                                           placeholder="Enter URL Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Platform*</label>
                                                    <input type="text" name="platform"
                                                           value="{{old('platform')}}"
                                                           required="required"
                                                           placeholder="Enter Platform Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Role*</label>
                                                    <input type="text" name="role"
                                                           value="{{old('role')}}"
                                                           required="required"
                                                           placeholder="Enter Platform Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group form-group-lg">
                                            <label for="body">Description</label>
                                            <textarea name="description" class="form-control"></textarea>
                                            <a href="" style="color: red">{{$errors->first('description')}}</a>
                                        </div>

                                        <br>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label>Select Required Field*</label>
                                                    <select class="form-control" required>
                                                        <option disabled selected>--Select Option--</option>
                                                        <option value="Image">Single Image</option>
                                                        <option value="Slider">Slider</option>
                                                        <option value="Video">Video</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Image choose">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Single
                                                        Image</label>
                                                    <input type="file" id="singleimage" name="singleimage"
                                                           class="btn btn-default btn-sm">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row Video choose" style="margin-top: 8px">
                                            <div class="col-md-8">
                                                <label for="none"
                                                       style="color: red;font-family: 'Montserrat';font-size: small">Insert
                                                    either Video or Video Link*</label>
                                            </div>
                                        </div>

                                        <div class="row Video choose">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="video">Video</label>
                                                    <input type="file" id="video" name="video"
                                                           class="btn btn-default btn-sm">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Video choose" style="margin-top: 10px">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="video">Video
                                                        Link</label>
                                                    <input type="text" name="link"
                                                           value="{{old('link')}}"
                                                           placeholder="Enter Link for Video" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Video choose" style="margin-top: 10px">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="video">Video Background Image</label>
                                                    <input type="file" name="thumbnail_image"
                                                           class="btn btn-default btn-sm">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Video choose" style="margin-top: 10px">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="">Video Title</label>
                                                    <input type="text" name="videotext"
                                                           value="{{old('videotext')}}"
                                                           placeholder="Enter Title for Video" class="form-control">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row Slider choose">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">First
                                                        Slider</label>
                                                    <input type="file" id="image1" name="slider1"
                                                           class="btn btn-default btn-sm" multiple>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Slider choose">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-lg">
                                                    <label for="video">Second
                                                        Slider</label>
                                                    <input type="file" name="slider2"
                                                           class="btn btn-default btn-sm">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row Slider choose">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-lg">
                                                    <label for="video">Third
                                                        Slider</label>
                                                    <input type="file" name="slider3"
                                                           class="btn btn-default btn-sm">
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <br>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Multiple Images</label>
                                                    <input type="file" name="gallery1[]"
                                                           class="btn btn-default btn-sm" multiple>

                                                    <br>
                                                </div>
                                            </div>
                                        </div>

                                        <br>

                                        <label for="body" style="font-family: 'Montserrat';font-size: medium">Select
                                            Images for Gallery :</label>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Gallery</label>
                                                    <input type="file" id="image1" name="gallery[]"
                                                           class="btn btn-default btn-sm" multiple>

                                                    <br>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group form-group-lg" style="margin-top: 25px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i>
                                                Save
                                                Portfolio
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->


@endsection

