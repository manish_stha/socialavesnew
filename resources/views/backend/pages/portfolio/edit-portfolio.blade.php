@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12">

                    <a href="{{route('portfolios')}}"
                       class="btn btn-success"><i
                                class="fa fa-list"></i> All Portfolios</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Edit Portfolio Here</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')

                                @foreach ($errors->all() as $error)
                                    <h2 style="color: red"> {{$error}} </h2>

                                @endforeach

                                <div class="col-md-6">
                                    <form action="{{route('edit-portfolio-action')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="criteria" value="{{$portfolioData->id}}">

                                        @include('backend.layouts.required')

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Title*</label>
                                                    <input type="text" id="name" name="title"
                                                           value="{{$portfolioData->title}}" required="required"
                                                           placeholder="Enter Title Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Preview Image*
                                                    </label>
                                                    <input type="file" name="preview_image"
                                                           class="btn btn-default btn-sm">
                                                    <a href="#"
                                                       style="color: red">{{$errors->first('preview_image')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img src="{{url('uploads/images/portfolio/preview_image/' .$portfolioData->preview_image)}}"
                                                     alt=""
                                                     class="img-responsive thumbnail" style="margin-top: 23px">
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="category">Select Category*</label>
                                                    <select name="category" class="form-control" required>
                                                        <option value="{{$portfolioData->category}}">{{$portfolioData->category}}</option>
                                                        <option value="Branding">Branding</option>
                                                        <option value="Campaign">Campaign</option>
                                                        <option value="Client Project">Client Project</option>
                                                        <option value="Graphic Design">Graphic Design</option>
                                                        <option value="Infographics">Infographics</option>
                                                        <option value="Logo & Identity">Logo & Identity</option>
                                                        <option value="Microsite Development">Microsite Development
                                                        </option>
                                                        <option value="Photography">Photography</option>
                                                        <option value="Social Media Post">Social Media Post</option>
                                                        <option value="Video">Video</option>
                                                        <option value="Website">Website</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg" style="margin-top: 8px">
                                            <label for="body">Subtitle*</label>
                                            <textarea name="subtitle" id="description_id"
                                                      class="form-control">{{$portfolioData->subtitle}}</textarea>
                                            <a href="" style="color: red">{{$errors->first('subtitle')}}</a>
                                        </div>


                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Client's Name*</label>
                                                    <input type="text" id="client" name="client"
                                                           value="{{$portfolioData->client}}"
                                                           required="required"
                                                           placeholder="Enter Client's Name Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">URL*</label>
                                                    <input type="text" id="" name="url" value="{{$portfolioData->url}}"
                                                           required="required"
                                                           placeholder="Enter URL Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Platform*</label>
                                                    <input type="text" id="" name="platform"
                                                           value="{{$portfolioData->platform}}"
                                                           required="required"
                                                           placeholder="Enter Platform Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Role*</label>
                                                    <input type="text" name="role"
                                                           value="{{$portfolioData->platform}}"
                                                           required="required"
                                                           placeholder="Enter Platform Here" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="body">Description</label>
                                            <textarea name="description" id="description_id1"
                                                      class="form-control">{{$portfolioData->description}}</textarea>
                                            <a href="" style="color: red">{{$errors->first('description')}}</a>
                                        </div>

                                        <br>

                                        @if($portfolioData->singleimage !== null)
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group form-group-lg">
                                                        <label for="image">Single Image</label>
                                                        <input type="file" id="singleimage" name="singleimage"
                                                               class="btn btn-default btn-sm">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="{{url('uploads/images/portfolio/single-image/' .$portfolioData->singleimage)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail" style="margin-top: 23px">
                                                </div>
                                            </div>
                                        @endif


                                        @if($portfolioData->video !== null OR $portfolioData->link !== null)

                                            @if($portfolioData->link !== null)
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group form-group-lg">
                                                            <label for="video">Video
                                                                Link</label>
                                                            <input type="text" name="link"
                                                                   value="{{$portfolioData->link}}"
                                                                   placeholder="Enter Link for Video"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            @if($portfolioData->video !== null)
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group form-group-lg">
                                                            <label for="video">Video</label>
                                                            <input type="file" id="video" name="video"
                                                                   class="btn btn-default btn-sm">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="row" style="margin-top: 8px">
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-lg">
                                                        <label for="video">Video Background Image</label>
                                                        <input type="file" name="thumbnail_image"
                                                               class="btn btn-default btn-sm">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="{{url('uploads/images/portfolio/thumbnail_image/' .$portfolioData->thumbnail_image)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail" style="margin-top: 23px">
                                                </div>
                                            </div>

                                            <div class="row" style="margin-top: 8px">
                                                <div class="col-md-12">
                                                    <div class="form-group form-group-lg">
                                                        <label for="">Video Title</label>
                                                        <input type="text" name="videotext"
                                                               value="{{$portfolioData->videotext}}"
                                                               placeholder="Enter Title for Video" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                        @endif

                                        @if($portfolioData->slider1 !== null OR $portfolioData->slider2 !== null OR $portfolioData->slider3 !== null)

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-lg">
                                                        <label for="image">First Slider</label>
                                                        <input type="file" id="image1" name="slider1"
                                                               class="btn btn-default btn-sm" multiple>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="{{url('uploads/images/portfolio/sliders/' .$portfolioData->slider1)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-lg">
                                                        <label for="video">Second Slider</label>
                                                        <input type="file" name="slider2"
                                                               class="btn btn-default btn-sm">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="{{url('uploads/images/portfolio/sliders/' .$portfolioData->slider2)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group form-group-lg">
                                                        <label for="video">Third Slider</label>
                                                        <input type="file" name="slider3"
                                                               class="btn btn-default btn-sm">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <img src="{{url('uploads/images/portfolio/sliders/' .$portfolioData->slider3)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail">
                                                </div>
                                            </div>

                                        @endif

                                        <br>
                                        <br>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-lg">
                                                    <label for="image">Multiple Images</label>
                                                    <input type="file" name="gallery1[]"
                                                           class="btn btn-default btn-sm" multiple>

                                                    <br>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            @foreach($FirstGalleryData as $data)
                                                <div class="col-md-6">
                                                    <a href="{{route('delete-portfolio-image').'/'.$data->id}}"
                                                       style="margin-left: 90%"
                                                       onclick="return confirm('Are you sure?')"
                                                       class="btn btn-sm btn-danger btn-round">X</a>
                                                    <img src="{{url('uploads/images/portfolio/gallery1/' .$data->gallery1)}}"
                                                         class="img-responsive thumbnail">
                                                </div>
                                            @endforeach

                                        </div>

                                        <br>
                                        <br>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group form-group-lg">
                                                    <label>Select
                                                        Images for Gallery :</label>
                                                    <input type="file" id="image1" name="gallery[]"
                                                           class="btn btn-default btn-sm" multiple>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            @foreach($galleryData as $data)
                                                <div class="col-md-6">
                                                    <br>
                                                    <a href="{{route('delete-portfolio-gallery').'/'.$data->id}}"
                                                       style="margin-left: 90%"
                                                       onclick="return confirm('Are you sure?')"
                                                       class="btn btn-sm btn-danger btn-round">X</a>
                                                    <img src="{{url('uploads/images/portfolio/gallery/' .$data->gallery)}}"
                                                         alt=""
                                                         class="img-responsive thumbnail">
                                                </div>
                                            @endforeach

                                        </div>

                                        <div class="form-group form-group-lg">
                                            <div class="form-group form-group-lg">
                                                <button class="btn btn-success"
                                                        style="margin-top: 25px"><i
                                                            class="fa fa-save"></i> Update
                                                </button>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <br>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection