@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('add-portfolio')}}" class="btn btn-success"><i
                                class="fa fa-plus"></i>
                        Add Portfolio</a>
                    <a href="{{route('portfolios')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        All Portfolios</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Portfolio List</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Subtitle</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($portfolioData as $key=>$data)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$data->title}}</td>
                                            <td><a href="#" class="btn btn-primary btn-xs"> {{$data->category}}</a>
                                            <td>{{str_limit(strip_tags($data->subtitle),'10')}}</td>
                                            <td>
                                                <form action="{{route('update-portfolio-status')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="criteria" value="{{$data->id}}">
                                                    @if($data->status==1)
                                                        <button name="active" class="btn btn-success btn-xs">
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    @else
                                                        <button name="inactive" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                            </td>
                                            <td>{{$data->created_at->diffForHumans()}}</td>
                                            <td>
                                                <a href="{{route('edit-portfolio').'/'.$data->id}}"
                                                   class="btn btn-success btn-xs"><i class="fa fa-edit"
                                                                                     title="Edit Portfolio"></i></a>
                                                <a href="{{route('delete-portfolio').'/'.$data->id}}"
                                                   onclick="return confirm('Are You Sure?')"
                                                   class="btn btn-danger btn-xs"><i
                                                            class="fa fa-trash" title="Delete Portfolio"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>


                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection