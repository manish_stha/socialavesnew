@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('add-portfolio')}}" class="btn btn-success"><i
                                class="fa fa-plus"></i> Add New Portfolio</a>

                    <a href="{{route('portfolio-list')}}" class="btn btn-success" style="margin-left: 15px"><i
                                class="fa fa-list"></i> Portfolio List</a>

                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Portfolios</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @include('backend.layouts.messages')

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                                            </div>

                                            <div class="clearfix"></div>

                                            @foreach($portfolioData as $key=>$data)

                                                <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                    <div class="well profile_view">
                                                        <div class="col-sm-12">
                                                            <img src="{{url('uploads/images/portfolio/preview_image/'.$data->preview_image)}}"
                                                                 class="img-responsive">

                                                            <h4 style="font-size: 16px;color: #23272b;margin-top: 15px;text-align: center"><?=strip_tags($data->title)?></h4>
                                                            <h5 style="text-align: center;font-size: 14px;color: #2A3F54">{{$data->client}}</h5>

                                                            <hr>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-pencil"></i> Created
                                                                    At: {{$data->created_at->format('m/d/Y H:i')}}
                                                                </li>
                                                            </ul>

                                                        </div>
                                                        <div class="col-xs-12 bottom text-center">
                                                            <div class="col-xs-12 col-sm-12 emphasis">

                                                                <a href="{{route('edit-portfolio').'/'.$data->id}}" class="btn btn-primary pull-left"><i
                                                                            class="fa fa-edit"></i> Edit</a>
                                                                <form action="{{route('update-portfolio-status')}}"
                                                                      method="post" class="pull-left">
                                                                    {{csrf_field()}}
                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$data->id}}">
                                                                    @if($data->status==1)
                                                                        <button name="active" class="btn btn-danger">
                                                                            Hide
                                                                        </button>
                                                                    @else
                                                                        <button name="inactive" class="btn btn-success">
                                                                            Publish
                                                                        </button>
                                                                    @endif
                                                                </form>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach

                                        </div>

                                        <div class="pull-right">
                                            {{$portfolioData->render()}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

