<?
use Illuminate\Support\Str;
?>

@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('testimonies')}}" class="btn btn-success"><i
                                class="fa fa-backward"></i>
                         Back</a>
                    <div class="x_panel" style="margin-top: 3px">
                        <div class="x_title">
                            <h2>Testimonies</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th>Testimony</th>
                                        <th>Designation</th>
                                        <th>Links</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($testimonyData as $key=>$testimony)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$testimony->name}}</td>
                                            <td>
                                                <img src="{{url('uploads/images/our-testimony/'.$testimony->image)}}"
                                                     width="30"
                                                     alt="">
                                            </td>
                                            <td><?= \Illuminate\Support\Str::limit(strip_tags($testimony->testimony), '200')?></td>
                                            <td>
                                                <a href="#"
                                                   class="btn btn-primary btn-xs">{{$testimony->designation}}</a>
                                            </td>
                                            <td>
                                                <a href="{{$testimony->facebook}}" class="btn btn-xs"><i
                                                            class="fa fa-facebook"></i></a>
                                                <a href="{{$testimony->instagram}}" class="btn btn-xs"><i
                                                            class="fa fa-instagram"></i></a>
                                                <a href="{{$testimony->linkedin}}" class="btn btn-xs"><i
                                                            class="fa fa-linkedin"></i></a>
                                                <a href="{{$testimony->youtube}}" class="btn btn-xs"><i
                                                            class="fa fa-youtube-play"></i></a
                                                >
                                            </td>
                                            <td>
                                                <form action="{{route('update-testimony-status')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="criteria" value="{{$testimony->id}}">
                                                    @if($testimony->status==1)
                                                        <button name="active" class="btn btn-success btn-xs">
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    @else
                                                        <button name="inactive" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                            </td>
                                            <td>
                                                <a href="{{route('edit-testimony').'/'.$testimony->id}}"
                                                   class="btn btn-success btn-xs"><i class="fa fa-edit" title="Edit"></i></a>
                                                <a href="{{route('delete-testimony').'/'.$testimony->id}}"
                                                   onclick="return confirm('Delete related quote first.')"
                                                   class="btn btn-danger btn-xs"><i
                                                            class="fa fa-trash" title="Delete"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>


                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection