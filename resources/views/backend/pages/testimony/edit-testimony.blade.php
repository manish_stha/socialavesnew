@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('testimonies')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Testimonies</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Update Testimony</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{route('edit-testimony-action')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <input type="hidden" value="{{$testimonyData->id}}" name="criteria">


                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="name">Name*</label>
                                                    <input type="text" id="name" name="name"
                                                           placeholder="Enter Your Name"
                                                           class="form-control" value="{{$testimonyData->name}}"
                                                           required="required">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="image">Photo*</label>
                                            <input type="file" id="image" class="btn btn-default btn-sm"
                                                   name="image">
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <img src="{{url('uploads/images/our-testimony/' .$testimonyData->image)}}"
                                                     alt=""
                                                     class="img-responsive thumbnail" style="margin-top: 23px">
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="testimony">Testimony*</label>
                                            <textarea name="testimony" required id="description_id"
                                                      class="form-control">{{$testimonyData->testimony}}</textarea>
                                            <a href="" style="color: red">{{$errors->first('testimony')}}</a>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="designation">Designation*</label>
                                                    <input type="text" id="writer" name="designation"
                                                           placeholder="Put Your Designation"
                                                           class="form-control"
                                                           value="{{$testimonyData->designation}}"
                                                           required="required">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group form-group-lg" style="margin-top: 10px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Save
                                                Changes
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection