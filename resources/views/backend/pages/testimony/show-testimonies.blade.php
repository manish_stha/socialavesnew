@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('team-testimony-list')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        Check List</a>
                    <a href="{{route('add-testimony')}}" class="btn btn-success" style="margin-left: 15px"><i
                                class="fa fa-plus"></i>
                        Add New Testimony</a>

                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Team Testimonies</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @include('backend.layouts.messages')

                        <div class="x_content">

                            <div class="row">


                                @foreach($teamHeaderData as $header)

                                    <div class="col-md-6 col-sm-4 col-xs-12 profile_details ">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <h4 class="brief"><b>Team Title &
                                                        Subtitle</b>
                                                </h4>
                                                <div class="left col-xs-12">
                                                    <p><strong>Title: {{ strip_tags($header->title)}} </strong></p>
                                                    <p>
                                                        <strong>Subtitle: <?= strip_tags($header->subtitle)?></strong>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">

                                                <div class="col-xs-12 col-sm-6 emphasis">

                                                    <button type="button" data-toggle="modal"
                                                            data-target="#{{$header->id}}"
                                                            class="btn btn-primary btn-sm pull-left">
                                                        <i class="fa fa-edit"> </i> Edit
                                                    </button>

                                                    {{--modal begins--}}
                                                    <div class="modal fade" id="{{$header->id}}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="basicModal" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-hidden="true">&times;
                                                                    </button>
                                                                    <h4 class="modal-title" id="myModalLabel"
                                                                        style="font-family: Montserrat"><b>Update
                                                                            Team Header Here</b></h4>
                                                                </div>
                                                                <form action="{{route('edit-home-team-header-action')}}"
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    {{csrf_field()}}
                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$header->id}}">

                                                                    <div class="col-sm-12" style="margin-top: 5px">
                                                                        <div class="form-group form-group-lg">
                                                                            <label for="title">Title</label>
                                                                            <textarea name="title"
                                                                                      required="required"
                                                                                      id="description_id"
                                                                                      class="form-control">{{$header->title}}</textarea>
                                                                            <a href=""
                                                                               style="color: red">{{$errors->first('title')}}</a>

                                                                        </div>
                                                                        <div class="form-group form-group-lg">
                                                                            <label for="body">Subtitle</label>
                                                                            <textarea name="subtitle"
                                                                                      required="required"
                                                                                      id="description_id1"
                                                                                      class="form-control">{{$header->subtitle}}</textarea>
                                                                            <a href=""
                                                                               style="color: red">{{$errors->first('subtitle')}}</a>

                                                                        </div>
                                                                        <button class="btn btn-primary"><i
                                                                                    class="fa fa-save"></i> Save
                                                                            Changes
                                                                        </button>
                                                                    </div>

                                                                </form>


                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{--modal ends--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                                <div class="col-md-12">
                                    <div class="x_panel">
                                        <div class="x_content">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 text-center"></div>
                                                <div class="clearfix"></div>

                                                @foreach($testimonyData as $testimony)

                                                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                        <div class="well profile_view">
                                                            <div class="col-sm-12">
                                                                <img src="{{url('uploads/images/our-testimony/'.$testimony->image)}}"
                                                                     class="img-responsive">

                                                                <h4 style="font-size: 16px;color: #23272b;margin-top: 15px;text-align: center"><?=strip_tags($testimony->name)?></h4>
                                                                <h5 style="text-align: center;font-size: 14px;color: #2A3F54">{{$testimony->designation}}</h5>

                                                                <hr>
                                                                <ul class="list-unstyled">
                                                                    <li><i class="fa fa-pencil"></i> Created
                                                                        At: {{$testimony->created_at->format('m/d/Y H:i')}}
                                                                    </li>
                                                                </ul>

                                                            </div>
                                                            <div class="col-xs-12 bottom text-center">
                                                                <div class="col-xs-12 col-sm-12 emphasis">

                                                                    <a href="{{route('edit-testimony').'/'.$testimony->id}}"
                                                                       class="btn btn-primary pull-left"><i
                                                                                class="fa fa-edit"></i> Edit</a>
                                                                    <form action="{{route('update-testimony-status')}}"
                                                                          method="post" class="pull-left">
                                                                        {{csrf_field()}}
                                                                        <input type="hidden" name="criteria"
                                                                               value="{{$testimony->id}}">
                                                                        @if($testimony->status==1)
                                                                            <button name="active"
                                                                                    class="btn btn-danger">
                                                                                Hide
                                                                            </button>
                                                                        @else
                                                                            <button name="inactive"
                                                                                    class="btn btn-success">
                                                                                Publish
                                                                            </button>
                                                                        @endif
                                                                    </form>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach

                                            </div>

                                            <div class="pull-right">
                                                {{$testimonyData->render()}}
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>


                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- /page content -->
@endsection

