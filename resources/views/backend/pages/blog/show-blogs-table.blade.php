@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('blogs')}}" style="font-size: small;" class="btn btn-success btn-xs"><i
                                class="fa fa-backward"></i>
                        Back</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Blog List</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Blogger's Name</th>
                                        <th>Profile</th>
                                        <th>Title</th>
                                        <th>Outer</th>
                                        <th>Inner</th>
                                        <th>Video</th>
                                        <th>Blog Type</th>
                                        {{--<th>Related Quote</th>--}}
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($blogData as $key=>$blog)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{$blog->name}}</td>
                                            <td>
                                                <img src="{{url('uploads/images/blogger/'.$blog->upload)}}"
                                                     width="30"
                                                     alt="">
                                            </td>
                                            <td>{{$blog->title}}</td>
                                            <td>
                                                <img src="{{url('uploads/images/blogs/'.$blog->upload1)}}"
                                                     width="30"
                                                     alt="">
                                            </td>

                                            <td>
                                                <img src="{{url('uploads/images/blogs/'.$blog->upload2)}}"
                                                     width="30"
                                                     alt="">
                                            </td>
                                            <td>
                                                <video src="{{url('uploads/videos/blogs/'.$blog->video)}}"
                                                       width="60" controls></video>
                                            </td>
                                            <td><a href="#" class="btn btn-primary btn-xs"> {{$blog->blog_type}}</a>
                                            </td>


                                            <td>
                                                <form action="{{route('update-blog-status')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="criteria" value="{{$blog->id}}">
                                                    @if($blog->status==1)
                                                        <button name="active" class="btn btn-success btn-xs">
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    @else
                                                        <button name="inactive" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                            </td>
                                            <td>{{$blog->created_at->diffForHumans()}}</td>
                                            <td>
                                                <a href="{{route('edit-blog').'/'.$blog->id}}"
                                                   class="btn btn-success btn-xs"><i class="fa fa-edit" title="Edit Blog"></i></a>
                                                <a href="{{route('delete-blog').'/'.$blog->id}}"
                                                   onclick="return confirm('Delete related quote, comments and replies first.')"
                                                   class="btn btn-danger btn-xs"><i
                                                            class="fa fa-trash" title="Delete Blog"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>


                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection