@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('quotes')}}" class="btn btn-success"><i
                                class="fa fa-backward"></i> All Quotes</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Edit Quote</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{route('edit-quote-action')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <input type="hidden" name="criteria" value="{{$quoteData->id}}">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-sm">
                                                    <label for="title">Select Blog Title*</label>
                                                    <select name="title" class="form-control">
                                                        <option disabled selected value="">---Select Blog Title---
                                                        </option>
                                                        @foreach ($blogData as $key=>$blog)
                                                            <option value="{{$blog->id}}" {{$quoteData->blog_id==$blog->id ? 'selected' : ''}}>{{ucfirst($blog->title)}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-sm">
                                            <label for="quote">Quote*</label>
                                            <textarea name="quote" required="required" id="description_id"
                                                      class="form-control">{{$quoteData->quote}}</textarea>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-sm">
                                                    <label for="writer">Writer*</label>
                                                    <input type="text" id="writer" name="writer"
                                                           value="{{$quoteData->writer}}"
                                                           placeholder="Enter Name of Quote Writer"
                                                           class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-sm">
                                            <button class="btn btn-success btn-sm"><i class="fa fa-save"></i> Save
                                                Changes
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection