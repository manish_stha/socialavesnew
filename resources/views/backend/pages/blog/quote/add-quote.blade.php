@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('quotes')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Quotes</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Write Quote Here</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{route('add-quote')}}" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        @include('backend.layouts.required')

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Select Blog Title*</label>
                                                    <select name="title" class="form-control" required="required">
                                                        <option disabled selected value="">---Select Blog Title---
                                                        </option>
                                                        @foreach ($blogData as $key=>$blog)
                                                            <option value="{{$blog->id}}">{{ucfirst($blog->title)}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-lg">
                                            <label for="quote">Quote*</label>
                                            <textarea name="quote" required="required" id="description_id"
                                                      class="form-control"></textarea>
                                            <a href="" style="color: red">{{$errors->first('quote')}}</a>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="writer">Writer*</label>
                                                    <input type="text" id="writer" name="writer"
                                                           value="{{old('writer')}}"
                                                           placeholder="Enter Name of Quote Writer"
                                                           class="form-control" required="required">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg" style="margin-top: 10px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Save Quote
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection