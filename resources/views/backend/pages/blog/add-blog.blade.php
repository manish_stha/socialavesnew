@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12">
                    <a href="{{route('blogs')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Blogs</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Create Your Blog Here</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{route('add-blog')}}" method="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        @include('backend.layouts.required')

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="name">Blogger's Name*</label>
                                                    <input type="text" id="name" name="name" required="required"
                                                           placeholder="Enter Your Name" class="form-control">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="upload">Blogger's Image*</label>
                                            <input type="file" id="upload" name="upload"
                                                   class="btn btn-default btn-sm" required="required">

                                        </div>


                                        <div class="form-group form-group-lg">
                                            <label for="title">Title*</label>
                                            <input type="text" id="title" name="title" required="required"
                                                   value="{{old('title')}}"
                                                   placeholder="Enter Title of Your Blog" class="form-control">
                                        </div>
                                        <div class="form-group form-group-lg" id="summernote">
                                            <label for="body">Body*</label>
                                            <textarea name="body" required="required"
                                                      class="form-control"></textarea>
                                            <a href="" style="color: red">{{$errors->first('body')}}</a>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="cat_name">Select Type of Blog*</label>
                                                    <select name="blog_type" class="form-control" required>
                                                        <option disabled selected value="">--Select Type--</option>
                                                        <option value="Normal">Normal</option>
                                                        <option value="Gallery">Gallery</option>
                                                        <option value="Video">Video</option>
                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="upload1">Preview Image*</label>
                                            <input type="file" id="upload1" name="upload1"
                                                   class="btn btn-default btn-sm">

                                        </div>
                                        <div class="form-group form-group-lg">
                                            <label for="upload2">Inner Page Image*</label>
                                            <input type="file" id="upload2" name="upload2"
                                                   class="btn btn-default btn-sm">
                                        </div>
                                        <div class="form-group form-group-lg">
                                            <label for="video">Video</label>
                                            <input type="file" id="video" name="video"
                                                   class="btn btn-default btn-sm">
                                        </div>

                                        <div class="form-group form-group-lg" style="margin-top: 20px">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Save Blog
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection
