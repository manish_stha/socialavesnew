@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">

                    <!--<a data-toggle="modal"-->
                    <!--   data-target="#AddClient" class="btn btn-success"><i-->
                    <!--            class="fa fa-plus"></i>-->
                    <!--    Add Facts</a>-->

                    <!--{{--modal begins--}}-->
                    <!--<div class="modal fade" id="AddClient" tabindex="-1" role="dialog"-->
                    <!--     aria-labelledby="basicModal" aria-hidden="true">-->
                    <!--    <div class="modal-dialog">-->
                    <!--        <div class="modal-content">-->
                    <!--            <div class="modal-header">-->
                    <!--                <button type="button" class="close" data-dismiss="modal"-->
                    <!--                        aria-hidden="true">&times;-->
                    <!--                </button>-->
                    <!--                <h4 class="modal-title" id="myModalLabel"><b>Add Here</b></h4>-->
                    <!--            </div>-->
                    <!--            <form action="{{route('add-fact')}}" method="post"-->
                    <!--                  enctype="multipart/form-data">-->
                    <!--                {{csrf_field()}}-->

                    <!--                <div class="row">-->
                    <!--                    <div class="col-sm-11" style="margin-top: 5px;margin-left: 10px">-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-md-6">-->
                    <!--                                <div class="form-group form-group-lg">-->
                    <!--                                    <label for="name">Title</label>-->
                    <!--                                    <input name="title1" required="required" class="form-control">-->
                    <!--                                </div>-->
                    <!--                            </div>-->

                    <!--                            <div class="col-md-5">-->
                    <!--                                <div class="form-group form-group-lg">-->
                    <!--                                    <label for="name">Data</label>-->
                    <!--                                    <input name="number1" required="required" class="form-control">-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->

                    <!--                        <div class="row">-->
                    <!--                            <div class="col-md-6">-->
                    <!--                                <div class="form-group form-group-lg">-->
                    <!--                                    <label for="name">Title</label>-->
                    <!--                                    <input name="title2" required="required" class="form-control">-->
                    <!--                                </div>-->
                    <!--                            </div>-->

                    <!--                            <div class="col-md-5">-->
                    <!--                                <div class="form-group form-group-lg">-->
                    <!--                                    <label for="name">Data</label>-->
                    <!--                                    <input name="number2" required="required" class="form-control">-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-md-6">-->
                    <!--                                <div class="form-group form-group-lg">-->
                    <!--                                    <label for="name">Title</label>-->
                    <!--                                    <input name="title3" required="required" class="form-control">-->
                    <!--                                </div>-->
                    <!--                            </div>-->

                    <!--                            <div class="col-md-5">-->
                    <!--                                <div class="form-group form-group-lg">-->
                    <!--                                    <label for="name">Data</label>-->
                    <!--                                    <input name="number3" required="required" class="form-control">-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <div class="row">-->
                    <!--                            <div class="col-md-6">-->
                    <!--                                <div class="form-group form-group-lg">-->
                    <!--                                    <label for="name">Title</label>-->
                    <!--                                    <input name="title4" required="required" class="form-control">-->
                    <!--                                </div>-->
                    <!--                            </div>-->

                    <!--                            <div class="col-md-5">-->
                    <!--                                <div class="form-group form-group-lg">-->
                    <!--                                    <label for="name">Data</label>-->
                    <!--                                    <input name="number4" required="required" class="form-control">-->
                    <!--                                </div>-->
                    <!--                            </div>-->
                    <!--                        </div>-->


                    <!--                        <button class="btn btn-primary" style="margin-top: 10px"><i-->
                    <!--                                    class="fa fa-save"></i> Save-->
                    <!--                        </button>-->
                    <!--                    </div>-->
                    <!--                </div>-->


                    <!--            </form>-->


                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</div>-->
                    <!--{{--modal ends--}}-->

                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Facts About Social Aves</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @include('backend.layouts.messages')

                        <div class="x_content">

                            <div class="row">

                                @foreach($FactData as $data)
                                    <div class="col-md-6 col-sm-4 col-xs-12 profile_details">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <h2>{{$data->title1}}: <b>{{$data->number1}}</b></h2>
                                                <h2>{{$data->title2}}: <b>{{$data->number2}}</b></h2>
                                                <h2>{{$data->title3}}: <b>{{$data->number3}}</b></h2>
                                                <h2>{{$data->title4}}: <b>{{$data->number4}}</b></h2>

                                                <hr>

                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-pencil"></i> Created
                                                        At: {{$data->created_at->diffForHumans()}}</li>
                                                </ul>

                                            </div>

                                            <div class="col-xs-12 bottom text-center">
                                                <div class="col-xs-12 col-sm-6 emphasis">

                                                    <button type="button" data-toggle="modal"
                                                            data-target="#{{$data->id}}"
                                                            class="btn btn-primary pull-left">
                                                        <i class="fa fa-edit"> </i> Edit
                                                    </button>

                                                </div>
                                            </div>

                                            {{--modal begins--}}
                                            <div class="modal fade" id="{{$data->id}}" tabindex="-1" role="dialog"
                                                 aria-labelledby="basicModal" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-hidden="true">&times;
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel"><b>Update Data Here</b>
                                                            </h4>
                                                        </div>
                                                        <form action="{{route('edit-fact-action')}}" method="post"
                                                              enctype="multipart/form-data">
                                                            {{csrf_field()}}

                                                            <input type="hidden" name="criteria" value="{{$data->id}}"
                                                                   class="form-control">

                                                            <div class="row">
                                                                <div class="col-sm-11"
                                                                     style="margin-top: 5px;margin-left: 10px">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group form-group-lg">
                                                                                <label for="name">Title</label>
                                                                                <input name="title1"
                                                                                       value="{{$data->title1}}"
                                                                                       required="required"
                                                                                       class="form-control">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-5">
                                                                            <div class="form-group form-group-lg">
                                                                                <label for="name">Data</label>
                                                                                <input name="number1"
                                                                                       value="{{$data->number1}}"
                                                                                       required="required"
                                                                                       class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group form-group-lg">
                                                                                <label for="name">Title</label>
                                                                                <input name="title2"
                                                                                       value="{{$data->title2}}"
                                                                                       required="required"
                                                                                       class="form-control">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-5">
                                                                            <div class="form-group form-group-lg">
                                                                                <label for="name">Data</label>
                                                                                <input name="number2"
                                                                                       value="{{$data->number2}}"
                                                                                       required="required"
                                                                                       class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group form-group-lg">
                                                                                <label for="name">Title</label>
                                                                                <input name="title3"
                                                                                       value="{{$data->title3}}"
                                                                                       required="required"
                                                                                       class="form-control">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-5">
                                                                            <div class="form-group form-group-lg">
                                                                                <label for="name">Data</label>
                                                                                <input name="number3"
                                                                                       value="{{$data->number3}}"
                                                                                       required="required"
                                                                                       class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group form-group-lg">
                                                                                <label for="name">Title</label>
                                                                                <input name="title4"
                                                                                       value="{{$data->title4}}"
                                                                                       required="required"
                                                                                       class="form-control">
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-5">
                                                                            <div class="form-group form-group-lg">
                                                                                <label for="name">Data</label>
                                                                                <input name="number4"
                                                                                       value="{{$data->number4}}"
                                                                                       required="required"
                                                                                       class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <button class="btn btn-primary" style="margin-top: 10px"><i
                                                                                class="fa fa-save"></i> Save Changes
                                                                    </button>
                                                                </div>
                                                            </div>


                                                        </form>


                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                            </div>
                            @endforeach


                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /page content -->
@endsection
