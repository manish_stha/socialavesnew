@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">


                <div class="title_left">
                    <a href="{{route('logos')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> Logo Actions</a>
                    <a href="{{route('videos')}}" class="btn btn-success" style="margin-left: 15px"><i
                                class="fa fa-list"></i> Banner Video Actions</a>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_content">

                            @include('backend.layouts.messages')

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                </div>

                                <div class="clearfix"></div>

                                @foreach($logoData as $logo)
                                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <h4 class="brief" style="font-family: Helvetica"><b>Website Logo</b>
                                                </h4>
                                                <div class="right col-xs-5 text-center">
                                                    <img style="margin-left: 80px"
                                                         src="{{url('uploads/images/logo/'.$logo->logo)}}" height="95"
                                                         width="90" alt=""
                                                         class="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">

                                                <div class="col-xs-12 col-sm-6 emphasis center-margin">
                                                    <button type="button" class="btn btn-primary btn-xs"
                                                            data-toggle="modal"
                                                            data-target="#{{$logo->id}}">
                                                        <i class="fa fa-exchange"> </i> Change Logo
                                                    </button>
                                                    {{--modal begins--}}
                                                    <div class="modal fade" id="{{$logo->id}}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="basicModal" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-hidden="true">&times;
                                                                    </button>
                                                                    <h4 class="modal-title" id="myModalLabel"><b>Change
                                                                            Logo</b></h4>
                                                                </div>
                                                                <form action="{{route('edit-logo-action')}}"
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$logo->id}}">
                                                                    {{csrf_field()}}
                                                                    <div class="row"
                                                                         style="margin-left: 2px;margin-top: 1px">
                                                                        <div class="col-sm-12">
                                                                            <input style="text-align: center;margin-left: 160px"
                                                                                   type="file"
                                                                                   id="logo" name="logo"
                                                                                   class="btn btn-default btn-sm"
                                                                                   required>
                                                                        </div>
                                                                    </div>
                                                                    <button style="text-align: center;margin-top: 10px"
                                                                            class="btn btn-primary btn-sm"><i
                                                                                class="fa fa-save"></i> Save
                                                                        Logo
                                                                    </button>
                                                                </form>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{--modal ends--}}

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                                @foreach($videoData as $video)
                                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <h4 class="brief" style="font-family: Helvetica"><b>Background Video</b>
                                                </h4>
                                                <div class="right col-xs-5 text-center">
                                                    <video src="{{url('uploads/videos/home/banner/' .$video->video)}}"
                                                           controls
                                                           width="250" height="65" style="margin-top: 23px">
                                                    </video>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">

                                                <div class="col-xs-12 col-sm-6 emphasis center-margin">
                                                    <button type="button" class="btn btn-primary btn-xs"
                                                            data-toggle="modal"
                                                            data-target="#{{$video->id}}">
                                                        <i class="fa fa-exchange"> </i> Change Video
                                                    </button>
                                                    {{--modal begins--}}
                                                    <div class="modal fade" id="{{$video->id}}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="basicModal" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-hidden="true">&times;
                                                                    </button>
                                                                    <h4 class="modal-title" id="myModalLabel"><b>Change
                                                                            Video</b></h4>
                                                                </div>
                                                                <form action="{{route('edit-video-action')}}"
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    {{csrf_field()}}
                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$video->id}}">
                                                                    <div class="row"
                                                                         style="margin-left: 2px;margin-top: 1px">

                                                                        <div class="col-sm-12">
                                                                            <input style="text-align: center;margin-left: 160px"
                                                                                   type="file"
                                                                                   id="video" name="video"
                                                                                   class="btn btn-default btn-sm"
                                                                                   required>
                                                                        </div>

                                                                        <div class="col-sm-12">
                                                                            <button style="text-align: center;margin-top: 10px"
                                                                                    class="btn btn-primary btn-sm"><i
                                                                                        class="fa fa-save"></i> Save
                                                                                Video
                                                                            </button>
                                                                        </div>

                                                                    </div>


                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{--modal ends--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

