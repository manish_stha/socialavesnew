@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-success"
                       data-toggle="modal"
                       data-target="#basicModal"><i
                                class="fa fa-plus"></i>
                        Add Brands</a>

                    {{--modal begins--}}
                    <div class="modal fade" id="basicModal" tabindex="-1" role="dialog"
                         aria-labelledby="basicModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close"
                                            data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel"><b>Add
                                            Logo Here</b></h4>
                                </div>
                                <form action="{{route('add-brand')}}"
                                      method="post"
                                      enctype="multipart/form-data">

                                    {{csrf_field()}}
                                    <div class="row"
                                         style="margin-left: 2px;margin-top: 1px">

                                        <div class="col-sm-12">
                                            <input style="margin-top: 5px;margin-left: 160px" type="file"
                                                   id="logo" name="image"
                                                   class="btn btn-default btn-sm"
                                                   required>
                                        </div>
                                    </div>
                                    <button style="margin-left: 250px;margin-top: 10px"
                                            class="btn btn-primary btn-sm"><i
                                                class="fa fa-save"></i> Save
                                        Brand
                                    </button>
                                </form>

                            </div>
                        </div>
                    </div>
                    {{--modal ends--}}

                    <a href="{{route('client-brands-list')}}"
                       class="btn btn-success" style="margin-left: 15px"><i
                                class="fa fa-list"></i> Brands List</a>

                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Brands Who Have Trusted Us</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @include('backend.layouts.messages')

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-center"></div>
                                            <div class="clearfix"></div>

                                            @foreach($brandData as $key=>$data)

                                                <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                    <div class="well profile_view">
                                                        <div class="col-sm-12">
                                                            <div class="col-xs-5 center-margin">

                                                                <img src="{{url('uploads/images/home/brands/'.$data->image)}}"
                                                                     class="img-responsive" alt="">

                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 bottom text-center">
                                                            <div class="col-xs-12 col-sm-12 emphasis">

                                                                <a class="btn btn-success btn-sm pull-left"
                                                                   data-toggle="modal"
                                                                   data-target="#{{$data->id}}"><i
                                                                            class="fa fa-edit"></i> Edit</a>
                                                                {{--modal begins--}}
                                                                <div class="modal fade" id="{{$data->id}}" tabindex="-1"
                                                                     role="dialog"
                                                                     aria-labelledby="basicModal" aria-hidden="true">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal"
                                                                                        aria-hidden="true">&times;
                                                                                </button>
                                                                                <h4 class="modal-title"
                                                                                    id="myModalLabel"><b>Change
                                                                                        Logo Here</b></h4>
                                                                            </div>
                                                                            <form action="{{route('edit-brand-action')}}"
                                                                                  method="post"
                                                                                  enctype="multipart/form-data">

                                                                                <input type="hidden" name="criteria"
                                                                                       value="{{$data->id}}">

                                                                                {{csrf_field()}}
                                                                                <div class="row"
                                                                                     style="margin-left: 2px;margin-top: 1px">

                                                                                    <div class="col-sm-12">
                                                                                        <input style="margin-top: 5px;margin-left: 160px"
                                                                                               type="file"
                                                                                               id="logo" name="image"
                                                                                               class="btn btn-default btn-sm"
                                                                                               required>
                                                                                    </div>
                                                                                </div>
                                                                                <button style="margin-top: 10px"
                                                                                        class="btn btn-primary btn-sm">
                                                                                    <i
                                                                                            class="fa fa-save"></i> Save
                                                                                    Brand
                                                                                </button>
                                                                            </form>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                {{--modal ends--}}

                                                                <form action="{{route('update-brand-status')}}"
                                                                      method="post" class="pull-left">
                                                                    {{csrf_field()}}
                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$data->id}}">
                                                                    @if($data->status==1)
                                                                        <button name="active"
                                                                                class="btn btn-danger btn-sm">
                                                                            Hide
                                                                        </button>
                                                                    @else
                                                                        <button name="inactive"
                                                                                class="btn btn-success btn-sm">
                                                                            Publish
                                                                        </button>
                                                                    @endif
                                                                </form>


                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

