@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">


                <div class="title_left">
                    <div class="container">
                        <a href="{{route('client-brands')}}"
                           class="btn btn-success"><i
                                    class="fa fa-backward"></i>
                            Back</a>

                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-md-12">

                    @include('backend.layouts.messages')

                    <div class="x_panel">
                        <h2 style="font-family: Cambria">Brands Who Have Trusted Us</h2>
                        <hr>


                        <div class="x_content">


                            <div class="row">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Brands</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    @foreach($brandData as $key=>$value)
                                        <tbody>
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>
                                                <img src="{{url('uploads/images/home/brands/'.$value->image)}}"
                                                     width="30"
                                                     alt="">
                                            </td>
                                            <td>
                                                <form action="{{route('update-brand-status')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="criteria" value="{{$value->id}}">
                                                    @if($value->status==1)
                                                        <button name="active" class="btn btn-success btn-xs">
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    @else
                                                        <button name="inactive" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                            </td>
                                            <td>
                                                {{$value->created_at->diffForHumans()}}
                                            </td>
                                            <td>

                                                <a class="btn btn-success btn-xs" data-toggle="modal"
                                                   data-target="#{{$value->id}}"><i class="fa fa-edit"></i></a>
                                                {{--modal begins--}}
                                                <div class="modal fade" id="{{$value->id}}" tabindex="-1" role="dialog"
                                                     aria-labelledby="basicModal" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close"
                                                                        data-dismiss="modal"
                                                                        aria-hidden="true">&times;
                                                                </button>
                                                                <h4 class="modal-title" id="myModalLabel"><b>Change
                                                                        Logo Here</b></h4>
                                                            </div>
                                                            <form action="{{route('edit-brand-action')}}"
                                                                  method="post"
                                                                  enctype="multipart/form-data">

                                                                <input type="hidden" name="criteria"
                                                                       value="{{$value->id}}">

                                                                {{csrf_field()}}
                                                                <div class="row"
                                                                     style="margin-left: 2px;margin-top: 1px">

                                                                    <div class="col-sm-12">
                                                                        <input style="margin-top: 5px;margin-left: 160px"
                                                                               type="file"
                                                                               id="logo" name="image"
                                                                               class="btn btn-default btn-sm"
                                                                               required>
                                                                    </div>
                                                                </div>
                                                                <button style="margin-left: 250px;margin-top: 10px"
                                                                        class="btn btn-primary btn-sm"><i
                                                                            class="fa fa-save"></i> Save
                                                                    Brand
                                                                </button>
                                                            </form>

                                                        </div>
                                                    </div>
                                                </div>
                                                {{--modal ends--}}

                                                <a href="{{route('delete-brand').'/'.$value->id}}"
                                                   onclick="return confirm('Are you sure?')"
                                                   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>

                                        </tbody>
                                    @endforeach


                                </table>

                                <div class="clearfix"></div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

