@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">

                <div class="title_left">
                    <a href="{{route('footer-logo-table')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        Footer Logo Actions</a>
                </div>

            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">

                    <div class="x_panel">

                        <div class="x_content">
                            @include('backend.layouts.messages')
                            <div class="row">

                                <div class="clearfix"></div>

                                @foreach($footerData as $footer)
                                    <div class="col-md-4 col-sm-4 col-xs-12 profile_details center-margin">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <h4 class="brief" style="font-family: Helvetica"><b>Footer Logo</b>
                                                </h4>
                                                <div class="right col-xs-5 text-center">
                                                    <img style="margin-left: 80px"
                                                         src="{{url('uploads/images/home/footer-logo/'.$footer->image)}}"
                                                         height="200"
                                                         width="200" alt=""
                                                         class="">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">

                                                <div class="col-xs-12 col-sm-6 emphasis center-margin">
                                                    <button type="button" class="btn btn-primary btn-sm"
                                                            data-toggle="modal"
                                                            data-target="#{{$footer->id}}">
                                                        <i class="fa fa-exchange"> </i> Change Logo
                                                    </button>
                                                    {{--modal begins--}}
                                                    <div class="modal fade" id="{{$footer->id}}" tabindex="-1"
                                                         role="dialog"
                                                         aria-labelledby="basicModal" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close"
                                                                            data-dismiss="modal"
                                                                            aria-hidden="true">&times;
                                                                    </button>
                                                                    <h4 class="modal-title" id="myModalLabel"><b>Change
                                                                            Logo Here</b></h4>
                                                                </div>
                                                                <form action="{{route('edit-footer-logo-action')}}"
                                                                      method="post"
                                                                      enctype="multipart/form-data">
                                                                    {{csrf_field()}}

                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$footer->id}}">
                                                                    <div class="col-md-6">
                                                                        <input style="margin-left: 160px;margin-top: 5px"
                                                                               type="file" id="image" name="image"
                                                                               class="form-control" required>
                                                                    </div>

                                                                    <div class="col-md-12" style="margin-top: 10px">
                                                                        <button class="btn btn-primary btn-sm"><i
                                                                                    class="fa fa-save"></i> Save Logo
                                                                        </button>
                                                                    </div>

                                                                </form>


                                                            </div>
                                                        </div>
                                                    </div>
                                                    {{--modal ends--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

