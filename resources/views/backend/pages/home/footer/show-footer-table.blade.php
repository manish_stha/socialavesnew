@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">

                    <a href="{{route('footer-logo')}}" class="btn btn-success pull-right"><i
                                class="fa fa-backward"></i>
                        Back</a>

                    <a data-toggle="modal"
                       data-target="#AddFooterLogo" class="btn btn-success"><i
                                class="fa fa-plus"></i>
                        Add New Logo</a>
                        
                    {{--modal begins--}}
                    <div class="modal fade" id="AddFooterLogo" tabindex="-1" role="dialog"
                         aria-labelledby="basicModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel"><b>Add New Logo Here</b></h4>
                                </div>
                                <form action="{{route('add-footer-logo')}}" method="post"
                                      enctype="multipart/form-data">
                                    {{csrf_field()}}

                                    <div class="col-sm-6">
                                        <input style="margin-left: 160px" type="file" id="image" name="image" class="form-control" required>
                                    </div>

                                    <div class="col-sm-12" style="margin-top: 10px;margin-left: 250px">
                                        <button class="btn btn-primary btn-sm"><i
                                                    class="fa fa-save"></i> Save Logo
                                        </button>
                                    </div>

                                </form>


                            </div>
                        </div>
                    </div>
                    {{--modal ends--}}
                    
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Footer Logo</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Logo</th>
                                        <th>Status</th>
                                        <th>Created At</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    @foreach($footerData as $key=>$footer)
                                        <tbody>
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>
                                                <img src="{{url('uploads/images/home/footer-logo/'.$footer->image)}}"
                                                     width="30"
                                                     alt="">
                                            </td>
                                            <td>
                                                <form action="{{route('update-footer-logo-status')}}" method="post">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="criteria" value="{{$footer->id}}">
                                                    @if($footer->status==1)
                                                        <button name="active" class="btn btn-success btn-xs">
                                                            <i class="fa fa-check"></i>
                                                        </button>
                                                    @else
                                                        <button name="inactive" class="btn btn-danger btn-xs">
                                                            <i class="fa fa-times"></i>
                                                        </button>
                                                    @endif
                                                </form>
                                            </td>
                                            <td>
                                                {{$footer->created_at->diffForHumans()}}
                                            </td>
                                            <td>

                                                <a href="{{route('delete-footer-logo').'/'.$footer->id}}"
                                                   onclick="return confirm('Are you sure?')"
                                                   class="btn btn-danger btn-xs">Delete</a>
                                            </td>
                                        </tr>

                                        </tbody>
                                    @endforeach


                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection