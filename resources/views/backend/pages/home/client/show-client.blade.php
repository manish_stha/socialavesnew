@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('client-table')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        Check List</a>
                    <a href="" data-toggle="modal"
                       data-target="#AddClient" class="btn btn-success" style="margin-left: 15px"><i
                                class="fa fa-plus"></i>
                        Add New Client Testimony</a>

                    {{--modal begins--}}
                    <div class="modal fade" id="AddClient" tabindex="-1" role="dialog"
                         aria-labelledby="basicModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel"><b>Add Here</b></h4>
                                </div>
                                <form action="{{route('add-home-client')}}" method="post"
                                      enctype="multipart/form-data">
                                    {{csrf_field()}}
                                    <div class="row">
                                        <div class="col-sm-6" style="margin-left: 10px">
                                            <label for="image">Image</label>
                                            <input type="file" id="image" name="image" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-11" style="margin-top: 5px;margin-left: 10px">
                                            <div class="form-group form-group-lg">
                                                <label for="name">Name</label>
                                                <input name="name" required="required" id="name"
                                                       class="form-control" value="{{old('name')}}">
                                            </div>
                                            <div class="form-group form-group-lg">
                                                <label for="designation">Designation</label>
                                                <input name="designation" required="required" id="designation"
                                                       class="form-control" value="{{old('designation')}}">
                                            </div>
                                            <div class="form-group form-group-lg">
                                                <label for="comment">Testimony</label>
                                                <textarea name="comment" required="required" id="description_id"
                                                          class="form-control">{{old('comment')}}</textarea>
                                                <a href="" style="color: red">{{$errors->first('comment')}}</a>
                                            </div>

                                            <button class="btn btn-primary"><i
                                                        class="fa fa-save"></i> Save
                                            </button>
                                        </div>
                                    </div>


                                </form>


                            </div>
                        </div>
                    </div>
                    {{--modal ends--}}

                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Clients</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @include('backend.layouts.messages')

                        <div class="x_content">

                            <div class="row">
                                @foreach($clientData as $client)
                                    <div class="col-md-6 col-sm-4 col-xs-12 profile_details">
                                        <div class="well profile_view">
                                            <div class="col-sm-12">
                                                <div class="left col-xs-7">
                                                    <h2><b>{{$client->name}}</b></h2>
                                                    <h2 style="font-size: medium">{{$client->designation}}</h2>
                                                    <br>
                                                    <p style="text-align-all: justify"><?=str_limit(strip_tags($client->comment), 100)?></p>

                                                    <hr>

                                                    <ul class="list-unstyled">
                                                        <li><i class="fa fa-pencil"></i> <b>Updated
                                                                At: {{$client->updated_at->diffForHumans()}}</b></li>
                                                    </ul>
                                                </div>
                                                <div class="right col-xs-5 text-center">
                                                    <img src="{{url('uploads/images/home/client/'.$client->image)}}"
                                                         height="150" width="150" alt="" class="img-circle">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 bottom text-center">
                                                <div class="col-xs-12 col-sm-6 emphasis">

                                                    <button type="button" data-toggle="modal"
                                                            data-target="#{{$client->id}}"
                                                            class="btn btn-primary btn-xs pull-left">
                                                        <i class="fa fa-edit"> </i> Edit
                                                    </button>
                                                    <form style="margin-left: 400px"
                                                          action="{{route('update-home-client-status')}}"
                                                          method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="criteria" value="{{$client->id}}">
                                                        @if($client->status==1)
                                                            <button name="active" class="btn btn-danger btn-xs">
                                                                 Hide
                                                            </button>
                                                        @else
                                                            <button name="inactive" class="btn btn-success btn-xs">
                                                                 Publish
                                                            </button>
                                                        @endif
                                                    </form>
                                                </div>

                                                {{--modal begins--}}
                                                <div class="modal fade" id="{{$client->id}}" tabindex="-1"
                                                     role="dialog"
                                                     aria-labelledby="basicModal" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal"
                                                                        aria-hidden="true">&times;
                                                                </button>
                                                                <h4 class="modal-title" id="myModalLabel"><b>Make
                                                                        Required Changes</b></h4>
                                                            </div>
                                                            <form action="{{route('edit-home-client-action')}}"
                                                                  method="post"
                                                                  enctype="multipart/form-data">
                                                                {{csrf_field()}}
                                                                <div class="row" style="margin-left: 10px">
                                                                    <input type="hidden" name="criteria"
                                                                           value="{{$client->id}}">

                                                                    <div class="col-sm-6">
                                                                        <label for="image"
                                                                               class="pull-left">Image</label>
                                                                        <input type="file" id="image" name="image"
                                                                               class="form-control">
                                                                    </div>

                                                                    <div class="col-sm-4">
                                                                        <img style="margin-left: 10px"
                                                                             src="{{url('uploads/images/home/client/' .$client->image)}}"
                                                                             alt=""
                                                                             class="img-responsive thumbnail">
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="margin-left: 10px">
                                                                    <div class="col-sm-8">
                                                                        <div class="form-group form-group-lg">
                                                                            <label for="name"
                                                                                   class="pull-left">Name</label>
                                                                            <input name="name" id="name"
                                                                                   class="form-control"
                                                                                   value="{{$client->name}}">

                                                                        </div>
                                                                        <div class="form-group form-group-lg">
                                                                            <label for="body" class="pull-left">Designation</label>
                                                                            <input type="text" class="form-control"
                                                                                   name="designation"
                                                                                   value="{{$client->designation}}">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-11">
                                                                        <div class="form-group form-group-lg">
                                                                            <label for="body"
                                                                                   class="pull-left">Testimony</label>
                                                                            <textarea rows="5" type="text"
                                                                                      class="form-control"
                                                                                      name="comment"
                                                                                      id="description_id2"><?=strip_tags($client->comment)?></textarea>
                                                                        </div>
                                                                        <button class="btn btn-primary pull-left">
                                                                            <i
                                                                                    class="fa fa-save"></i> Save Changes
                                                                        </button>
                                                                    </div>


                                                                </div>


                                                            </form>


                                                        </div>
                                                    </div>
                                                </div>
                                                {{--modal ends--}}


                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection
