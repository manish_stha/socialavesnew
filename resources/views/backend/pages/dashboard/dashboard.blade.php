@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">

                <div class="x_panel">
                {{--<div class="x_title">--}}
                {{--<h2>Social Aves--}}
                {{--<small>Dashboard</small>--}}
                {{--</h2>--}}
                {{--<ul class="nav navbar-right panel_toolbox">--}}
                {{--<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>--}}
                {{--</li>--}}
                {{--<li class="dropdown">--}}
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"--}}
                {{--aria-expanded="false"><i class="fa fa-wrench"></i></a>--}}
                {{--<ul class="dropdown-menu" role="menu">--}}
                {{--<li><a href="#">Settings 1</a>--}}
                {{--</li>--}}
                {{--<li><a href="#">Settings 2</a>--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--</li>--}}
                {{--<li><a class="close-link"><i class="fa fa-close"></i></a>--}}
                {{--</li>--}}
                {{--</ul>--}}
                {{--<div class="clearfix"></div>--}}
                {{--</div>--}}
                <!-- Contents Starts -->


                    {{--<div class="card-body">--}}
                    {{--<div class="row top_tiles">--}}
                    {{--<div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">--}}
                    {{--<li class="dropdown" id="MarkAsRead">--}}
                    {{--<a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown"--}}
                    {{--role="button"--}}
                    {{--style="font-size: large;">--}}
                    {{--<i class="fa fa-briefcase"></i> Applications Received--}}
                    {{--<span class="badge">{{$CountApplication}}</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}

                    {{--@foreach($applicationData as $application)--}}
                    {{--<li>--}}
                    {{--<a href="{{route('applications')}}"--}}
                    {{--style="font-size: medium">--}}
                    {{--<b>{{$application->name}}</b> has send an--}}
                    {{--application {{$application->created_at->diffForHumans()}}--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--@endforeach--}}

                    {{--@if($CountApplication == 0)--}}
                    {{--<li>--}}
                    {{--<a href="#" style="font-size: medium">--}}
                    {{--No applications received yet.--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--@endif--}}


                    {{--</ul>--}}


                    {{--</li>--}}
                    {{--</div>--}}
                    {{--<div class="animated flipInY col-lg-4 col-md-3 col-sm-6 col-xs-12">--}}
                    {{--<li class="dropdown" id="MarkAsRead">--}}
                    {{--<a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown"--}}
                    {{--role="button"--}}
                    {{--style="font-size: large;">--}}
                    {{--<i class="fa fa-commenting"></i> Query Received--}}
                    {{--<span class="badge">{{$CountContact}}</span>--}}
                    {{--</a>--}}
                    {{--<ul class="dropdown-menu">--}}

                    {{--@foreach($contactData as $contact)--}}
                    {{--<li>--}}
                    {{--<a href="{{route('contacts')}}"--}}
                    {{--style="font-size: medium">--}}
                    {{--<b>{{$contact->name}}</b> has send a--}}
                    {{--query {{$contact->created_at->diffForHumans()}}--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--@endforeach--}}

                    {{--@if($CountContact == 0)--}}
                    {{--<li>--}}
                    {{--<a href="#"--}}
                    {{--style="font-size: medium">--}}
                    {{--No contacts received yet.--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--@endif--}}

                    {{--</ul>--}}


                    {{--</li>--}}
                    {{--</div>--}}


                    <div style="margin-top: 10px">
                        <div class="animated flipInY col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-envelope"></i></div>
                                <div class="count">{{$CountApplication}}</div>
                                <a href="{{route('applications')}}"
                                   style="margin-top: 10px;color: #3d4852;font-size: 24px;margin-top: 10px">Applications </a>
                                <p style="font-size: 14px;margin-top: 0px;margin-left: 62%">From <a href="{{route('applications')}}">Career</a></p>
                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-comments-o"></i></div>
                                <div class="count">{{$CountContact}}</div>
                                <a href="{{route('contacts')}}" style="margin-top: 10px;color: #3d4852;font-size: 24px">Queries</a>
                                <p style="font-size: 14px;margin-top: 0px;margin-left: 57%">From <a href="{{route('contacts')}}">Contact</a></p>

                            </div>
                        </div>
                        <div class="animated flipInY col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="tile-stats">
                                <div class="icon"><i class="fa fa-comment"></i></div>
                                <div class="count">{{$CountComment}}</div>
                                <a href="{{route('comments')}}" style="margin-top: 10px;color: #3d4852;font-size: 24px">Comments</a>
                                <p style="font-size: 14px;margin-top: 0px;margin-left: 65%">From <a href="{{route('blogs')}}">Blogs</a></p>

                            </div>
                        </div>

                    </div>


                </div>

            </div>
        </div>


        <div class="row" style="margin-top: 10px">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="row top_tiles">
                        <div class="col-md-3 tile">
                            <a href="{{route('users')}}" style="margin-top: 10px;color: #3d4852; font-size: 16px;">Total
                                Users</a>
                            <h1 style="margin-left: 30px"><b>{{$CountUser}}</b></h1>

                        </div>
                        <div class="col-md-3 tile">
                            <a href="{{route('portfolios')}}" style="margin-top: 10px;color: #3d4852; font-size: 16px;">Total
                                Portfolios</a>
                            <h1 style="margin-left: 30px"><b>{{$CountPortfolio}}</b></h1>

                        </div>
                        <div class="col-md-3 tile">
                            <a href="{{route('services')}}" style="margin-top: 10px;color: #3d4852; font-size: 16px;">Total
                                Services</a>
                            <h1 style="margin-left: 40px"><b>{{$CountService}}</b></h1>

                        </div>
                        <div class="col-md-3 tile">
                            <a href="{{route('blogs')}}" style="margin-top: 10px;color: #3d4852; font-size: 16px;">Total
                                Blogs</a>
                            <h1 style="margin-left: 30px"><b>{{$CountBlog}}</b></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>



    <!-- /page content -->

@endsection


