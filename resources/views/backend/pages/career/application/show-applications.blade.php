@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Application Received</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Post</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>CV</th>
                                        <th>Received Date</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($applicationData as $key=>$application)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <!--<td>{{$application->post_id}}</td>-->
                                            @foreach($PostData as $post)
                                                @if($post->id == $application->post_id)
                                                    <td style="font-family: Montserrat">
                                                        <strong>{{$post->title}}</strong></td>
                                                @endif
                                            @endforeach
                                            @if($application->post_id == null)
                                                <td style="font-family: Montserrat"><strong>Don't See Your Role
                                                        ?</strong></td>
                                            @endif
                                            <td>{{$application->name}}</td>
                                            <td>{{$application->email}}</td>
                                            <td>{{$application->address}}</td>
                                            <td>{{$application->contact}}</td>
                                            <td>
                                                <a href="{{url('uploads/application/'.$application->cv)}}"
                                                   class="btn btn-success btn-xs" download="{{$application->name}}"><i
                                                            class="glyphicon glyphicon-download"
                                                            title="Download CV"></i> Download</a>
                                            </td>
                                            <td>{{$application->created_at->format('m/d/Y H:i')}}</td>
                                            <td>
                                                <a href="{{route('delete-application').'/'.$application->id}}"
                                                   onclick="return confirm('Are you sure?')"
                                                   class="btn btn-danger btn-xs"><i
                                                            class="fa fa-trash" title="Delete Application"></i></a>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>


                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection