@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('posts')}}" class="btn btn-success"><i
                                class="fa fa-list"></i> All Posts</a>
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2 class="">Update Post Details</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                <div class="col-md-6">
                                    <form action="{{route('edit-post-action')}}" method="post"
                                          enctype="multipart/form-data">
                                        {{csrf_field()}}

                                        @include('backend.layouts.required')

                                        <input type="hidden" name="criteria" value="{{$postData->id}}">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="title">Select Post Title*</label>
                                                    <input type="text" id="name" name="title"
                                                           placeholder="Enter Title of the Post" class="form-control"
                                                           required="required" value="{{$postData->title}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label for="number">Required Number of Vacancies*</label>
                                                    <select name="number" class="form-control">
                                                        <option value="{{$postData->number}}"> {{$postData->number}}</option>
                                                        @for($x = 1; $x <= 20; $x++)
                                                            <option value="{{$x}}"> {{$x}}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group form-group-lg">
                                                    <label>Job Type</label>
                                                    <input type="text" name="job_type"
                                                           placeholder="Enter Type of the Job (eg. Full Time)"
                                                           value="{{$postData->job_type}}" class="form-control"
                                                           required="required">

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-lg">
                                            <label for="description">Description</label>
                                            <textarea name="description" id="description_id"
                                                      class="form-control">{!! $postData->description !!}</textarea>
                                        </div>
                                        <div class="form-group form-group-lg">
                                            <label for="about">About This Position*</label>
                                            <textarea name="about" required="required" id="description_id1"
                                                      class="form-control">{!! $postData->about !!}</textarea>
                                            <a href="" style="color: red">{{ $errors->first('about') }}</a>
                                        </div>
                                        <div class="form-group form-group-lg">
                                            <label for="works">What You Will be Doing*</label>
                                            <textarea name="works" id="description_id2"
                                                      class="form-control">{!! $postData->works !!}</textarea>
                                            <a href="" style="color: red">{{$errors->first('works')}}</a>

                                        </div>
                                        <div class="form-group form-group-lg">
                                            <label for="needs">What You Need for This Position*</label>
                                            <textarea name="needs" id="description_id3"
                                                      class="form-control">{!! $postData->needs !!}</textarea>
                                            <a href="" style="color: red">{{$errors->first('needs')}}</a>

                                        </div>
                                        <div class="form-group form-group-lg">
                                            <label for="bonus_skills">Bonus SKills*</label>
                                            <textarea name="bonus_skills" id="description_id4"
                                                      class="form-control">{!! $postData->bonus_skills !!}</textarea>
                                            <a href="" style="color: red">{{$errors->first('bonus_skills')}}</a>

                                        </div>


                                        <div class="form-group form-group-lg">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Save
                                                Changes
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection