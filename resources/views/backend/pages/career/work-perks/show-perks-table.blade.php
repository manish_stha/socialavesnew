@extends('backend.master.master')

@section('content')

    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('work-perks')}}" class="btn btn-success"><i
                                class="fa fa-backward"></i> Back</a>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>List of Work Perks</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <div class="row">
                                @include('backend.layouts.messages')
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Title</th>
                                        <th>Image</th>
                                        <th>Created Date</th>
                                        <th>Action</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($perkData as $key=>$perk)
                                        <tr>
                                            <td>{{++$key}}</td>
                                            <td>{{strip_tags($perk->title)}}</td>
                                            <td>
                                                <img src="{{url('uploads/images/career/work-perks/'.$perk->image)}}"
                                                     alt=""
                                                     width="30">
                                            </td>
                                            <td>{{$perk->created_at->format('Y-m-d H:i')}}</td>
                                            <td>
                                                <a href="{{route('delete-work-perk').'/'.$perk->id}}"
                                                   onclick="return confirm('Are you sure?')"
                                                   class="btn btn-danger btn-xs"><i
                                                            class="fa fa-trash" title="Delete"></i></a>
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>


                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->





@endsection