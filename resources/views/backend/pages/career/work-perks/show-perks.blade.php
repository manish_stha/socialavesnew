@extends('backend.master.master')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <a href="{{route('work-perk-table')}}" class="btn btn-success"><i
                                class="fa fa-list"></i>
                        Check List</a>
                    <a data-toggle="modal"
                       data-target="#AddPerk" class="btn btn-success" style="margin-left: 15px"><i
                                class="fa fa-plus"></i>
                        Add New Work Perk</a>
                    {{--modal begins--}}
                    <div class="modal fade" id="AddPerk" tabindex="-1" role="dialog"
                         aria-labelledby="basicModal" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel"><b>Add New Work Perk</b></h4>
                                </div>
                                <form action="{{route('add-work-perk')}}" method="post"
                                      enctype="multipart/form-data">
                                    {{csrf_field()}}

                                    <div class="col-sm-6">
                                        <div class="form-group form-group-sm">
                                            <label for="image">Image</label>
                                            <input type="file" id="image" name="image" class="form-control" required>
                                        </div>
                                    </div>

                                    <div class="col-sm-12" style="margin-top: 5px">
                                        <div class="form-group form-group-sm">
                                            <label for="title">Title</label>
                                            <textarea name="title"
                                                      required="required"
                                                      id="description_id"
                                                      class="form-control">{{old('title')}}</textarea>
                                            <a href=""
                                               style="color: red">{{$errors->first('title')}}</a>

                                        </div>

                                        <button class="btn btn-primary"><i
                                                    class="fa fa-save"></i> Save Perk
                                        </button>
                                    </div>

                                </form>


                            </div>
                        </div>
                    </div>
                    {{--modal ends--}}
                    <div class="x_panel" style="margin-top: 5px">
                        <div class="x_title">
                            <h2>Social Aves Work Perks</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a>
                                        </li>
                                        <li><a href="#">Settings 2</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        @include('backend.layouts.messages')

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                                            </div>

                                            <div class="clearfix"></div>

                                            @foreach($perkData as $perk)
                                                <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                    <div class="well profile_view">
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="right col-md-5 col-sm-5 col-xs-5 center-margin">
                                                                <img src="{{url('uploads/images/career/work-perks/'.$perk->image)}}"
                                                                     class="img-responsive" alt="">
                                                            </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <h4 style="font-size: 16px;color: #23272b;margin-top: 15px;text-align: center"><?=strip_tags($perk->title)?></h4>
                                                                <hr>
                                                                <ul class="list-unstyled">
                                                                    <li><i class="fa fa-pencil"></i> Created
                                                                        At: {{$perk->updated_at->format('m/d/Y H:i')}}
                                                                    </li>
                                                                </ul>
                                                            </div>

                                                        </div>
                                                        <div class="col-xs-12 bottom text-center">
                                                            <div class="col-xs-12 col-sm-6 emphasis">

                                                                <button type="button" data-toggle="modal"
                                                                        data-target="#{{$perk->id}}"
                                                                        class="btn btn-primary pull-left">
                                                                    <i class="fa fa-edit"> </i> Edit
                                                                </button>
                                                            </div>

                                                            {{--modal begins--}}
                                                            <div class="modal fade" id="{{$perk->id}}" tabindex="-1"
                                                                 role="dialog"
                                                                 aria-labelledby="basicModal" aria-hidden="true">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <button type="button" class="close"
                                                                                    data-dismiss="modal"
                                                                                    aria-hidden="true">&times;
                                                                            </button>
                                                                            <h4 class="modal-title" id="myModalLabel">
                                                                                <b>Make
                                                                                    Required Changes</b></h4>
                                                                        </div>
                                                                        <form action="{{route('edit-work-perk-action')}}"
                                                                              method="post"
                                                                              enctype="multipart/form-data">
                                                                            {{csrf_field()}}
                                                                            <div class="row" style="margin-left: 10px">
                                                                                <input type="hidden" name="criteria"
                                                                                       value="{{$perk->id}}">

                                                                            </div>
                                                                            <div class="row" style="margin-left: 10px">
                                                                                <div class="col-md-8">
                                                                                    <div class="form-group form-group-sm">
                                                                                        <label for="body"
                                                                                               class="pull-left">Title</label>
                                                                                        <input type="text"
                                                                                               class="form-control"
                                                                                               name="title"
                                                                                               value="{{strip_tags($perk->title)}}">
                                                                                    </div>
                                                                                    <div class="form-group form-group-sm">
                                                                                        <label for="body"
                                                                                               class="pull-left">Image</label>
                                                                                        <input type="file"
                                                                                               class="form-control"
                                                                                               name="image">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    <img src="{{url('uploads/images/career/work-perks/'.$perk->image)}}"
                                                                                         alt="" class="img-responsive"
                                                                                         style="margin-top: 23px">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-sm-11"
                                                                                 style="margin-left: 10px">
                                                                                <button class="btn btn-primary pull-left">
                                                                                    <i class="fa fa-save"></i> Save
                                                                                </button>
                                                                            </div>


                                                                        </form>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {{--modal ends--}}


                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>

                </div>

            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection

