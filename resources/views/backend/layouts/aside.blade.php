@section('aside')

    <div class="col-md-3 left_col" style="background: #2f2f2f">
        <div class="left_col scroll-view" style="background: #2f2f2f">
            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
                <div class="profile_pic center-margin" style="margin-top: 5px">
                    <img src="{{url('backend/images/logo/sa.png')}}" height="70px" width="70px"
                         class="img-circle">
                </div>
                <div style="margin-top: 10px;text-align: center">
                    <h2 style="font-size: 18px;color: white">
                        Social Aves</h2>
                    <a href="{{route('index')}}" target="_blank"><h2
                                style="font-size: 10px;color: white">
                            View Site</h2></a>
                </div>
            </div>
            <!-- /menu profile quick info -->

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" style="margin-top: 10px">
                <div class="menu_section">
                    <ul class="nav side-menu">
                        <li><a href="{{route('dashboard')}}"><i class="fa fa-dashboard "></i> Dashboard</a></li>

                        @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->privilege=='Super Admin')

                            <li><a href="{{route('users')}}"><i class="fa fa-users"></i> Users</a></li>

                        @endif

                        <h3 style="margin-top: 20px;font-family: 'Montserrat';font-size: small">Content
                            Management</h3>


                        <li style="margin-top: 10px"><a><i class="fa fa-home"></i> Home <span
                                        class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{route('banner')}}"> Banner</a></li>
                                <li><a href="{{route('facts')}}"> Intresting Facts</a></li>
                                <li><a href="{{route('home-client')}}"> Client</a></li>
                                <li><a href="{{route('client-brands')}}"> Brands</a></li>
                                <li><a href="{{route('footer-logo')}}"> Footer</a></li>
                            </ul>
                        </li>


                        <li>
                            <a href="{{route('services')}}"><i class="fa fa-check"></i> Service Management</a>
                        </li>
                        <li>
                            <a href="{{route('portfolios')}}"><i class="fa fa-lightbulb-o"></i> Portfolios</a>

                        </li>

                        <li><a><i class="fa fa-pencil"></i> Blogs & Quotes <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                                <li><a href="{{route('blogs')}}"> Blogs</a></li>
                                <li><a href="{{route('quotes')}}"> Quotes</a></li>
                            </ul>
                        </li>

                        <li><a href="{{route('testimonies')}}"><i class="fa fa-file-text-o"></i> Team
                                Testimonials</a>
                        </li>
                        <li><a href="{{route('employee-story')}}"><i class="fa fa-quote-left"></i> Employee Stories</a>
                        </li>
                        <li>
                            <a href="{{route('client-testimonies')}}"><i class="fa fa-video-camera"></i> Testimonial
                                Videos</a>
                        </li>

                        @if(\Illuminate\Support\Facades\Auth::guard('admin')->user()->privilege=='Super Admin' OR \Illuminate\Support\Facades\Auth::guard('admin')->user()->privilege=='Admin')

                            <li>
                                <a><i class="fa fa-briefcase"></i> Career<span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('work-perks')}}"> Work Perks</a></li>
                                    <li><a href="{{route('posts')}}"> Vacancy Post</a></li>
                                </ul>
                            </li>

                            <h3 style="margin-top: 20px;font-family: 'Montserrat';font-size: small">Live
                                On</h3>
                            <li style="margin-top: 10px"><a href="{{route('applications')}}"><i
                                            class="fa fa-envelope"></i> Application
                                    Received</a></li>
                            <li><a href="{{route('contacts')}}"><i class="fa fa-phone"></i> Queries Received</a>
                            </li>
                            <li>
                                <a href="{{route('comments')}}"><i class="fa fa-comment"></i> Comments on Blog</a>
                            </li>

                        @endif


                    </ul>
                </div>

            </div>
            <!-- /sidebar menu -->

        </div>
    </div>

@endsection