@include('frontend.layouts.header')
@include('frontend.layouts.footer')

@yield('header')

@yield('top-nav')

@yield('top-nav-2')

@yield('content')

@yield('footer')