@section('header')

        <!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="noindex,nofollow"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="{{url('frontend/font/fontawesome/css/all.min.css')}}">
    <!-- Animation on Scrolling -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css"/>
    <!-- Owl Carousel CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <!-- For Video and Image Gallery CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css"/>

    <!-- Main Stylesheet File -->
    <link href="{{url('frontend/css/main.css')}}" rel="stylesheet" type="text/css">

    <link href="{{url('frontend/css/jquery.fancybox.min.css')}}" rel="stylesheet" type="text/css">

    <!-- Pre-Loading js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>

    <!-- Favicons -->
    <link href="{{url('frontend/images/favicon.ico')}}" rel="icon">
    {{--Google Recaptcha--}}
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <title>{{$title}}</title>

</head>

<body id="top" class="main-body">


<!-- Preloader -->
<div id="preloader">
    <div id="status">Make your brand talk</div>
</div>
<!-- End Preloader -->


@endsection