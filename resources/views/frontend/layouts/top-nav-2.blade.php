@section('top-nav-2')

    <!-- Header Navigation -->
    <section class="header-nav p-0">
        @foreach($logoData as $logo)
            <div class="header-logo">
                <a href="{{route('index')}}" data-toggle="popover" data-trigger="hover"
                   data-content="Make your brand talk">
                    <img class="logo" src="{{url('uploads/images/logo/'.$logo->logo)}}" width="60%" height="auto"/>
                </a>
            </div>
        @endforeach

        <div class="hire">
            <a class="btn btn-hire mt-0" href="{{url('career')}}">HIRE ME</a>
        </div>
        <div class="menu-text">MENU</div>

        <div class="open-menu">
            <img class="p-0" style="margin-top: 3px" src="{{url('frontend/images/menu.svg')}}" alt=""/>
        </div>
    </section>
    <!-- End Header Navigation -->

    <!-- ==================================
         MENU
         ==================================
    -->

    <!-- Menu Nav -->
    <section class="nav-overlay p-0 ">
        @foreach($logoData as $logo)
            <div class="header-logo">
                <a href="{{route('index')}}" data-toggle="popover" data-trigger="hover"
                   data-content="Make your brand talk">
                    <img class="logo d-inline-block align-top" src="{{url('uploads/images/logo/'.$logo->logo)}}"
                         width="60%" height="auto">
                </a>

            </div>

        @endforeach
        <div class="close-menu">
            <img src="{{url('frontend/images/close.svg')}}" alt=""/>
        </div>

        <!-- Overlay Menu -->
        <div class="overlay-menu">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h6 class="mb-4 menu-title">Navigation</h6>
                    </div>
                    <div class="col-lg-6">

                        <div class="menu mb-4">
                            <ul>
                                <!--<li class="dropdown">-->
                                <!--    <a href="{{route('service')}}" class="dropdown-toggle" type="button"-->
                                <!--       id="serviceMenu"-->
                                <!--       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Explore our-->
                                <!--        services</a>-->
                                <!--    <div class="dropdown-menu" aria-labelledby="serviceMenu">-->
                                <!--        @foreach($serviceData as $data)-->
                                <!--            <a class="dropdown-item"-->
                                <!--               href="{{route('view-service',[str_slug("$data->title","-"),$data->id,])}}">{{$data->title}}</a>-->
                                <!--        @endforeach-->
                                <!--    </div>-->
                                <!--</li>-->
                                <li><a href="{{route('service')}}" title="Our Services">Explore Our Services</a></li>
                                <li><a href="{{route('portfolio')}}" title="Our Works">Portfolio</a></li>
                                <li><a href="{{route('team')}}" title="Our Team">Meet our Team</a></li>
                                <li><a href="{{route('blog')}}" title="Our Blogs">Blogs</a></li>
                                <li><a href="{{route('career')}}" title="Make your career at Social Aves">Career</a>
                                </li>
                                <li><a href="{{route('contact')}}" title="Contact with us">Contact Us</a></li>
                            </ul>
                        </div>


                    </div>

                    <div class="col-lg-6 mt-2 mb-4">
                        <div class="contact-info">
                            <h6>CALL US TODAY</h6>
                            <h4 class="mb-0 color-red">+977-1-5537595</h4>
                            <p class="text-muted mb-4">Sunday to Friday 10:00 am – 6:00 pm</p>

                            <h6>VISIT THE OFFICE</h6>
                            <p class="text-muted mb-4">Jawalakhel Chowk, Lalitpur, Nepal<br>
                                GPO BOX No. 8354</p>

                            <h6>DROP A LINE</h6>
                            <p class=" text-muted mb-4">info@socialaves.com</p>
                        </div>

                        <!-- Menu Social -->
                        <div class="menu-social">
                            <h6>FIND US ON</h6>
                            <li><a href="https://www.facebook.com/socialaves/" target="_blank"><i
                                            class="fab fa-facebook-f"></i></a></li>
                            <li><a href="https://twitter.com/SocialAves" target="_blank"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li><a href="https://www.instagram.com/avessocial/" target="_blank"><i
                                            class="fab fa-instagram"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCJ2gYCjdce1idG5MJY4hbvw" target="_blank"><i
                                            class="fab fa-youtube"></i></a></li>
                            <li class="m-0"><a href="https://www.linkedin.com/company/social-aves-pvt-ltd-/"
                                               target="_blank"><i class="fab fa-linkedin"></i></a></li>
                        </div>
                        <!-- End Menu Social -->
                    </div>

                    <div class="col-lg-12">
                        <div class=" mb-4">©2020 Social Aves. All Rights Reserved. <br>Developed by <a
                                    href="https://socialaves.com/" target="_blank">Social
                                Aves</a></div>
                    </div>

                </div>

            </div>
        </div>
        <!-- End Overlay Menu -->


    </section>
    <!-- End Overlay Menu -->
@endsection