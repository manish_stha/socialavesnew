@if (\Illuminate\Support\Facades\Session::has('sweet_alert.alert'))
    <script>
                @if (\Illuminate\Support\Facades\Session::has('sweet_alert.content'))
        let config = {!! \Illuminate\Support\Facades\Session::pull('sweet_alert.alert') !!}
                let
        content = document.createElement('div');
        content.insertAdjacentHTML('afterbegin', config.content);
        config.content = content;
        swal(config);
        @else
        swal({!! \Illuminate\Support\Facades\Session::pull('sweet_alert.alert') !!});
        @endif
    </script>
@endif


{{--<div class="swal-overlay swal-overlay--show-modal" tabindex="-1">--}}
{{--<div class="swal-modal" role="dialog" aria-modal="true">--}}
{{--<div class="swal-icon swal-icon--success">--}}
{{--<span class="swal-icon--success__line swal-icon--success__line--long"></span>--}}
{{--<span class="swal-icon--success__line swal-icon--success__line--tip"></span>--}}

{{--<div class="swal-icon--success__ring"></div>--}}
{{--<div class="swal-icon--success__hide-corners"></div>--}}
{{--</div>--}}
{{--<div class="swal-title" style="">Message Sent</div>--}}
{{--<div class="swal-text" style="">{{session('success')}}</div>--}}
{{--<div class="swal-footer">--}}
{{--<div class="swal-button-container">--}}

{{--<button class="swal-button swal-button--confirm">OK</button>--}}

{{--<div class="swal-button__loader">--}}
{{--<div></div>--}}
{{--<div></div>--}}
{{--<div></div>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}


@if(session('error'))

    <div class="col-md-16">
        <div class="form-group">
            <div class="alert alert-danger">
                <i class="fa fa-times"></i> {{session('error')}}

            </div>
        </div>
    </div>
    <br>

@endif