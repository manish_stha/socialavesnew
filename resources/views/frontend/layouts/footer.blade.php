@section('footer')


    <!-- ==================================
     FOOTER
     ==================================
-->
    <footer class="footer bgdark">
        <div class="container text-center">
            
        <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v6.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="290749250989781">
      </div>
      
            <div class="row">
                @foreach($footerData as $footer)
                    <div class="col-lg-12">
                        <img class="col-center" src="{{url('uploads/images/home/footer-logo/'.$footer->image)}}">
                    </div>
                @endforeach
                <div class="col-lg-12 my-5">
                    <div class="footer-social">
                        <li><a href="https://www.facebook.com/socialaves/" target="_blank"><i
                                        class="fab fa-facebook-f"></i></a></li>
                        <li><a href="https://twitter.com/SocialAves" target="_blank"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li><a href="https://www.instagram.com/avessocial/" target="_blank"><i
                                        class="fab fa-instagram"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCJ2gYCjdce1idG5MJY4hbvw" target="_blank"><i
                                        class="fab fa-youtube"></i></a></li>
                        <li class="m-0"><a href="https://www.linkedin.com/company/social-aves-pvt-ltd-/"
                                           target="_blank"><i class="fab fa-linkedin"></i></a></li>
                    </div>
                </div>
                <div class="col-lg-12">
                    <p>©{{date('Y')}} Social Aves. All Rights Reserved. <br>Developed by <a href="{{route('index')}}">Social
                            Aves</a>
                    </p>
                </div>
                <div class="go-top" style="display: block;">
                  <a class="smoothscroll" href="#top" data-toggle="popover" data-trigger="hover" data-content="Back to Top" data-original-title="" title=""><i class="fas fa-arrow-up" aria-hidden="true"></i></a>
                </div>

            </div>
        </div>
    </footer>
    <!-- ==================================
       END FOOTER
       ==================================
    -->

    <!-- AOS JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js"></script>

    <script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2"></script>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <!-- Preloader JS -->
    <script src="https://cdn.jsdelivr.net/npm/jquery-bez@1.0.11/src/jquery.bez.js"></script>
    <!-- Owl Carousel JS -->
    <script src="{{url('frontend/jquery/owl.carousel.js')}}"></script>
    <!-- Fancy box JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.js"></script>

    <!-- Counter JS -->
    <script src="{{url('frontend/jquery/jquery.counterup.js')}}"></script>
    <script src="{{url('frontend/jquery/jquery.waypoints.min.js')}}"></script>

    <!-- Typed JS -->
    <script src="{{url('frontend/jquery/typed.min.js')}}"></script>

    <!-- custom script
    ================================================== -->
    <script src="{{url('frontend/jquery/action.js')}}" charset="utf-8"></script>
    <script src="{{url('frontend/custom/custom.js')}}" charset="utf-8"></script>


    </body>
    </html>
@endsection