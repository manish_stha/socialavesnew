
{{--footer contact--}}
<section class="bg-light-grey">
    <div class="container">
        <div class="row">
            <div class="col col-center text-center">
                <h1 class="mb-4">Let's build something amazing</h1>
                <a href="{{route('contact')}}" role="button" class="btn btn-fill w-30">Get in touch</a>
            </div>
        </div>
    </div>
</section>

<section class="footer-contact bg-white p-0">

    <div class="container-fluid bg-white p-0 text-center">


        <div class="row no-gutters">
            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <img class="location mb-4" src="{{url('frontend/images/location.svg')}}">
                        <h4 class="text-uppercase m-0">Visit our office</h4>
                        <hr class="my-4">
                        <div class="text-black-50">Jawalakhel Chowk, Lalitpur, Nepal <br>GPO BOX No. 8354</div>
                        <div class="small mt-4"><a
                                    href="https://www.google.com/maps/place/Social+Aves/@27.6719049,85.3085274,17z/data=!3m1!4b1!4m5!3m4!1s0x39eb1834531b4c81:0x518579b3717c7755!8m2!3d27.6719049!4d85.3107161"
                                    target="_blank"><b>VIEW
                                    THE MAP</b></a></div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <img class="email mb-4" src="{{url('frontend/images/email.svg')}}">
                        <h4 class="text-uppercase m-0">Drop a line</h4>
                        <hr class="my-4">
                        <div class="text-black-50">info@socialaves.com</div>
                        <div class="small mt-4"><a href="https://mail.google.com/mail/u/0/"
                                                   target="_blank"><b>SEND THE EMAIL</b></a></div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 mb-3 mb-md-0">
                <div class="card py-4 h-100">
                    <div class="card-body text-center">
                        <img class="call mb-4" src="{{url('frontend/images/call.svg')}}">
                        <h4 class="text-uppercase m-0">CALL US TODAY</h4>
                        <hr class="my-4">
                        <div class="big">+977-1-5537595</div>
                        <div class="small text-black-50">Sunday to Friday 10:00 am – 6:00 pm</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
