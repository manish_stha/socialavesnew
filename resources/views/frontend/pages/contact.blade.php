@extends('frontend.master.master')

@include('frontend.layouts.top-nav-2')

@section('content')
    <!-- ==================================
         Contact title
         ==================================
    -->
    <section class="contact-title pb-0 bg-white">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">contact Us</marquee>
        </div>
        <!-- End Marquee -->
        <div class="container p-0">
            <!-- Contact us title -->
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">contact us now</h2>

                @include('frontend.layouts.messages')

                <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">Feel free to contact us
                    using the information below. You can also submit your request via online form.</p>
                <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
            </div>
            <!-- end contact title -->
        </div>
    </section>
    <!-- ==================================
         End contact tiltle
         ==================================
    -->

    <!-- ==================================
         CONTACT INFORMATION
         ==================================
    -->

    <section class="contact bg-white pt-0">

        <div class="container bg-white text-center">

            <div class="row ">
                <div class="col-md-4 mb-3 mb-md-0" data-aos="fade-down" data-aos-easing="ease" data-aos-delay="400">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <img class="location mb-4" src="{{url('frontend/images/location.svg')}}">
                            <h4 class="text-uppercase m-0">Visit the office</h4>
                            <hr class="my-4">
                            <div class="text-black-50">Jawalakhel Chowk, Lalitpur, Nepal <br>GPO BOX No. 8354</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-3 mb-md-0" data-aos="fade-down" data-aos-easing="ease" data-aos-delay="500">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <img class="email mb-4" src="{{url('frontend/images/email.svg')}}">
                            <h4 class="text-uppercase m-0">Drop a line</h4>
                            <hr class="my-4">
                            <div class="text-black-50">info@socialaves.com</div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mb-3 mb-md-0" data-aos="fade-down" data-aos-easing="ease" data-aos-delay="600">
                    <div class="card py-4 h-100">
                        <div class="card-body text-center">
                            <img class="call mb-4" src="{{url('frontend/images/call.svg')}}">
                            <h4 class="text-uppercase m-0">CALL US TODAY</h4>
                            <hr class="my-4">
                            <div class="big">+977-1-5537595</div>
                            <div class="small text-black-50">Sunday to Friday 10:00 am – 6:00 pm</div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- ==================================
         END CONTACT INFORMATION
         ==================================
    -->

    <div class="pattern-down" data-negative="false">
        <svg viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
            <path class="grey-fill"
                  d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7 s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7 c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3  c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6 c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7  C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5 c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1  c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7  c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6  C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8 c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2  C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3  C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1 z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1  c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z">
            </path>
        </svg>
    </div>

    <section class="bg-light-grey">
        <div class="container contact-form">
            <div class="mb-5 text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200" class="aos-init aos-animate">Tell us about your project </h2>
                <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200" class="aos-init aos-animate">Let us help you get your business online and grow it with passion</p>
            </div>


            <form action="{{route('add-contact')}}" onsubmit="return ValidateContactForm();" id="myformm" method="post"
                  novalidate data-aos="fade-up" data-aos-easing="ease">
                {{csrf_field()}}

                <div class="row" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
                    <div class="col-md-6">
                        <div class="form-group">

                            <input type="text" id="name" name="name" class="form-control" value="{{old('name')}}"
                                   placeholder="Name *"/>
                            <a href="#" style="color: red;">{{$errors->first('name')}}</a>
                            <h7 id="error_message1"></h7>
                        </div>

                        <div class="form-group">
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email *"
                                   value="{{old('email')}}"/>
                            <a href="#" style="color: red;">{{$errors->first('email')}}</a>
                            <h7 id="error_message2"></h7>
                        </div>

                        <div class="form-group">
                            <input type="text" id="phone" name="phone" class="form-control"
                                   placeholder="Phone Number *"
                                   value="{{old('phone')}}"/>
                            <a href="#" style="color: red;">{{$errors->first('phone')}}</a>
                            <h7 id="error_message3"></h7>
                        </div>


                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
            <textarea id="message" name="comment" class="form-control" placeholder="Your Message *"
                      style="width: 100%; height: 238px;">{{old('comment')}}</textarea>
                            <a href="#" style="color: red;">{{$errors->first('comment')}}</a>
                            <h7 id="error_message4"></h7>
                        </div>

                    </div>
                    {{--<div class="col-md-12">--}}
                    {{--<div class="form-group">--}}
                    {{--<div class="g-recaptcha" data-sitekey="6LdMNM8UAAAAABzIsf5HChVfyyThg2yOhUVCv8Jj"></div>--}}
                    {{--</div>--}}
                    {{--</div>--}}
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="submit" id="contact-form" name="btnSubmit" class="btn btn-fill"
                                   value="Send Message"/>
                        </div>
                    </div>
                </div>
            </form>


        </div>
    </section>

    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.468555068436!2d85.30852741423472!3d27.67190963365705!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1834531b4c81%3A0x518579b3717c7755!2sSocial%20Aves!5e0!3m2!1sen!2snp!4v1575281433911!5m2!1sen!2snp"
            width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>


    {{--Testimonials Video--}}
    <section class="t-video p-0">

        <div class="container-fluid ">
            <div class="row d-flex justify-content-end align-items-center">
                @foreach($ClientTestimonyData as $data)

                    <div class="col-lg-12 col-md-12 no-padding">
                        <a id="play-video" class="video-play-button" href="{{$data->link}}"
                           data-fancybox data-animation-effect="zoom-in-out" data-animation-duration="600">
                            <span></span>
                        </a>

                        <div class=" video-text">
                            <div class="w-100 text-white">
                                <h4>{{$data->title}}</h4>
                                <p class="mb-0 text-white">CLIENT TESTIMONIALS</p>
                            </div>
                        </div>

                        <div class="overlay"></div>
                        <img class="img-fluid" src="{{url('uploads/images/client-testimony/'. $data->preview_image)}}"
                             alt="">
                    </div>

                @endforeach

            </div>
        </div>
    </section>
@endsection

<script src="{{url('frontend/jquery/sweetalert.min.js')}}"></script>

@include('sweet::alert')





