@extends('frontend.master.master')

@include('frontend.layouts.top-nav')

@section('content')
    <!-- ==================================
     BLOG
     ==================================
-->
    <section class="blog bg-white  ">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">Blog</marquee>
        </div>
        <!-- End Marquee -->
        <div class="container">
            <!-- Blog title -->
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">FROM THE BLOG</h2>
                <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">Our news and stories</p>
                <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
            </div>
            <!-- End Blog title -->

            <div class="card-columns">

                @foreach($blogData as $blog)

                    <div class="card">
                        <img class="card-img-top" src="{{url('uploads/images/blogs/' . $blog->upload1)}}"
                             alt="Card image">
                        <div class="card-body mb-3">
                            <p class="card-date text-muted">{{$blog->created_at->format('j F, Y')}}</p>
                            <h4 class="card-title mb-3">{!! $blog->title !!}</h4>
                            <p class="card-text">{!! str_limit(strip_tags($blog->body,100)) !!}</p>
                            <a href="{{route('view-blog',[str_slug("$blog->title","-"),$blog->id])}}"
                               class="stretched-link">See Details</a>
                        </div>
                    </div>
                    @foreach($blog->quote as $quote)
                      @if($quote->status == 1)
                        <div class="card bg-dark text-white text-center p-3">
                            <blockquote class="blockquote mb-0">
                                <h4>{!! $quote->quote !!}</h4>
                                <footer class="blockquote-footer text-white">
                                    <small>{{$quote->writer}}</small>
                                </footer>
                            </blockquote>
                        </div>
                     @endif
                    @endforeach
                @endforeach

            </div>

        </div>

    </section>
    <!-- ==================================
         END BLOG
         ==================================
    -->


    <!-- ==================================
          BLOG PAGINATION
         ==================================
    -->


    <section class="ss-pagination ss-pagination-center">

          <!-- START: ss-Pagination -->
        <div class="container">
            @if($blogData->onFirstPage())
                <a class="ss-pagination-center" href="{{route('blog')}}" title="Main Blog">
                    <i class="fad fa-2x fa-th"></i>
                </a>

                @if($blogData->hasMorePages())
                    <a class="ss-pagination-next" href="{{$blogData->nextPageUrl()	}}" title="Next"><i
                                class="far fa-2x fa-long-arrow-right"></i></a>
                @endif

            @else
                <a class="ss-pagination-prev" href="{{$blogData->PreviousPageUrl()}}" title="Previous">
                    <i class="far fa-2x fa-long-arrow-left"></i></a>
                <a class="ss-pagination-center" href="{{route('blog')}}" title="Main Blog">
                    <i class="fad fa-2x fa-th"></i>
                </a>

                @if($blogData->hasMorePages())
                    <a class="ss-pagination-next" href="{{$blogData->nextPageUrl()	}}" title="Next"><i
                                class="far fa-2x fa-long-arrow-right"></i></a>
                @endif
            @endif


        </div>
        <!-- END: ss-Pagination -->

    </section>


    <!-- ==================================
         END BLOG PAGINATION
         ==================================
    -->

    <section data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
        <div class="container">

            <div class="row justify-content-center text-center">
                <div class="col-md-8">
                    <div class="slide-one-item owl-carousel">
                        <blockquote class="testimonial-1">
                            <i class="fas fa-quote-left"></i>
                            <p>Social Aves is by far the best Digital Marketing Agency in Nepal. They have the right
                                team and right attitude about work. I have worked with them in the last 5 years and I
                                have no complain and we are still continuing with their services. A well lead company by
                                Ravi, I wish them all the best always.</p>
                            <span class="text-black">Mike Dorney</span> &mdash; <span class="small text-muted">CEO and Co-Founder</span>
                        </blockquote>

                        <blockquote class="testimonial-1">
                            <i class="fas fa-quote-left"></i>
                            <p>My company’s Google rankings and overall site traffic improved dramatically after just a
                                few months of working with this agency. The service we’ve received from their team has
                                consistently been above and beyond our expectations.</p>
                            <span class="text-black">James Smith</span> &mdash; <span class="small text-muted">CEO and Co-Founder</span>
                        </blockquote>

                        <blockquote class="testimonial-1">
                            <i class="fas fa-quote-left"></i>
                            <p> This hard-working team provides a consistent stream of fresh leads while equipping us
                                with what we need to turn those into loyal customers. This is a well team by Ravi and I
                                know they have much more potential then what they are doing now</p>
                            <span class="text-black">Mike Dorney</span> &mdash; <span class="small text-muted">CEO and Co-Founder</span>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <div class="pattern-down" data-negative="false">
        <svg viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
            <path class="grey-fill"
                  d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7 s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7 c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3  c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6 c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7  C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5 c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1  c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7  c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6  C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8 c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2  C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3  C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1 z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1  c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z">
            </path>
        </svg>
    </div>





    <!-- ==================================
         Contact form
         ==================================
    -->
    <section id="queries" class="bg-light-grey">
        <div class="container contact-form ">
            <div class="mb-5 text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">Tell us about your project </h2>
                <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">Let us help you get your business
                    online and grow it with passion</p>
            </div>


            <form method="post" class="needs-validation" novalidate data-aos="fade-up" data-aos-easing="ease"
                  data-aos-delay="300">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="txtName" class="form-control" placeholder="Name *" value=""
                                   required/>
                            <div class="invalid-feedback">Your name is invalid.</div>
                        </div>
                        <div class="form-group">
                            <input type="email" name="txtEmail" class="form-control" placeholder="Email *" value=""
                                   required/>
                            <div class="invalid-feedback">Your email address is invalid.</div>
                        </div>

                        <div class="form-group">
                            <input type="text" name="txtPhone" class="form-control" placeholder="Phone Number *"
                                   value="" required/>
                            <div class="invalid-feedback">Your phone number is invalid.</div>
                        </div>


                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" name="txtCompany" class="form-control" placeholder="Company Name"
                                   value="" required/>
                            <div class="invalid-feedback">Your company is invalid.</div>
                        </div>
                        <div class="form-group">
                            <textarea name="txtMsg" class="form-control" placeholder="Your Message *"
                                      style="width: 100%; height: 152px;" required></textarea>
                            <div class="invalid-feedback">Please enter your message</div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="submit" name="btnSubmit" class="btn btn-fill" value="Hear from an expert"/>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </section>


    @include('frontend.layouts.footer-contact')



@endsection