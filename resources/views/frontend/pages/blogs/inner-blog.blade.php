@extends('frontend.master.master')

@include('frontend.layouts.top-nav-2')


@section('content')
    {{--Blogs starts--}}
    <header class="masthead"
            style="background-image: url('{{ asset('uploads/images/blogs/' . $blogData->upload2) }}')">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto text-center">
                    <div class="post-heading">
                        <span class="meta">{{$blogData->created_at->format('j F, Y')}}</span>
                        <h1>{{ $blogData->title }}</h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    @include('frontend.layouts.messages')

    @if($blogData->id === 12)
        <!-- Post Content -->
        <article class="post-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-md-10 mx-auto">
                        <p>As a&nbsp;<a href="https://socialaves.com/">digital</a><a href="https://socialaves.com/">
                                marketing agency</a>, Social Aves boosts digital presence of brands to connect them with
                            individual audiences. We highly encourage brands to experiment on regular basis and apply
                            the generated engagement for executing impactful campaigns. All the campaigns are ideated
                            with well thought process. With an objective to accelerate the brand, the campaign creatives
                            are crafted with absolute love and thus, executed precisely.</p>

                        <p>Here are top five Digital Marketing Campaigns conducted by Social Aves :</p>

                        <img src="{{url('frontend/images/gifs/infoo.gif')}}" alt="">

                        <h2><strong>1. Dell Nepal: Dell Dashain Character</strong></h2>

                        <p>On celebrating the country’s greatest festivals, Dashain and Tihar&nbsp;and associate with
                            Nepali audience, Dell Nepal conducted #DellDashainCharacter campaign that was held from 1st&nbsp;October
                            to 15th&nbsp;October 2016. To foster the festive vibes, eight different Dashain&nbsp;characters
                            were developed that were inspired from Nepali folks resembling each unique trait. The
                            characters developed were power packed in a contest post that interrogated “Which Dell
                            Dashain Character are you?”</p>

                        <img src="{{url('frontend/images/gifs/dell1.gif')}}" alt="">


                        <p>Want to see how the campaign rocked? Here is the link:&nbsp;<a href="https://goo.gl/L6aXxz"
                                                                                          target="_blank">https://goo.gl/L6aXxz</a>
                        </p>

                        <h2><strong>2. WorldLink Communications: Scratch Garnuhos Bhagya Chamkaunhos</strong></h2>

                        <p>Nepal’s top ISP (Internet Service Provider), WorldLink Communications started a ATL (Above
                            The Line) campaign called “<em>Ye hajur Kata? Scratch Garau Yeta!</em>&nbsp;(<a
                                    href="https://business.facebook.com/hashtag/chitobhanekaiworldlink" target="_blank">#chitobhanekaiworldlink</a>&nbsp;<a
                                    href="https://business.facebook.com/hashtag/yehajurkata" target="_blank">#yehajurkata</a>&nbsp;<a
                                    href="https://business.facebook.com/hashtag/worldlink"
                                    target="_blank">#worldlink</a>). With its objective to facilitate the current
                            customers and increase new connections, this campaign turned out to be WorldLink’s biggest
                            ever physical campaign.</p>

                        <p>The participants received a scratch coupon when they renewed their subscription or subscribed
                            a new connection. The coupon included exciting sure shot prizes of up to 12 months of free
                            internet or NRs 1000/- cashback on subscription also including a chance to win 43” Samsung
                            UHD TV every week and a grand opportunity to take home Ford Aspire as the bumper prize in
                            lucky draw. This campaign kicked off creating a buzz in the market as it was a huge leap for
                            WorldLink and a threat for its competitors.</p>

                        <p>To align and match up with the grand physical campaign, Social Aves came up with the
                            campaign, “WorldLink&nbsp;<em>Scratch Garnuhos Bhagya Chamkaunuhos</em>”. It was WorldLink’s
                            first ever website integrated digital campaign. The basic concept was, “You speed test your
                            internet every time, but this time, why not you speed test your luck?” The participants
                            landed in the website’s app (https://www.worldlink.com.np/) through social media. Then, they
                            had to click the “Try your Luck” button and submit their participation details. The audience
                            had to pull the lever that would activate the&nbsp;<em>Bhagya</em>&nbsp;Meter. Excitement
                            was created as the pointer fluctuated up and down. Lastly, the participants had to scratch
                            the card to find out what was on their luck and become eligible to win the prizes. &nbsp;A
                            total of 144 prizes were up for grabs. Among them, some of were NRs.2,500 discount on
                            internet bills, 25 Mbps internet + NET TV for 1 month, movie tickets, shopping vouchers from
                            Kaymu, 25 Mbps internet for 12 months and so-on. Lucky winners were selected from each
                            respective category.<br>
                            The campaign’s popularity skyrocketed with 3000+ participation in just 3 hours of the
                            launch. WorldLink successfully achieved a total participation of 18,324 and 34,611 pageviews
                            on the app page during the period of Oct 9th– Oct 28th, 2017.</p>

                        <img src="{{url('frontend/images/gifs/worldlink1.gif')}}" alt="">


                        <h2><strong>3. PRAN’s Valentine’s Campaign</strong></h2>

                        <p>PRAN Frooto Nepal and PRAN Potato Cracker Nepal launched campaigns for Valentine’s. Since the
                            brand’s target audience is children and youth, the campaign had to come up with peppy
                            street-smart activities to engage the audience.</p>

                        <p>PRAN Potato Cracker Nepal’s&nbsp;<em>Prem Patra Pratiyogita</em>&nbsp;was a humorous take on
                            Valentine’s Day. People had to dedicate their love message to anyone or anything. The
                            contest was conducted from Feb 13- Feb 17 2017. The process was to express feelings to loved
                            ones by preparing a message towards loved one in not more than 200 characters and win cash
                            prize. The love messages sent by the participants were posted in the form of template on the
                            PRAN Potato Cracker Nepal’s Facebook page. The participants had to share the template
                            (publicly visible) on their profile with the hashtag #PRANPremPatraPratiyogita&nbsp;<a
                                    href="https://www.facebook.com/hashtag/monthoflove" target="_blank">#MonthOfLove</a>.
                        </p>

                        <p>Here’s the link of the contest post:&nbsp;<a href="https://goo.gl/Xjyfvm" target="_blank">https://goo.gl/Xjyfvm</a>&nbsp;.
                        </p>

                        <p>Every relationship is precious, isn’t it? PRAN Frooto Nepal came up with “Celebrate
                            relationships” campaign in which different creative of different relations were to be
                            shared. All the participants had to share the design which they wanted to celebrate and tag
                            their loved one along with the hashtags:&nbsp;<a
                                    href="https://business.facebook.com/hashtag/celebratingpreciousrelationships?source=feed_text&amp;story_id=1909835732366894"
                                    target="_blank">#CelebratingPreciousRelationships</a>&nbsp;#PRANFrootoNepal. This
                            campaign was all about spreading love and expressing what people felt and thus, embraced
                            people’s hearts generating an engagement of 24,671. The campaign designs became viral with
                            815 shares in total during the Valentine’s week. Below are few creatives that celebrated
                            precious relationships for the Valentine’s in 2017.</p>

                        <img src="{{url('frontend/images/gifs/frooto1.gif')}}" alt="">


                        <h2><strong>4. Pashupati Paints: KhelaunKhusika Rangharu</strong></h2>

                        <p>Since&nbsp;<em>Holi</em>&nbsp;is all about celebrating the joys of colours, for a paint
                            company, it’s the prime time to come up with an intriguing campaign and engage its audience.
                            Pashupati Paints did the same too with #KhelaunKhusikaRangHaru contest. This was a GIPHY
                            campaign where the audience had to capture the colourful face. Escaping the mainstream, this
                            campaign indulged playfulness in emotions and colours. Different faces with different
                            emotions appeared in the GIPHY; all dark and faded except for the one with happy glace and
                            colourful emotion that stood out from the rest. The participants had to capture and
                            screenshot the same colourful happy face. A simple and engaging campaign surely added a
                            bunch of happiness to the<em>&nbsp;Holi&nbsp;</em>fever and brought people in the mood to
                            celebrate the festival of colours.</p>

                        <img src="{{url('frontend/images/gifs/pashupati1.gif')}}" alt="">


                        <p>Contest Post Link:&nbsp;<a href="https://goo.gl/pXkPJU"
                                                      target="_blank">https://goo.gl/pXkPJU</a></p>

                        <h2><strong>5. Siddhartha Cement: Joddai Cha Sara Nepalilai</strong></h2>

                        <p>It is usually challenging for infrastructure brands like Siddhartha Cement to associate with
                            individuals. Siddhartha Cement’s core focus on its value proposition “जोड्दै छ सारा
                            नेपालीलाई” demanded a patriotic feel activity.A Facebook frame campaign added a perfect
                            touch to the baby steps into the brand’s&nbsp;Digital Marketing&nbsp;world. This campaign
                            initiated during the second phase of Nepal’s election 2074. “के कुराले जोड्दै छ सारा
                            नेपालीलाई?&nbsp;Was the question that wrapped the whole campaign. Six different Facebook
                            frames were developed. The audience had a chance to express their opinions on what they
                            thought unites Nepali people with six different frames of विश्वास, भावना, शिक्षा, राजनीति,
                            संस्कृति and विकास. The campaign was held from Nov 16- Dec 14, 2017. The participants had to
                            change their profile picture among the six frames by clicking on the “Try it” button and had
                            to send the screenshot of the updated profile picture in the comment section of the contest
                            post.</p>

                        <img src="{{url('frontend/images/gifs/siddha1.gif')}}" alt="">

                    </div>
                </div>
            </div>
        </article>

    @else

        <!-- Post Content -->
        <article class="post-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-md-10 mx-auto">
                        <p>{!! $blogData->body !!}</p>
                        @foreach($key as $value)
                            <span>{!! $value->subtitle !!}</span>
                        @endforeach
                    </div>
                </div>
            </div>
        </article>
    @endif

    {{--<span class="likebtn-wrapper" data-identifier="item_1"></span>--}}
    {{--<script>(function (d, e, s) {--}}
            {{--if (d.getElementById("likebtn_wjs")) return;--}}
            {{--a = d.createElement(e);--}}
            {{--m = d.getElementsByTagName(e)[0];--}}
            {{--a.async = 1;--}}
            {{--a.id = "likebtn_wjs";--}}
            {{--a.src = s;--}}
            {{--m.parentNode.insertBefore(a, m)--}}
        {{--})(document, "script", "//w.likebtn.com/js/w/widget.js");</script>--}}


    {{--Related Blogs--}}
    <section class="blog bg-light-grey pt-0 mt-5">

        <div class="container">
            <!-- Blog title -->
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">RELATED BLOG</h2>

            </div>
            <!-- End Blog title -->
            <div class="row ">

                @foreach($RelatedBlogData as $value)
                    @if($value->id !== $blogData->id)
                        <div class="col-lg-4" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="300">
                            <div class="card">
                                <img class="card-img-top"
                                     src="{{ asset('uploads/images/blogs/' . $value->upload1) }}" alt="Card image">
                                <div class="card-body mb-3">
                                    <p class="card-date text-muted">{{$value->created_at->format('j F, Y')}}</p>
                                    <h4 class="card-title mb-3">{{$value->title}}</h4>
                                    <p class="card-text">{!! str_limit(strip_tags($value->body,100)) !!}</p>
                                    <a href="{{route('view-blog',[str_slug("$value->title","-"),$value->id,])}}"
                                       class="stretched-link">See Details</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach


            </div>
        </div>

    </section>

    <section class="pt-5 mt-5">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-md-10 mx-auto">
                    @if(\App\Comment::countComment($blogData->id) === 1)
                        <h3 class="mb-5">{{\App\Comment::countComment($blogData->id)}} Comment</h3>
                    @else
                        <h3 class="mb-5">{{\App\Comment::countComment($blogData->id)}} Comments</h3>
                @endif
                <!-- comment-list -->
                    <ul class="comment-list">

                        @foreach($commentData as $value)
                            @if($value->blog_id == $blogData->id)

                                <li class="comment">
                                    <div class="vcard bio">
                                        <img src="{{url('frontend/images/blog-comment.jpg')}}" alt="Image placeholder">
                                    </div>
                                    <div class="comment-body">
                                        <h3>{{$value->name}}</h3>
                                        <div class="meta">{{$value->created_at->diffForHumans()}}</div>
                                        <p>{!! $value->comment !!}</p>

                                    </div>
                                </li>
                            @endif
                        @endforeach

                    </ul>
                    <!-- END comment-list -->
                </div>
            </div>
        </div>
    </section>

    <!-- ==================================
         Comment form
         ==================================
    -->
    <section class="pt-0">
        <div class="container contact-form ">
            <div class="row">
                <div class="col-lg-10 col-md-10 mx-auto">
                    <div class="mb-5 comment-form-wrap">
                        <h2 data-aos="fade-up" data-aos-easing="ease">Leave your comment </h2>
                    </div>

                    <form action="javascript:void(0)" id="blogcomment" method="post"
                          onsubmit="return ValidateBlogCommentForm();"
                          class="p-5" novalidate
                          data-aos="fade-up"
                          data-aos-easing="ease">

                        {{csrf_field()}}

                        <input type="hidden" name="blog_id"
                               value={{$blogData->id}}
                                       class="form-control"
                               required>
                        <div class="form-group">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Name *"
                                   value=""/>
                            <a href="#" style="color: red">{{$errors->first('name')}}</a>
                            <h7 id="error_message1"></h7>

                        </div>
                        <div class="form-group">
                            <input type="text" id="email" name="email" class="form-control" placeholder="Email *"
                                   value=""/>
                            <a href="#" style="color: red">{{$errors->first('email')}}</a>
                            <h7 id="error_message2"></h7>

                        </div>

                        <div class="form-group">
                            <textarea name="comment" id="comment" class="form-control" placeholder="Your Message *"
                                      style="width: 100%; height: 152px;"></textarea>
                            <a href="#" style="color: red">{{$errors->first('comment')}}</a>
                            <h7 id="error_message3"></h7>

                        </div>
                        <div class="form-group">
                            <input type="submit" name="btnSubmit" id="send_form" class="btn btn-fill"
                                   value="Post Comment"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    {{--footer contact--}}

    @include('frontend.layouts.footer-contact')

@endsection

<script src="{{url('frontend/jquery/sweetalert.min.js')}}"></script>

@include('sweet::alert')