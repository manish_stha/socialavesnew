@extends('frontend.master.master')

@include('frontend.layouts.top-nav-2')

@section('content')

    <section class="ss-portfolio">

        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">What we did ?</marquee>
        </div>

        <div class="container">
            <div class="section-heading text-center">
                <h2>Our Latest Featured Projects</h2>
                <p>Whenever your audience is searching for you, be found on the top</p>

                @include('frontend.layouts.messages')

                <hr>
            </div>

        </div>
        <div class="container">
            <div class="row">
                @foreach($portfolioData as $data)
                    <div class="col-lg-4 col-md-4 col-sm-6 design  campaign webdesign" data-aos="fade-up"
                         data-aos-easing="ease"
                         data-aos-delay="200">
                        <a href="{{route('view-portfolio',[str_slug("$data->title","-"),$data->id,])}}">
                            <div class="h_gallery_item">
                                <div class="g_img_item">
                                    <img class="img-fluid col-center"
                                         src="{{url('uploads/images/portfolio/preview_image/'.$data->preview_image)}}"
                                         alt="">
                                    <img class="light" src="{{url('frontend/images/eye.svg')}}" alt="">
                                </div>
                                <div class="g_item_text">
                                    <h5 class="mt-4">{{$data->title}}</h5>
                                    <p class="mb-0">{{$data->client}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>

        </div>
    </section>


    <section class="ss-pagination ss-pagination-center">

         <!-- START: ss-Pagination -->
        <div class="container">
            @if($portfolioData->onFirstPage())
                <a class="ss-pagination-center" href="{{route('portfolio')}}" title="Main Page">
                    <i class="fad fa-2x fa-th"></i>
                </a>
                @if($portfolioData->hasMorePages())

                    <a class="ss-pagination-next" href="{{$portfolioData->nextPageUrl()	}}" title="Next"><i
                                class="far fa-2x fa-long-arrow-right"></i></a>

                @endif
            @else
                <a class="ss-pagination-prev" href="{{$portfolioData->PreviousPageUrl()}}" title="Previous">
                    <i class="far fa-2x fa-long-arrow-left"></i></a>
                <a class="ss-pagination-center" href="{{route('portfolio')}}" title="Main Page">
                    <i class="fad fa-2x fa-th"></i>
                </a>
                @if($portfolioData->hasMorePages())

                    <a class="ss-pagination-next" href="{{$portfolioData->nextPageUrl()	}}" title="Next"><i
                                class="far fa-2x fa-long-arrow-right"></i></a>

                @endif
            @endif


        </div>
        <!-- END: ss-Pagination -->

    </section>
    <!-- ==================================
         END PORTFOLIO PAGINATION
         ==================================-->
         
         

    <section class="bg-soft-teal">
        <div class="container pl-5 pr-5">
            <div class=" row d-flex justify-content-center align-content-center align-items-center">
                <div class="col text-center">
                    <img class="mb-2" src="{{url('frontend/images/phone-call.svg')}}" width="50px;">
                    <p class="mb-0">Interested about hiring us ?</p>
                    <h3 class="color-teal mb-4">Go ahead and talk with us</h3>
                    <p class="mb-0">Call Us</p>
                    <h3 class="mb-2">+977-1-5537595</h3>
                    <a class="btn btn-fill mt-0" href="#queries">Tell us your project</a>
                </div>
            </div>
        </div>
    </section>

    {{--Testimonials Video--}}
    <section class="t-video p-0">

        <div class="container-fluid ">
            <div class="row d-flex justify-content-end align-items-center">
                @foreach($ClientTestimonyData as $data)

                    <div class="col-lg-12 col-md-12 no-padding">
                        <a id="play-video" class="video-play-button" href="{{$data->link}}"
                           data-fancybox data-animation-effect="zoom-in-out" data-animation-duration="600">
                            <span></span>
                        </a>

                        <div class=" video-text">
                            <div class="w-100 text-white">
                                <h4>{{$data->title}}</h4>
                                <p class="mb-0 text-white">CLIENT TESTIMONIALS</p>
                            </div>
                        </div>

                        <div class="overlay"></div>
                        <img class="img-fluid" src="{{url('uploads/images/client-testimony/'. $data->preview_image)}}"
                             alt="">
                    </div>

                @endforeach

            </div>
        </div>
    </section>

    <section data-aos="fade-up" data-aos-easing="ease" data-aos-delay="300">
        <div class="container">

            <div class="row justify-content-center text-center">
                <div class="col-md-7">
                    <div class="slide-one-item owl-carousel">

                        @foreach($clientData as $data)
                            <blockquote class="testimonial-1">
                                <i class="fas fa-quote-left"></i>
                                <p>{!! $data->comment !!}</p>
                                <span class="text-black">{{$data->name}}</span> &mdash; <span
                                        class="small text-muted">{{$data->designation}}</span>
                            </blockquote>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="pt-0 bt" data-aos="fade-up" data-aos-easing="ease">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div id="logo-carousel" class="owl-carousel owl-theme">
                        @foreach($brandData as $value)
                            <div class="item"><img src="{{url('uploads/images/home/brands/'.$value->image)}}">
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- ==================================
         Contact form
         ==================================
    -->
    <div class="pattern-down" data-negative="false">
        <svg viewBox="0 0 283.5 27.8" preserveAspectRatio="none">
            <path class="grey-fill"
                  d="M283.5,9.7c0,0-7.3,4.3-14,4.6c-6.8,0.3-12.6,0-20.9-1.5c-11.3-2-33.1-10.1-44.7-5.7 s-12.1,4.6-18,7.4c-6.6,3.2-20,9.6-36.6,9.3C131.6,23.5,99.5,7.2,86.3,8c-1.4,0.1-6.6,0.8-10.5,2c-3.8,1.2-9.4,3.8-17,4.7 c-3.2,0.4-8.3,1.1-14.2,0.9c-1.5-0.1-6.3-0.4-12-1.6c-5.7-1.2-11-3.1-15.8-3.7C6.5,9.2,0,10.8,0,10.8V0h283.5V9.7z M260.8,11.3  c-0.7-1-2-0.4-4.3-0.4c-2.3,0-6.1-1.2-5.8-1.1c0.3,0.1,3.1,1.5,6,1.9C259.7,12.2,261.4,12.3,260.8,11.3z M242.4,8.6 c0,0-2.4-0.2-5.6-0.9c-3.2-0.8-10.3-2.8-15.1-3.5c-8.2-1.1-15.8,0-15.1,0.1c0.8,0.1,9.6-0.6,17.6,1.1c3.3,0.7,9.3,2.2,12.4,2.7  C239.9,8.7,242.4,8.6,242.4,8.6z M185.2,8.5c1.7-0.7-13.3,4.7-18.5,6.1c-2.1,0.6-6.2,1.6-10,2c-3.9,0.4-8.9,0.4-8.8,0.5 c0,0.2,5.8,0.8,11.2,0c5.4-0.8,5.2-1.1,7.6-1.6C170.5,14.7,183.5,9.2,185.2,8.5z M199.1,6.9c0.2,0-0.8-0.4-4.8,1.1  c-4,1.5-6.7,3.5-6.9,3.7c-0.2,0.1,3.5-1.8,6.6-3C197,7.5,199,6.9,199.1,6.9z M283,6c-0.1,0.1-1.9,1.1-4.8,2.5s-6.9,2.8-6.7,2.7  c0.2,0,3.5-0.6,7.4-2.5C282.8,6.8,283.1,5.9,283,6z M31.3,11.6c0.1-0.2-1.9-0.2-4.5-1.2s-5.4-1.6-7.8-2C15,7.6,7.3,8.5,7.7,8.6  C8,8.7,15.9,8.3,20.2,9.3c2.2,0.5,2.4,0.5,5.7,1.6S31.2,11.9,31.3,11.6z M73,9.2c0.4-0.1,3.5-1.6,8.4-2.6c4.9-1.1,8.9-0.5,8.9-0.8 c0-0.3-1-0.9-6.2-0.3S72.6,9.3,73,9.2z M71.6,6.7C71.8,6.8,75,5.4,77.3,5c2.3-0.3,1.9-0.5,1.9-0.6c0-0.1-1.1-0.2-2.7,0.2  C74.8,5.1,71.4,6.6,71.6,6.7z M93.6,4.4c0.1,0.2,3.5,0.8,5.6,1.8c2.1,1,1.8,0.6,1.9,0.5c0.1-0.1-0.8-0.8-2.4-1.3  C97.1,4.8,93.5,4.2,93.6,4.4z M65.4,11.1c-0.1,0.3,0.3,0.5,1.9-0.2s2.6-1.3,2.2-1.2s-0.9,0.4-2.5,0.8C65.3,10.9,65.5,10.8,65.4,11.1 z M34.5,12.4c-0.2,0,2.1,0.8,3.3,0.9c1.2,0.1,2,0.1,2-0.2c0-0.3-0.1-0.5-1.6-0.4C36.6,12.8,34.7,12.4,34.5,12.4z M152.2,21.1  c-0.1,0.1-2.4-0.3-7.5-0.3c-5,0-13.6-2.4-17.2-3.5c-3.6-1.1,10,3.9,16.5,4.1C150.5,21.6,152.3,21,152.2,21.1z">
            </path>
        </svg>
    </div>

    <section id="queries" class="bg-light-grey">
        <div class="container contact-form ">
            <div class="mb-5 text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">Tell us about your project </h2>
                <p data-aos="fade-up" data-aos-easing="ease">Let us help you get your business online and grow it with
                    passion</p>
            </div>


            <form action="{{route('add-contact')}}" onsubmit="return ValidateServiceForm();" method="post"
                  class="p-5" novalidate data-aos="fade-up" data-aos-easing="ease">
                {{csrf_field()}}

                <div class="row" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Name *"
                                   value="{{old('name')}}">
                            <a href="#" style="color: red;">{{$errors->first('name')}}</a>
                            <h7 id="error_message1"></h7>


                        </div>
                        <div class="form-group">
                            <input type="email" id="email" name="email" class="form-control" placeholder="Email *"
                                   value="{{old('email')}}">
                            <a href="#" style="color: red;">{{$errors->first('email')}}</a>
                            <h7 id="error_message2"></h7>

                        </div>

                        <div class="form-group">
                            <input type="text" id="phone" name="phone" class="form-control" placeholder="Phone Number *"
                                   value="{{old('phone')}}">
                            <a href="#" style="color: red;">{{$errors->first('phone')}}</a>
                            <h7 id="error_message3"></h7>

                        </div>


                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="text" id="company" name="company" class="form-control"
                                   placeholder="Company Name"
                                   value="{{old('company')}}">
                            <h7 id="error_message4"></h7>

                        </div>
                        <div class="form-group">
                        <textarea name="comment" id="message" class="form-control" placeholder="Your Message *"
                                  style="width: 100%; height: 152px;">{{old('comment')}}</textarea>
                            <a href="#" style="color: red;">{{$errors->first('comment')}}</a>
                            <h7 id="error_message5"></h7>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="submit" name="btnSubmit" class="btn btn-fill"
                                   value="Hear from an expert"/>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </section>


    <!-- ==================================
         FOOTER CONTACT
         ==================================
    -->
    @include('frontend.layouts.footer-contact')
    <!-- ==================================
         END FOOTER CONTACT
         ==================================
    -->

    <!-- ==================================
         FOOTER
         ==================================
    -->
@endsection

<script src="{{url('frontend/jquery/sweetalert.min.js')}}"></script>

@include('sweet::alert')