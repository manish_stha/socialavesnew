@extends('frontend.master.master')

@include('frontend.layouts.top-nav-2')

@section('content')

    <section class="job-description">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">Vacancy Open</marquee>
        </div>
        <!-- End Marquee -->
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    {{--<h1 class="display-3 mt-5">Content<span class="color-red"> Writer</span></h1>--}}
                    <h1 class="display-4 mt-5">{!! $postData->title !!}</h1>

                    @include('frontend.layouts.messages')


                    <p class="color-red"><b><span class="text-muted">Required Numbers:</span> {{$postData->number}}
                            &nbsp;&nbsp;|&nbsp;&nbsp; Full
                            Time</b></p>

                </div>
                <div class="col-lg-8 col-center text-center">
                    {!! $postData->description !!}
                </div>

            </div>


            <div class="accordion mt-5" id="vacancyDescription">
                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button type="button" class="btn btn-link" data-toggle="collapse"
                                    data-target="#collapseOne"><i class="fa fa-plus"></i> About This Position
                            </button>
                        </h2>
                    </div>
                    <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                         data-parent="#vacancyDescription">
                        <div class="card-body">
                            <p>{!! $postData->about !!}</p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h2 class="mb-0">
                            <button type="button" class="btn btn-link collapsed" data-toggle="collapse"
                                    data-target="#collapseTwo"><i class="fa fa-plus"></i> What You Will Be Doing?
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                         data-parent="#vacancyDescription">
                        <div class="card-body">
                            <p>{!! $postData->works !!}</p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button type="button" class="btn btn-link collapsed" data-toggle="collapse"
                                    data-target="#collapseThree"><i class="fa fa-plus"></i> What You Need for this
                                Position?
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                         data-parent="#vacancyDescription">
                        <div class="card-body">
                            <p>{!! $postData->needs !!}</p>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header" id="headingFour">
                        <h2 class="mb-0">
                            <button type="button" class="btn btn-link collapsed" data-toggle="collapse"
                                    data-target="#collapseFour"><i class="fa fa-plus"></i> Bonus Skills
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                         data-parent="#vacancyDescription">
                        <div class="card-body">
                            <p>{!! $postData->bonus_skills !!}</p>
                        </div>
                    </div>
                </div>
                <!-- Job Apply Part -->
                <div class="card">
                    <div class="card-header" id="headingFive">
                        <h2 class="mb-0">
                            <button type="button" class="btn btn-link collapsed" data-toggle="collapse"
                                    data-target="#collapseFive"><i class="fa fa-plus"></i> Application Process
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive"
                         data-parent="#vacancyDescription">
                        <div class="card-body contact-form">

                            <p class="mb-5 text-center">To apply for this job please fill the form or email your CV
                                (2
                                pages max) and <br>cover letter to <a href="#">career@socialaves.com</a></p>

                            <form action="{{route('add-application')}}" onsubmit="return ValidateApplicationForm();"
                                  method="post" class="p-5 bg-light" novalidate data-aos="fade-up"
                                  data-aos-easing="ease" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="hidden" name="post_id" class="form-control"
                                                   value="{{$postData->id}}">

                                            <input type="text" id="name" name="name" class="form-control"
                                                   placeholder="Name *"
                                                   value="{{old('name')}}">
                                            <a href="#" style="color: red;">{{$errors->first('name')}}</a>
                                            <h7 id="error_message1"></h7>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="email" name="email" class="form-control"
                                                   placeholder="Email *" value="{{old('email')}}">
                                            <a href="#" style="color: red;">{{$errors->first('email')}}</a>
                                            <h7 id="error_message2"></h7>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" id="phone" name="contact" class="form-control"
                                                   placeholder="Phone Number *" value="{{old('contact')}}">
                                            <a href="#" style="color: red;">{{$errors->first('contact')}}</a>
                                            <h7 id="error_message3"></h7>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" id="address" name="address" class="form-control"
                                                   placeholder="Address*" value="{{old('address')}}">
                                            <a href="#" style="color: red;">{{$errors->first('address')}}</a>
                                            <h7 id="error_message4"></h7>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="custom-file">
                                            <input id="cv" type="file" class="custom-file-input " name="cv">
                                            <label for="customFile" class="custom-file-label ">Choose file</label>
                                        </div>
                                        <a href="#" style="color: red">{{$errors->first('cv')}}</a>
                                        <h7 id="error_message5"></h7>
                                    </div>

                                    <div class="col-md-4 col-center mt-4">
                                        <div class="form-group">
                                            <input type="submit" name="btnSubmit" class="btn btn-fill w-75"
                                                   value="APPLY NOW"/>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- Job Apply Part -->

            </div>

        </div>
    </section>

    <!--<section class="bg-light-grey">-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-lg-6 col-center text-center">-->
    <!--                <h2 class="mb-4">Don’t See Your Role?</h2>-->
    <!--                <p class="text-muted mb-4">We’re always looking for talented people, so send us <br>your resume and-->
    <!--                    we’ll be in touch if there’s a fit.</p>-->
    <!--                <a href="" data-toggle="modal" data-target="#form" class="btn btn-fill ">Apply Now</a>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->

    <!--    <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"-->
    <!--         aria-hidden="true">-->
    <!--        <div class="modal-dialog modal-dialog-centered" role="document">-->
    <!--            <div class="modal-content">-->
    <!--                <div class="modal-header p-5 border-bottom-0">-->
    <!--                    <h5 class="modal-title" id="exampleModalLabel">Fill your Details</h5>-->
    <!--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">-->
    <!--                        <span aria-hidden="true">&times;</span>-->
    <!--                    </button>-->
    <!--                </div>-->

    <!--                <form action="{{route('add-application')}}" method="post"-->
    <!--                      onsubmit="return ValidateApplicationForm();" enctype="multipart/form-data">-->

    <!--                    {{csrf_field()}}-->

    <!--                    <div class="modal-body pr-5 pl-5 pb-5 pt-0 contact-form">-->
    <!--                        <div class="row">-->
    <!--                            <div class="col-md-12">-->
    <!--                                <div class="form-group">-->
    <!--                                    <input type="text" name="name" id="name" class="form-control"-->
    <!--                                           placeholder="Name *"-->
    <!--                                           value="" required/>-->
    <!--                                    <a href="#" style="color: red;">{{$errors->first('name')}}</a>-->
    <!--                                    <h7 id="error_message1"></h7>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <div class="form-group">-->
    <!--                                    <input type="email" name="email" id="email" class="form-control"-->
    <!--                                           placeholder="Email *"-->
    <!--                                           value="" required/>-->
    <!--                                    <a href="#" style="color: red;">{{$errors->first('email')}}</a>-->
    <!--                                    <h7 id="error_message2"></h7>-->
    <!--                                </div>-->
    <!--                            </div>-->

    <!--                            <div class="col-md-12">-->
    <!--                                <div class="form-group">-->
    <!--                                    <input type="text" id="phone" name="contact" class="form-control"-->
    <!--                                           placeholder="Phone Number *" value="" required/>-->
    <!--                                    <a href="#" style="color: red;">{{$errors->first('contact')}}</a>-->
    <!--                                    <h7 id="error_message3"></h7>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <div class="form-group">-->
    <!--                                    <input type="text" name="address" id="address" class="form-control"-->
    <!--                                           placeholder="Address*"-->
    <!--                                           value="" required/>-->
    <!--                                    <a href="#" style="color: red;">{{$errors->first('address')}}</a>-->
    <!--                                    <h7 id="error_message4"></h7>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                            <div class="col-md-12">-->
    <!--                                <div class="custom-file">-->
    <!--                                    <input id="cv" type="file" class="custom-file-input" name="cv">-->
    <!--                                    <label for="customFile" class="custom-file-label ">Choose file</label>-->
    <!--                                </div>-->
    <!--                                <a href="#" style="color: red">{{$errors->first('cv')}}</a>-->
    <!--                                <h7 id="error_message5"></h7>-->
    <!--                            </div>-->

    <!--                            <div class="col-md-4 col-center text-center mt-4">-->
    <!--                                <div class="form-group">-->
    <!--                                    <input type="submit" name="btnSubmit" class="btn btn-fill" value="SUBMIT"/>-->
    <!--                                </div>-->
    <!--                            </div>-->
    <!--                        </div>-->
    <!--                    </div>-->
    <!--                </form>-->

    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->


    {{--Team Section--}}
    <section class="team pb-0 bg-white">

        <div class="owl-carousel block-1">

            @foreach($teamData as $team)
                <a class="thumb" href="{{url('uploads/images/our-testimony/'.$team->image)}}">
                    <div class="title-text">
                        <h3>{{$team->name}}</h3>
                        <span class="sub-title">{{$team->designation}}</span>
                    </div>
                    <img src="{{url('uploads/images/our-testimony/'.$team->image)}}" alt="Image" class="img-fluid">
                </a>
            @endforeach


        </div>

    </section>


    @include('frontend.layouts.footer-contact-for-career-team')

@endsection

<script src="{{url('frontend/jquery/sweetalert.min.js')}}"></script>

@include('sweet::alert')