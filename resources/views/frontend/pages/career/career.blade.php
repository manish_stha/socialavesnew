@extends('frontend.master.master')

@include('frontend.layouts.top-nav-2')

@section('content')

    <!-- ==================================
     CAREER
     ==================================
-->

    <section class="career">

        <div class="section-heading text-center">
            <h2 data-aos="fade-up" data-aos-easing="ease">Become a part <br>of our big family</h2>
            <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">Work is fun here. Ready?
            </p>

            @include('frontend.layouts.messages')


            <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
        </div>

        <div class="marquee-career">
            <marquee behavior="scroll" direction="left" scrollamount="5">Career</marquee>
        </div>

        <img class="img-fluid" src="{{url('frontend/images/us.jpg')}}">

        <div class="container">
            <div class="row">
                <div class="col-lg-12 pt-4 mb-0 text-uppercase text-center">
                    <a class="mb-3" href="#view-career">See Opportunities<br></a>

                </div>

                <div class="col-lg-12">
                    <div class="arrow">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>

    </section>



    <section class="perks p-0">
        <div class="section-heading text-center">
            <h2 data-aos="fade-up" data-aos-easing="ease">Social Aves Work Perks</h2>
            <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">Our creative team </p>
            <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
        </div>
        <div class="container text-center">
            <div class="row">

                @foreach($perkData as $perk)
                    <div class="col-lg-4 mb-5">
                        <img class="img mb-3" src="{{url('uploads/images/career/work-perks/'.$perk->image)}}">
                        <h3>{!! $perk->title !!}</h3>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

    @foreach($storyData as $value)
        <section class="employee-story">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 employee-title  bg-soft-teal">
                        <h5>Employee Stories</h5>
                        <div class="display-3">{{$value->header}}</div>
                        <a href="#"><h6>Read {{$value->name}}'s Story</h6></a>
                    </div>
                    <div class="col-lg-4 bg-dark-teal">
                    </div>
                </div>

                <div class="row employee-detail">
                    <div class="col-lg-8 p-5">
                        <div class="display-1">{{$value->name}}</div>
                        <h6>{{$value->designation}}</h6>
                    </div>
                    <div class="col-lg-4">
                        <img class="img-fluid" src="{{url('uploads/images/stories/'.$value->image1)}}">
                    </div>
                </div>

                <div class="row employee-description">
                    <div class="col-lg-4">
                        <img class="img-fluid" src="{{url('uploads/images/stories/'.$value->image2)}}">
                    </div>
                    <div class="col-lg-8 p-5 ">
                        <h4>{{$value->title1}}</h4>
                        <p>{!! $value->description1 !!}</p>
                        <h4>{{$value->title2}}</h4>
                        <p>{!! $value->description2 !!}</p>
                    </div>
                </div>

                <div class="row employee-description">

                    <div class="col-lg-8 p-5 ">
                        <h4>{{$value->title3}}</h4>
                        <p>{!! $value->description3 !!}</p>

                        <h4>{{$value->title4}}</h4>
                        <p>{!! $value->description4 !!}</p>

                    </div>
                    <div class="col-lg-4">
                        <img class="img-fluid" src="{{url('uploads/images/stories/'.$value->image3)}}">
                    </div>
                </div>

            </div>
        </section>
    @endforeach

    {{--Vacancy Available--}}
    <section id="view-career" class="vacancy pt-0 bg-light-grey">
        <div class="container ">
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">Check Out Our Current Openings</h2>
                <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">WE WELCOME YOUNG ENERGY LIKE YOU</p>
                <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
            </div>
            <div class="container ">
                <div class="row">
                    <div class="col-lg-8 col-center">
                        <div class="row">

                            @foreach($postData as $post)

                                <div class="col-lg-6 mb col-center">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="job-num mt-4 text-center">
                                                <p class="mb-0">Required</p>
                                                <h2>{{$post->number}}</h2>
                                            </div>
                                            <div class="job-degination text-center">
                                                <h2>{{$post->title}}</h2>
                                                <p class="color-red"><b>{{$post->job_type}}</b></p>
                                            </div>
                                        </div>

                                        <a href="{{route('vacancy-detail',[str_slug("$post->title","-"),$post->id])}}"
                                           role="button" class="btn job-btn">Apply Now</a>
                                    </div>
                                </div>

                            @endforeach


                        </div>
                    </div>
                </div>

            </div>

            @if($CountPostData == 0)

                <h2 class="text-center">Vacancy is not available currently.</h2>

            @endif
        </div>
    </section>



    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-center text-center">
                    <h2 class="mb-4">Don’t See Your Role?</h2>
                    <p class="text-muted mb-4">We’re always looking for talented people, so send us <br>your resume and
                        we’ll be in touch if there’s a fit.</p>
                    <a href="" data-toggle="modal" data-target="#form" class="btn btn-fill ">Apply Now</a>
                </div>
            </div>
        </div>

        <div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header p-5 border-bottom-0">
                        <h5 class="modal-title" id="exampleModalLabel">Fill your Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form action="{{route('add-application')}}" method="post"
                          onsubmit="return ValidateApplicationForm();" enctype="multipart/form-data">

                        {{csrf_field()}}

                        <div class="modal-body pr-5 pl-5 pb-5 pt-0 contact-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="name" id="name" class="form-control"
                                               placeholder="Name *"
                                               value="" required/>
                                        <a href="#" style="color: red;">{{$errors->first('name')}}</a>
                                        <h7 id="error_message1"></h7>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control"
                                               placeholder="Email *"
                                               value="" required/>
                                        <a href="#" style="color: red;">{{$errors->first('email')}}</a>
                                        <h7 id="error_message2"></h7>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" id="phone" name="contact" class="form-control"
                                               placeholder="Phone Number *" value="" required/>
                                        <a href="#" style="color: red;">{{$errors->first('contact')}}</a>
                                        <h7 id="error_message3"></h7>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="address" id="address" class="form-control"
                                               placeholder="Address*"
                                               value="" required/>
                                        <a href="#" style="color: red;">{{$errors->first('address')}}</a>
                                        <h7 id="error_message4"></h7>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="custom-file">
                                        <input id="cv" type="file" class="custom-file-input" name="cv">
                                        <label for="customFile" class="custom-file-label ">Choose file</label>
                                    </div>
                                    <a href="#" style="color: red">{{$errors->first('cv')}}</a>
                                    <h7 id="error_message5"></h7>
                                </div>

                                <div class="col-md-4 col-center text-center mt-4">
                                    <div class="form-group">
                                        <input type="submit" name="btnSubmit" class="btn btn-fill" value="SUBMIT"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>



    {{--Team Section--}}
    <section class="team pb-0 bg-white">

        <div class="owl-carousel block-1">

            @foreach($teamData as $team)
                <a class="thumb" href="{{url('uploads/images/our-testimony/'.$team->image)}}">
                    <div class="title-text">
                        <h3>{{$team->name}}</h3>
                        <span class="sub-title">{{$team->designation}}</span>
                    </div>
                    <img src="{{url('uploads/images/our-testimony/'.$team->image)}}" alt="Image" class="img-fluid">
                </a>
            @endforeach


        </div>

    </section>


    @include('frontend.layouts.footer-contact-for-career-team')

@endsection

<script src="{{url('frontend/jquery/sweetalert.min.js')}}"></script>

@include('sweet::alert')