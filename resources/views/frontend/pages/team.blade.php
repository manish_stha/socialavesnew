@extends('frontend.master.master')

@include('frontend.layouts.top-nav')


@section('content')
    <!-- ==================================
     TEAM
     ==================================
-->

    <section class="team bg-white">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="right" scrollamount="5">team</marquee>
        </div>
        <!-- End Marquee -->

        <div class="container-fluid p-0">
            <!-- team title -->
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">Some Say AGENCY <br>We Say TEAM</h2>
                <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">A big family of diverse individuals
                </p>
                <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
            </div>
            <!-- end team title -->
            <div class="container">
                @foreach($teamData as $team)
                    @if($team->designation == 'CEO')
                        <div class="row team-margin">
                            <div class="col-md-6 col-lg-6 text-center" data-aos="fade-up" data-aos-easing="ease"
                                 data-aos-delay="300">
                                <img src="{{url('uploads/images/our-testimony/'.$team->image)}}" alt="Ravi Singhal"
                                     class="img-fluid">
                            </div>
                            <div class="col-md-6 col-lg-6" data-aos="fade-up" data-aos-easing="ease"
                                 data-aos-delay="300">
                                <div class="team-describe">
                                    <p><?= strip_tags($team->testimony) ?></p>
                                </div>
                                <div class="team-details">
                                    <h2>{{$team->name}}</h2>
                                    <p>{{$team->designation}}</p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="row no-gutters text-center">
                @foreach($teamData as $team)
                    @if($team->designation !== 'CEO')
                        <div class="col-md-6 col-lg-4 col-xl-3 mb-5" data-aos="fade-up" data-aos-easing="ease"
                             data-aos-delay="300">
                            <div class="container-team">
                                <img src="{{url('uploads/images/our-testimony/'.$team->image)}}" alt="Avatar"
                                     class="image">
                                <div class="team-overlay">
                                    <div class="team-content text-left p-4">
                                        <p>{!! $team->testimony !!}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="p-4 team-details">
                                <h2>{{$team->name}}</h2>
                                <p>{{$team->designation}}</p>
                            </div>
                        </div>
                    @endif
                @endforeach


            </div>


        </div>
    </section>
    <!-- ==================================
         END TEAM
         ==================================
    -->

    {{--Vacancy Available--}}
    <section id="view-career" class="vacancy pt-0 bg-light-grey">
        <div class="container ">
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">Join the TEAM! Check out our openings</h2>
                <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">WE WELCOME YOUNG ENERGY LIKE YOU</p>
                <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
            </div>
            <div class="container ">
                <div class="row">
                    <div class="col-lg-8 col-center">
                        <div class="row">
                            @foreach($postData as $post)

                                <div class="col-lg-6 mb col-center">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="job-num mt-4 text-center">
                                                <p class="mb-0">Required</p>
                                                <h2>{{$post->number}}</h2>
                                            </div>
                                            <div class="job-degination text-center">
                                                <h2>{!! $post->title !!}</h2>
                                                <p class="color-red"><b>{{$post->job_type}}</b></p>
                                            </div>
                                        </div>

                                        <a href="{{route('vacancy-detail',[str_slug("$post->title","-"),$post->id])}}"
                                           role="button" class="btn job-btn">Apply Now</a>
                                    </div>
                                </div>

                            @endforeach

                        </div>

                        @if($CountPostData !== 0)
                            <div class="row ">
                                <div class="col-lg-12 mt-5 text-center">
                                    <a class="btn btn-fill mt-0" href="{{route('career')}}">View all
                                        jobs</a>
                                </div>
                            </div>
                        @endif

                    </div>
                </div>
            </div>

            @if($CountPostData == 0)

                <h2 class="text-center">Vacancy is not available currently.</h2>

            @endif

        </div>
    </section>

    {{--Portfolio--}}
    <section class="ss-portfolio">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">Our Works</marquee>
        </div>
        <!-- End Marquee -->
        <div class="container">
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">Our Projects</h2>
                <p data-aos="fade-up" data-aos-easing="ease">These are some projects that you may be interested to
                    see.</p>
                <hr data-aos="fade-up" data-aos-easing="ease">
            </div>
            <div class="gallery_f_inner row">

                @foreach($PortfolioDataInOtherPages as $data)
                    <div class="col-lg-4 col-md-4 col-sm-6 design  seo" data-aos="fade-up" data-aos-easing="ease"
                         data-aos-delay="200">
                        <a href="{{route('view-portfolio',[str_slug("$data->title","-"),$data->id,])}}">
                            <div class="h_gallery_item">
                                <div class="g_img_item">
                                    <img class="img-fluid col-center"
                                         src="{{url('uploads/images/portfolio/preview_image/'.$data->preview_image)}}"
                                         alt="">
                                    <img class="light" src="{{url('frontend/images/eye.svg')}}" alt="">
                                </div>
                                <div class="g_item_text">
                                    <h5 class="mt-4">{{$data->title}}</h5>
                                    <p class="mb-0">{{$data->client}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>
            <div class="row ">
                <div class="col-lg-12 mt-5 text-center">
                    <a class="btn btn-fill mt-0" href="{{route('portfolio')}}">See More</a>
                </div>
            </div>
        </div>
    </section>



    @include('frontend.layouts.footer-contact-for-career-team')

@endsection