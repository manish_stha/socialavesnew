@extends('frontend.master.master')
@include('frontend.layouts.top-nav')

@section('content')


    <!-- ==================================
         BANNER
         ==================================
    -->
    <section class="main-header p-0 bgdark">

        <!-- Middle Banner Content -->
        <div class="banner-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 m-auto text-center col-sm-12 col-md-12">
                        <p>We love exploring new everyday to create awesome and unique experiences to the audience</p>
                        <a class="btn btn-home mt-0" href="{{route('service')}}">Explore our services</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Middle Banner Content -->

        <!-- Header Social -->
        <div class="header-social">
            <li><a href="https://www.facebook.com/socialaves/" target="_blank"><i
                            class="fab fa-facebook-f"></i></a></li>
            <li><a href="https://twitter.com/SocialAves" target="_blank"><i class="fab fa-twitter"></i></a>
            </li>
            <li><a href="https://www.instagram.com/avessocial/" target="_blank"><i
                            class="fab fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCJ2gYCjdce1idG5MJY4hbvw" target="_blank"><i
                            class="fab fa-youtube"></i></a></li>
            <li class="m-0"><a href="https://www.linkedin.com/company/social-aves-pvt-ltd-/"
                               target="_blank"><i class="fab fa-linkedin"></i></a></li>
        </div>
        <!-- End Header Social -->

        <!-- Header Scroll -->
        <div class="header-scroll">

            <a href="#about">
                <div class="scroll-downs">
                    <div class="mousey">
                        <div class="scroller"></div>
                    </div>
                </div>
            </a>
        </div>
        <!-- End Header Scroll -->

        <!-- Header Video Background -->
        <div class="overlay"></div>

        @foreach($videoData as $video)

            <video autoplay muted loop id="home-banner">
                <source src="{{url('uploads/videos/home/banner/'.$video->video)}}" type="video/mp4">
            </video>
    @endforeach

    <!-- End Header Video Background -->


    </section>
    <!-- ==================================
         END BANNER
         ==================================
    -->
    <section class="bgdark pt-0">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-center">
                    <div class="timeline">
                        <ul>
                            <li>
                                <div class="content">
                                    <h3>Roots developed</h3>
                                    <p>Roots developed- Impressed by the power of social media, a group of IT
                                        enthusiasts came together to create a breakthrough</p>
                                </div>
                                <div class="time">
                                    <h4>2009</h4>
                                </div>
                            </li>

                            <li>
                                <div class="content">
                                    <h3>Founded</h3>
                                    <p>The team decided to embark on the journey of changing the digital scenario of
                                        Nepal</p>
                                </div>
                                <div class="time">
                                    <h4>2012</h4>
                                </div>
                            </li>

                            <li>
                                <div class="content">
                                    <h3>9 years experienced</h3>
                                    <p>Successfully building brands and businesses for achieving great milestones in the
                                        digital arena</p>
                                </div>
                                <div class="time">
                                    <h4>2019</h4>
                                </div>
                            </li>

                            <div style="clear:both;"></div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- ==================================
       Company Information
       ==================================
    -->
    <section id="about" class="about_1 bgdark bt">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5" >Nepal’s No.1 Digital Marketing Agency</marquee>
        </div>
        <!-- End Marquee -->

        <div class="container text-center">
            <div class="row">
                <div class="col-lg-8 col-center">
                    <div class="mb-5">
                        <h2 >All our work begin with research and data-based analysis. </h2>
                    </div>
                    <div class="vertical col-center"></div>
                </div>
                <div class="col-lg-8 col-center">
                    <div class="mt-5 mb-5">
                        <p>In the world of technology, we focus on creating an emotional connection between your brand and your audience. Experimenting on new spurs creativity and when the creativity is blended with our expertise of a decade, we make your business soar high in the digital landscape.</p>
                        <p>That’s why, Creative Avians, critical thinkers, problem solvers, digital marketing enthusiasts−we love to be called any.</p>
                    </div>
                    <div class="vertical col-center"></div>
                </div>
                <div class="col-lg-10 col-center">
                    <div class="mt-5 mb-5">
                        <h2>Go an extra mile with us</br> For the next level business transformation</h2>
                    </div>
                </div>

            </div>
        </div>
    </section>


    <!-- ==================================
         SERVICES
         ==================================
    -->

    <section class="service-home">

        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">Services</marquee>
        </div>
        <!-- End Marquee -->

        @foreach($serviceHeaderData as $serviceheader)
            <div class="container">
                <!-- Service title -->
                <div class="section-heading text-center">
                    <h2 data-aos="fade-up" data-aos-easing="ease">{{strip_tags( $serviceheader->title) }}</h2>
                    <p data-aos="fade-up" data-aos-easing="ease"
                       data-aos-delay="200"><?= strip_tags(strtoupper($serviceheader->subtitle))?></p>
                    <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
                </div>
            @endforeach
            <!-- End Service title -->

                <div class="row justify-content-center mt-lg-8 mt-6" data-aos="fade-up" data-aos-easing="ease"
                     data-aos-delay="300">
                    <div class="col-xl-10 col-lg-9">
                        <div class="row justify-content-between">

                            @foreach($serviceData as $service)
                                <div class="col-xl-5 col-md-6 mb-5">
                                    <div class="media"><img class="align-self-start mr-2"
                                                            src="{{url('uploads/images/services/icons/'.$service->icon)}}">
                                        <div class="media-body">
                                            <h5><?=strip_tags($service->title)?></h5>
                                            <p><?=$service->subtitle?></p>
                                            // <a class="stretched-link" href="{{route('view-service',[str_slug("$service->title","-"),$service->id,])}}">Explore
                                            //     now</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                            <div class="col-xl-5 col-md-6 mb-5">
                                <div class="media ml-7">
                                    <div class="media-body">
                                        <h5 class="text-capitalize mb-3">To know more about our speciality</h5>
                                        <p>We strive to create brilliance and get results that drive sustained
                                            growth.</p>
                                        <a class="btn btn-fill mt-0" href="{{route('service')}}">CLICK HERE</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </section>



    {{--Testimonials Video--}}
    <section class="t-video p-0">

        <div class="container-fluid ">
            <div class="row d-flex justify-content-end align-items-center">
                @foreach($ClientTestimonyData as $data)

                    <div class="col-lg-12 col-md-12 no-padding">
                        <a id="play-video" class="video-play-button" href="{{$data->link}}"
                           data-fancybox data-animation-effect="zoom-in-out" data-animation-duration="600">
                            <span></span>
                        </a>

                        <div class=" video-text">
                            <div class="w-100 text-white">
                                <h4>{{$data->title}}</h4>
                                <p class="mb-0 text-white">CLIENT TESTIMONIALS</p>
                            </div>
                        </div>

                        <div class="overlay"></div>
                        <img class="img-fluid" src="{{url('uploads/images/client-testimony/'. $data->preview_image)}}"
                             alt="">
                    </div>

                @endforeach

            </div>
        </div>
    </section>

    {{--Portfolio--}}
    <section class="ss-portfolio">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">Our Works</marquee>
        </div>
        <!-- End Marquee -->
        <div class="container">
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">Our Projects</h2>
                <p data-aos="fade-up" data-aos-easing="ease">These are some projects that you may be interested to
                    see.</p>
                <hr data-aos="fade-up" data-aos-easing="ease">
            </div>
            <div class="gallery_f_inner row">

                @foreach($PortfolioDataInOtherPages as $data)
                    <div class="col-lg-4 col-md-4 col-sm-6 design  seo" data-aos="fade-up" data-aos-easing="ease"
                         data-aos-delay="200">
                        <a href="{{route('view-portfolio',[str_slug("$data->title","-"),$data->id,])}}">
                            <div class="h_gallery_item">
                                <div class="g_img_item">
                                    <img class="img-fluid col-center"
                                         src="{{url('uploads/images/portfolio/preview_image/'.$data->preview_image)}}"
                                         alt="">
                                    <img class="light" src="{{url('frontend/images/eye.svg')}}" alt="">
                                </div>
                                <div class="g_item_text">
                                    <h5 class="mt-4">{{$data->title}}</h5>
                                    <p class="mb-0">{{$data->client}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                @endforeach


            </div>
            <div class="row ">
                <div class="col-lg-12 mt-5 text-center">
                    <a class="btn btn-fill mt-0" href="{{route('portfolio')}}">See More</a>
                </div>
            </div>
        </div>
    </section>

    {{--Awards Section--}}
    <section class="p-0">
        <div class="paralax-mf bg-image" style="background-image: url('{{url('frontend/images/counters-bg.jpg')}}')">
            <div class="overlay-mf"></div>
            <div class="container">
                @foreach($FactData as $data)
                   <div class="row" data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
                        <div class="col-sm-3 col-lg-3">
                            <div class="counter-box">
                                <div class="counter-num">
                                    <p class="counter-count">{{$data->number1}}</p>
                                    <span class="counter-text">{{$data->title1}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-3">
                            <div class="counter-box">
                                <div class="counter-num">
                                    <p class="counter-count">{{$data->number2}}</p>
                                    <span class="counter-text">{{$data->title2}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-3">
                            <div class="counter-box pt-4 pt-md-0">
                                <div class="counter-num">
                                    <p class="counter-count">{{$data->number3}}</p>
                                    <span class="counter-text">{{$data->title3}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-lg-3">
                            <div class="counter-box pt-4 pt-md-0">
                                <div class="counter-num">
                                    <p class="counter-count">{{$data->number4}}</p>
                                    <span class="counter-text">{{$data->title4}}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>


    {{--Team Section--}}
    <section class="team pb-0 bg-white">

        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="right" scrollamount="5">team</marquee>
        </div>
        <!-- End Marquee -->

        <div class="container-fluid p-0">

            @foreach($teamHeaderData as $data)
                <div class="section-heading text-center">
                    <h2 data-aos="fade-up" data-aos-easing="ease">{{$data->title}}</h2>
                    <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">{{$data->subtitle}}</p>
                    <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
                </div>
            @endforeach

        </div>
        <div class="owl-carousel block-1">

            @foreach($teamData as $team)
                <a class="thumb" href="{{url('uploads/images/our-testimony/'.$team->image)}}">
                    <div class="title-text">
                        <h3>{{$team->name}}</h3>
                        <span class="sub-title">{{$team->designation}}</span>
                    </div>
                    <img src="{{url('uploads/images/our-testimony/'.$team->image)}}" alt="Image" class="img-fluid">
                </a>
            @endforeach


        </div>

    </section>


    <section class="p-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col meet-link text-center">
                    <h2>How can We make You shine?</h2>
                    <a class="btn btn-fill mt-0" href="{{route('team')}}">MEET THE ENTIRE TEAM</a>
                </div>
            </div>
        </div>
    </section>


    <!-- ==================================
       BLOG
       ==================================
  -->

    <section class="blog bg-white  ">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">Blog</marquee>
        </div>
        <!-- End Marquee -->
        <div class="container">
            <!-- Blog title -->
            <div class="section-heading text-center">
                <h2 data-aos="fade-up" data-aos-easing="ease">FROM THE BLOG</h2>
                <p data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
                    Our
                    news and stories</p>
                <hr data-aos="fade-up" data-aos-easing="ease" data-aos-delay="200">
            </div>
            <!-- End Blog title -->
            <div class="row ">
                @foreach($blogData as $blog)
                    <div class="col-lg-4" data-aos="fade-up" data-aos-easing="ease"
                         data-aos-delay="300">
                        <div class="card">
                            <img class="card-img-top"
                                 src="{{url('uploads/images/blogs/' . $blog->upload1)}}"
                                 alt="Card image">
                            <div class="card-body mb-3">
                                <p class="card-date text-muted">{{$blog->created_at->format('j F, Y')}}</p>
                                <h4 class="card-title mb-3">{{strip_tags($blog->title)}}</h4>
                                <p class="card-text">{!! str_limit(strip_tags($blog->body,100)) !!}</p>
                                <a href="{{route('view-blog',[str_slug("$blog->title","-"),$blog->id,])}}"
                                   class=" stretched-link">See Details</a>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>

            <div class="row">
                <div class="col p-5 text-center">
                    <a href="{{route('blog')}}" class="btn btn-fill mb-0">SEE MORE
                        BLOG</a>
                </div>
            </div>

        </div>

    </section>
    <!-- ==================================
         END BLOG
         ==================================
    -->

    {{--Client Says--}}

    <section class="client p-0 bg-white">
        <!-- Marquee -->
        <div class="marquee">
            <marquee behavior="scroll" direction="left" scrollamount="5">Client Says</marquee>
        </div>
        <!-- End Marquee -->

        <div class="container px-3 px-sm-5 my-5 text-center">
            <!-- Client Title -->
            <div class="section-heading text-center" data-aos="fade-up"
                 data-aos-easing="ease">
                <h2>What our client says</h2>
                <hr>
            </div>
            <!-- End Client Title -->

            <div id="carousel-client" class="owl-carousel owl-theme"
                 data-aos="fade-up"
                 data-aos-easing="ease"
                 data-aos-delay="300">

                <div class="item first prev">
                    <div class="card border-0 ">
                        <div class="row justify-content-center"><img
                                    src="{{url('uploads/images/home/client/'.$FirstClientData->image)}}"
                                    class="rounded-circle img-fluid profile-pic mb-4 mt-3">
                        </div>
                        <p class="content  mx-2">{!! $FirstClientData->comment !!}</p>
                        <h5>{{$FirstClientData->name}}</h5>
                        <p class="text-muted small">{!! $FirstClientData->designation !!}</p>
                    </div>
                </div>


                <div class="item show">
                    <div class="card border-0 ">
                        <div class="row justify-content-center"><img
                                    src="{{url('uploads/images/home/client/'.$SecondClientData->image)}}"
                                    class="rounded-circle img-fluid profile-pic mb-5 mt-3">
                        </div>
                        <p class="content  mx-2">{!! $SecondClientData->comment !!}</p>
                        <h5 class="mb-0 mt-2">{{$SecondClientData->name}}</h5>
                        <p class="text-muted small">{!! $SecondClientData->designation !!}</p>

                    </div>
                </div>

                @foreach($clientData as $client)

                    @if($client->id !== $FirstClientData->id AND $client->id !== $SecondClientData->id AND $client->id !== $LastClientData->id)
                        <div class="item next">
                            <div class="card border-0 ">
                                <div class="row justify-content-center"><img
                                            src="{{url('uploads/images/home/client/'.$client->image)}}"
                                            class="rounded-circle img-fluid profile-pic mb-5 mt-3">
                                </div>
                                <p class="content  mx-2">{!! $client->comment !!}</p>
                                <h5 class="mb-0 mt-2">{{$client->name}}</h5>
                                <p class="text-muted small">{!! $client->designation !!}</p>
                            </div>
                        </div>
                    @endif
                @endforeach


                <div class="item last">
                    <div class="card border-0 ">
                        <div class="row justify-content-center"><img
                                    src="{{url('uploads/images/home/client/'.$LastClientData->image)}}"
                                    class="rounded-circle img-fluid profile-pic mb-5 mt-3">
                        </div>
                        <p class="content  mx-2">{!! $LastClientData->comment !!}</p>
                        <h5 class="mb-0 mt-2">{{$LastClientData->name}}</h5>
                        <p class="text-muted small">{!! $LastClientData->designation !!}</p>
                    </div>
                </div>

            </div>


        </div>

    </section>


    <section class="p-0 bb bt" data-aos="fade-up" data-aos-easing="ease">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div id="logo-carousel" class="owl-carousel owl-theme">
                        @foreach($brandData as $value)
                            <div class="item"><img
                                        src="{{url('uploads/images/home/brands/'.$value->image)}}">
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>

        </div>
    </section>


    @include('frontend.layouts.footer-contact')

@endsection