@extends('admin-auth.layouts.app')

@section('content')
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card border-primary">

                        @if(session('error'))

                            <div class="alert alert-danger" style="background-color: #ff0000;color: white ">
                                <i class="fa fa-times"></i> {{session('error')}}

                            </div>

                        @endif


                        <div class="card-header bg-primary text-white" style="font-size: large;margin-top: 0px"><a


                                    href="{{route('index')}}" target="_blank" style="color: white">Social Aves</a>:
                            Admin
                            Login
                        </div>
                        <div class="card-body" style="color: black">
                            <form method="post" onsubmit="return validate();" action="{{route('admin-login')}}">
                                {{csrf_field()}}
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">E-Mail
                                        Address</label>

                                    <div class="col-md-6">
                                        <input id="email" type="name" class="form-control" name="email" value="">
                                        <h7 id="error_message1"></h7>


                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password">
                                        <h7 id="error_message2"></h7>


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember"
                                                   id="remember">

                                            <label class="form-check-label" for="remember">
                                                Remember Me
                                            </label>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-unlock"></i> Login
                                        </button>

                                       <a class="btn btn-link" href="{{route('admin.password.request')}}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>


                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection

